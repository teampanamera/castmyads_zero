import sys, os
#from uuid import getnode

'''
    This file provides first level authentication for InfoViz.
    It needs to be run, as a part of InfoViz installation process, otherwise IVZ will not work
'''

def getMacAddress():
    mac = None

    if sys.platform == 'win32':
        for line in os.popen("ipconfig /all"):
            if line.lstrip().startswith('Physical Address'):
                mac = line.split(':')[1].strip().replace('-',':')
                break
    else:
        for line in os.popen("/sbin/ifconfig"):
            #print(line)
            if line.find('ether') > -1:
                mac = line.split()[1]
                print(mac)
                #break
    return mac

if os.path.exists("/home/pi/mac.txt"):
    os.system("rm /home/pi/mac.txt")

f = open("/home/pi/mac.txt", "w")

#mac = str(getnode())
mac = getMacAddress()

f.write(mac)

f.close()

print("MAC Address data initialized")
#f = open("/home/pi/mac.txt", "r")
#print(f.read())
