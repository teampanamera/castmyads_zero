#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <unistd.h>

#include "bcm_host.h"

#include "GLES2/gl2.h"
#include "EGL/egl.h"
#include "EGL/eglext.h"
//#include <SOIL/SOIL.h>

#include <IL/il.h>

typedef struct
{
   uint32_t screen_width;
   uint32_t screen_height;
// OpenGL|ES objects
   EGLDisplay display;
   EGLSurface surface;
   EGLContext context;

} CUBE_STATE_T;


typedef struct
{
    // Handle to a program object
    GLuint programObject;

    // Attribute locations
    GLint positionLoc;
    GLint texCoordLoc;

    // Sampler location
    GLint samplerLoc;

    // Texture handle
    GLuint textureId[10];
} UserData;

static CUBE_STATE_T _state, *state=&_state;

#define check() assert(glGetError() == 0)

// display rotation mode. 0,1,2,3 for 0,90,180,270 degree rotation resp
// Initialize with 0 degree rotation
int display_rotation = 0;

// Sets display rotation index by greping and parsing the output of /boot/config.txt
void setDisplayRotationIndex()
{

  FILE *fp;
  char path[1035];

  /* Open the command for reading. */
  fp = popen("grep display_rotate /boot/config.txt", "r");
  if (fp == NULL) {
    printf("Failed to run command\n" );
    return;
  }

  /* Read the output a line at a time - output it. */
  while (fgets(path, sizeof(path)-1, fp) != NULL) {
    printf("%s", path);
  }

  char *token, *string, *tofree;

  tofree = string = strdup(path);

  int i =0;
  while ((token = strsep(&string, "=")) != NULL)
  {
    if (i==1) {
        sscanf(token, "%d", &display_rotation);
    }

    i ++;
  }

  free(tofree);

  /* close */
  pclose(fp);
}

GLint createTextureDevil(const char* filename)
{
    ILboolean success;
    unsigned int imageID;
    GLint textureID;
 
    // init DevIL. This needs to be done only once per application
    ilInit();

    // generate an image name
    ilGenImages(1, &imageID); 

    // bind it
    ilBindImage(imageID); 

    // match image origin to OpenGL’s
    ilEnable(IL_ORIGIN_SET);
    ilOriginFunc(IL_ORIGIN_UPPER_LEFT);

    // load  the image
    success = ilLoadImage((ILstring)filename);
    // check to see if everything went OK
    if (!success) {
        ilDeleteImages(1, &imageID); 
 
        return 0;
    }

    /* Convert image to RGBA with unsigned byte data type */
    ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE); 

    /* Create and load textures to OpenGL */
    glGenTextures(1, &textureID); /* Texture name generation */
    glBindTexture(GL_TEXTURE_2D, textureID); 
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
                ilGetInteger(IL_IMAGE_WIDTH),
                ilGetInteger(IL_IMAGE_HEIGHT), 
                0, GL_RGBA, GL_UNSIGNED_BYTE,
                ilGetData());
   //Set texture parameters
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );

    //Delete file from memory
    ilDeleteImages( 1, &imageID );

    ilShutDown();

    return textureID;
}

GLuint esLoadProgram(const char *, const char *);

/*
GLuint CreateSimpleTexture2D()
{
    // Texture object handle
    GLuint textureId;

    // 2x2 Image, 3 bytes per pixel (R, G, B)
    GLubyte pixels[4 * 3] = {
        255, 0, 0, // Red
        0, 255, 0, // Green
        0, 0, 255, // Blue
        255, 255, 0 // Yellow
    };

    // Use tightly packed data
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    // Generate a texture object
    glGenTextures(1, &textureId);

    // Bind the texture object
    glBindTexture(GL_TEXTURE_2D, textureId);

    // Load the texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 2, 2, 0, GL_RGB, GL_UNSIGNED_BYTE, pixels);

    // Set the filtering mode
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    return textureId;
}
*/

GLfloat getOpenGLX(unsigned int x)
{
    return (((2.0f* x) / state->screen_width) - 1.0f);
}

GLfloat getOpenGLY(unsigned int y)
{
    return (1.0f - ((2.0f * y) / state->screen_height));
}


// Draw a triangle using the shader pair created in Init()
void Draw (UserData* userData, unsigned int id, const char* filepath, unsigned int xPos, unsigned int yPos, unsigned int imageWidth, unsigned int imageHeight)
{

/*
    userData->textureId[id] = SOIL_load_OGL_texture
                                  (
                                  filepath,
                                  SOIL_LOAD_AUTO,
                                  SOIL_CREATE_NEW_ID,
                                  SOIL_FLAG_POWER_OF_TWO
                                  );
    char* res;

    res = SOIL_last_result();

    printf("\n%s\n", res);
*/

    userData->textureId[id] = createTextureDevil(filepath);

    GLfloat vVertices[] = { -1.0f,  1.0f, 0.0f,  // Position 0
                            0.0f,  0.0f,        // TexCoord 0
                           -1.0f, -1.0f, 0.0f,  // Position 1
                            0.0f,  1.0f,        // TexCoord 1
                            1.0f, -1.0f, 0.0f,  // Position 2
                            1.0f,  1.0f,        // TexCoord 2
                            1.0f,  1.0f, 0.0f,  // Position 3
                            1.0f,  0.0f         // TexCoord 3
                         };

    if (display_rotation == 0) 
    {

        // Rotation 0

        vVertices[0] = getOpenGLX(xPos);
        vVertices[1] = getOpenGLY(yPos);

        vVertices[5] = getOpenGLX(xPos);
        vVertices[6] = getOpenGLY(yPos + imageHeight);

        vVertices[10] = getOpenGLX(xPos + imageWidth);
        vVertices[11] = getOpenGLY(yPos + imageHeight);

        vVertices[15] = getOpenGLX(xPos + imageWidth);
        vVertices[16] = getOpenGLY(yPos);

    }
    else if (display_rotation == 1) 
    {

        // Rotation 90 anti clockwise
        vVertices[5] = getOpenGLX(xPos);
        vVertices[6] = getOpenGLY(yPos);

        vVertices[10] = getOpenGLX(xPos);
        vVertices[11] = getOpenGLY(yPos + imageHeight);

        vVertices[15] = getOpenGLX(xPos + imageWidth);
        vVertices[16] = getOpenGLY(yPos + imageHeight);

        vVertices[0] = getOpenGLX(xPos + imageWidth);
        vVertices[1] = getOpenGLY(yPos);

    }
    else if (display_rotation == 2)
    {

        // Rotation 180 counter clockwise
        vVertices[0] = getOpenGLX(xPos + imageWidth);
        vVertices[1] = getOpenGLY(yPos + imageHeight);

        vVertices[5] = getOpenGLX(xPos + imageWidth);
        vVertices[6] = getOpenGLY(yPos);

        vVertices[10] = getOpenGLX(xPos);
        vVertices[11] = getOpenGLY(yPos);

        vVertices[15] = getOpenGLX(xPos);
        vVertices[16] = getOpenGLY(yPos + imageHeight);

    }
    else if (display_rotation == 3)
    {
        // Rotation 270 counter clockwise
    
        vVertices[0] = getOpenGLX(xPos);
        vVertices[1] = getOpenGLY(yPos + imageHeight);

        vVertices[5] = getOpenGLX(xPos + imageWidth);
        vVertices[6] = getOpenGLY(yPos + imageHeight);

        vVertices[10] = getOpenGLX(xPos + imageWidth);
        vVertices[11] = getOpenGLY(yPos);

        vVertices[15] = getOpenGLX(xPos);
        vVertices[16] = getOpenGLY(yPos);
   
    }

    //printf("\nPosition 0 = %f %f \nPosition 1 = %f %f \nPosition 2 = %f %f \nPosition 3 = %f %f\n", vVertices[0], vVertices[1], vVertices[5], vVertices[6], vVertices[10], vVertices[11], vVertices[15], vVertices[16]);

    GLushort indices[] =
    { 0, 1, 2, 0, 2, 3 };

    // Set the viewport
    glViewport(0, 0, state->screen_width, state->screen_height);

    // Clear the color buffer
    //glClear(GL_COLOR_BUFFER_BIT);

    // Use the program object
    glUseProgram(userData->programObject);

    // Load the vertex position
    glVertexAttribPointer(userData->positionLoc, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), vVertices);
    // Load the texture coordinate
    glVertexAttribPointer(userData->texCoordLoc, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), &vVertices[3]);

    glEnableVertexAttribArray(userData->positionLoc);
    glEnableVertexAttribArray(userData->texCoordLoc);

    // Bind the texture
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, userData->textureId[id]);

    // Set the sampler texture unit to 0
    glUniform1i(userData->samplerLoc, 0);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, indices);

}

///
// Cleanup
//
void ShutDown(UserData *userData, unsigned int numTextures)
{

    glDeleteTextures(numTextures, userData->textureId);

    // Delete program object
    glDeleteProgram(userData->programObject);
}

///
// Initialize the shader and program object
//
int Init(UserData *userData)
{
    GLchar vShaderStr[] =
        "attribute vec4 a_position;   \n"
        "attribute vec2 a_texCoord;   \n"
        "varying vec2 v_texCoord;     \n"
        "void main()                  \n"
        "{                            \n"
        "   gl_Position = a_position; \n"
        "   v_texCoord = a_texCoord;  \n"
        "}                            \n";

    GLchar fShaderStr[] =
        "precision mediump float;                            \n"
        "varying vec2 v_texCoord;                            \n"
        "uniform sampler2D s_texture;                        \n"
        "void main()                                         \n"
        "{                                                   \n"
        "  gl_FragColor = texture2D( s_texture, v_texCoord );\n"
        "}                                                   \n";

    // Load the shaders and get a linked program object
    userData->programObject = esLoadProgram(vShaderStr, fShaderStr);

    // Get the attribute locations
    userData->positionLoc = glGetAttribLocation(userData->programObject, "a_position");
    userData->texCoordLoc = glGetAttribLocation(userData->programObject, "a_texCoord");

    // Get the sampler location
    userData->samplerLoc = glGetUniformLocation(userData->programObject, "s_texture");

    // Load the texture
    //userData->textureId = CreateSimpleTexture2D();
    
    //glGenerateMipmap(userData->textureId);

    //glClearColor(1.0f, 0.0f, 0.0f, 0.0f);
    return 1;
}

/***********************************************************
 * Name: init_ogl
 *
 * Arguments:
 *       CUBE_STATE_T *state - holds OGLES model info
 *
 * Description: Sets the display, OpenGL|ES context and screen stuff
 *
 * Returns: void
 *
 ***********************************************************/
static void init_ogl(CUBE_STATE_T *state, unsigned int fullScreenFlag)
{
   int32_t success = 0;
   EGLBoolean result;
   EGLint num_config;

   static EGL_DISPMANX_WINDOW_T nativewindow;

   DISPMANX_ELEMENT_HANDLE_T dispman_element;
   DISPMANX_DISPLAY_HANDLE_T dispman_display;
   DISPMANX_UPDATE_HANDLE_T dispman_update;
   VC_RECT_T dst_rect;
   VC_RECT_T src_rect;

   static const EGLint attribute_list[] =
   {
      EGL_RED_SIZE, 8,
      EGL_GREEN_SIZE, 8,
      EGL_BLUE_SIZE, 8,
      EGL_ALPHA_SIZE, 8,
      EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
      EGL_NONE
   };
   
   static const EGLint context_attributes[] = 
   {
      EGL_CONTEXT_CLIENT_VERSION, 2,
      EGL_NONE
   };
   EGLConfig config;

   // get an EGL display connection
   state->display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
   assert(state->display!=EGL_NO_DISPLAY);
   check();

   // initialize the EGL display connection
   result = eglInitialize(state->display, NULL, NULL);
   assert(EGL_FALSE != result);
   check();

   // get an appropriate EGL frame buffer configuration
   result = eglChooseConfig(state->display, attribute_list, &config, 1, &num_config);
   assert(EGL_FALSE != result);
   check();

   // get an appropriate EGL frame buffer configuration
   result = eglBindAPI(EGL_OPENGL_ES_API);
   assert(EGL_FALSE != result);
   check();

   // create an EGL rendering context
   state->context = eglCreateContext(state->display, config, EGL_NO_CONTEXT, context_attributes);
   assert(state->context!=EGL_NO_CONTEXT);
   check();

   // create an EGL window surface
   success = graphics_get_display_size(0 /* LCD */, &state->screen_width, &state->screen_height);

   // Make space for marquee text
   //state->screen_height -= (state->screen_height/18);

   assert( success >= 0 );

   if (display_rotation == 0)
   {
       if (!fullScreenFlag)
       {
	   state->screen_height -= (state->screen_height/18);
       }

       dst_rect.x = 0;
       dst_rect.y = 0;
       dst_rect.width = state->screen_width;
       dst_rect.height = state->screen_height;
   }
   else if (display_rotation == 1)
   {
       if (fullScreenFlag) 
       {
           dst_rect.x = 0;
       }
       else
       {
           dst_rect.x = state->screen_height/18;
           state->screen_width -= (state->screen_height/18);
       }

       dst_rect.y = 0;
       dst_rect.width = state->screen_width;
       dst_rect.height = state->screen_height;
   }
   else if (display_rotation == 2)
   {

       if (fullScreenFlag)
       {
           dst_rect.y = 0;
       }
       else
       {
           dst_rect.y = state->screen_height/18;
       }

       //state->screen_height -= (state->screen_height/18);

       dst_rect.x = 0;
       dst_rect.width = state->screen_width;
       dst_rect.height = state->screen_height;
   }
   else if (display_rotation == 3)
   {

       if (!fullScreenFlag) 
       {
           state->screen_width -= (state->screen_height/18);
       }

       dst_rect.x = 0;
       dst_rect.y = 0;
       dst_rect.width = state->screen_width;
       dst_rect.height = state->screen_height;
   }

   //printf("\ntriangle2 : %d %d %d %d\n", dst_rect.x, dst_rect.y, dst_rect.width, dst_rect.height);
   //printf("\ntriangle2 : %d %d", state->screen_width, state->screen_height);

   //dst_rect.width = 500;
   //dst_rect.height = 500;
       
   src_rect.x = 0;
   src_rect.y = 0;
   src_rect.width = state->screen_width << 16;
   src_rect.height = state->screen_height << 16;        

   dispman_display = vc_dispmanx_display_open( 0 /* LCD */);
   dispman_update = vc_dispmanx_update_start( 0 );
         
   dispman_element = vc_dispmanx_element_add ( dispman_update, dispman_display,
      0/*layer*/, &dst_rect, 0/*src*/,
      &src_rect, DISPMANX_PROTECTION_NONE, 0 /*alpha*/, 0/*clamp*/, 0/*transform*/);
      
   nativewindow.element = dispman_element;
   nativewindow.width = state->screen_width;
   nativewindow.height = state->screen_height;
   vc_dispmanx_update_submit_sync( dispman_update );
      
   check();

   state->surface = eglCreateWindowSurface( state->display, config, &nativewindow, NULL );
   assert(state->surface != EGL_NO_SURFACE);
   check();

   // connect the context to the surface
   result = eglMakeCurrent(state->display, state->surface, state->surface, state->context);
   assert(EGL_FALSE != result);
   check();

}

static void exit_func(void)
{
   eglDestroySurface( state->display, state->surface );

   // Release OpenGL resources
   eglMakeCurrent( state->display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT );
   eglDestroyContext( state->display, state->context );
   eglTerminate( state->display );
} 

GLuint esLoadShader(GLenum type, const char *shaderSrc)
{
    GLuint shader;
    GLint compiled;

    // Create the shader object
    shader = glCreateShader(type);

    if (shader == 0)
        return 0;

    // Load the shader source
    glShaderSource(shader, 1, &shaderSrc, NULL);

    // Compile the shader
    glCompileShader(shader);

    // Check the compile status
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);

    if (!compiled)
    {
        GLint infoLen = 0;

        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);

        if (infoLen > 1)
        {
            char* infoLog = malloc(sizeof(char) * infoLen);

            glGetShaderInfoLog(shader, infoLen, NULL, infoLog);

            free(infoLog);
        }

        glDeleteShader(shader);
        return 0;
    }

    return shader;
}

GLuint esLoadProgram(const char *vertShaderSrc, const char *fragShaderSrc)
{
    GLuint vertexShader;
    GLuint fragmentShader;
    GLuint programObject;
    GLint linked;

    // Load the vertex/fragment shaders
    vertexShader = esLoadShader(GL_VERTEX_SHADER, vertShaderSrc);
    if (vertexShader == 0)
        return 0;

    fragmentShader = esLoadShader(GL_FRAGMENT_SHADER, fragShaderSrc);
    if (fragmentShader == 0)
    {
        glDeleteShader(vertexShader);
        return 0;
    }

    // Create the program object
    programObject = glCreateProgram();

    if (programObject == 0)
        return 0;

    glAttachShader(programObject, vertexShader);
    glAttachShader(programObject, fragmentShader);

    // Link the program
    glLinkProgram(programObject);

    // Check the link status
    glGetProgramiv(programObject, GL_LINK_STATUS, &linked);

    if (!linked)
    {
        GLint infoLen = 0;

        glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &infoLen);

        if (infoLen > 1)
        {
            char* infoLog = malloc(sizeof(char) * infoLen);

            glGetProgramInfoLog(programObject, infoLen, NULL, infoLog);
            printf("Error linking program:\n%s\n", infoLog);

            free(infoLog);
        }

        glDeleteProgram(programObject);
        return 0;
    }

    // Free up no longer needed shader resources
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    return programObject;
}

int main (int argc, char* argv[])
{

   printf("\n%d\n", argc);

   // Program name, duration, bg_red, bg_green, bg_blue, numTextures, fullScreenFlag, imagePath, xPos, yPos, imageWidth, imageHeight, ...
   if (argc < 12) {
       printf("\nNot enough params ...\n");
       return 0;
   }

   unsigned int duration = atoi(argv[1]);
   unsigned int numTextures = atoi(argv[5]);
   // fullScreenFlag indicates whether window needs to be full screen and not leave any space for marquee
   unsigned int fullScreenFlag = atoi(argv[6]);

   if (argc != (5*numTextures + 7)) {
       printf("\nNot enough params ...\n");
       return 0;
   }

   GLfloat bg_red = atof(argv[2]);
   GLfloat bg_green = atof(argv[3]);
   GLfloat bg_blue = atof(argv[4]);

   //int terminate = 0;
   //GLfloat cx, cy;
   bcm_host_init();

   // Clear application state
   memset(state, 0, sizeof(*state));

   setDisplayRotationIndex();

   // Start OGLES
   init_ogl(state, fullScreenFlag);

   UserData userData;

   if (!Init(&userData)) {
       printf("\nOpenGL initialization failed ...\n");
       return 0;
   }

   // Set background color and clear buffers
   glClearColor(bg_red, bg_green, bg_blue, 1.0f);
   glClear( GL_COLOR_BUFFER_BIT );

   for (int i = 0; i < numTextures; i++) {
       // Program name, duration, bg_red, bg_green, bg_blue, numTextures, imagePath, xPos, yPos, imageWidth, imageHeight, ...
       Draw(&userData, i, argv[i*5 + 7], atoi(argv[i*5 + 8]), atoi(argv[i*5 + 9]), atoi(argv[i*5 + 10]), atoi(argv[i*5 + 11]));
   }

   eglSwapBuffers(state->display, state->surface);

   sleep(duration);

   ShutDown(&userData, numTextures);

   exit_func();
   
   return 0;
}

