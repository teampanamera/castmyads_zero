/*
Copyright (c) 2012, Broadcom Europe Ltd
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the copyright holder nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// A rotating cube rendered with OpenGL|ES. Three images used as textures on the cube faces.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <unistd.h>

#include "bcm_host.h"

#include "EGL/egl.h"
#include "EGL/eglext.h"
#include "VG/openvg.h"
#include "VG/vgu.h"
#include "DejaVuSerif.inc"
#include "fontinfo.h"

typedef struct
{
   uint32_t screen_width;
   uint32_t screen_height;
   DISPMANX_DISPLAY_HANDLE_T dispman_display;
   DISPMANX_ELEMENT_HANDLE_T dispman_element;
   EGLDisplay display;
   EGLSurface surface;
   EGLContext context;
} CUBE_STATE_T;

static void init_ogl(CUBE_STATE_T *state);
static CUBE_STATE_T _state, *state=&_state;
Fontinfo SerifTypeface;
static const int MAXFONTPATH = 500;

// TextHeight reports a font's height
VGfloat TextHeight(int pointsize) {
        return (SerifTypeface.font_height * pointsize) / 65536;
}

// next_utf_char handles UTF encoding
unsigned char *next_utf8_char(unsigned char *utf8, int *codepoint) {
        int seqlen;
        int datalen = (int)strlen((const char *)utf8);
        unsigned char *p = utf8;

        if (datalen < 1 || *utf8 == 0) {                   // End of string
                return NULL;
        }
        if (!(utf8[0] & 0x80)) {                           // 0xxxxxxx
                *codepoint = (wchar_t) utf8[0];
                seqlen = 1;
        } else if ((utf8[0] & 0xE0) == 0xC0) {             // 110xxxxx
                *codepoint = (int)(((utf8[0] & 0x1F) << 6) | (utf8[1] & 0x3F));
                seqlen = 2;
        } else if ((utf8[0] & 0xF0) == 0xE0) {             // 1110xxxx
                *codepoint = (int)(((utf8[0] & 0x0F) << 12) | ((utf8[1] & 0x3F) << 6) | (utf8[2] & 0x3F));
                seqlen = 3;
        } else {
                return NULL;                               // No code points this high here
        }
        p += seqlen;
        return p;
}


// TextWidth returns the width of a text string at the specified font and size.
VGfloat TextWidth(const char *s, int pointsize) {
        VGfloat tw = 0.0;
        VGfloat size = (VGfloat) pointsize;
        int character;
        unsigned char *ss = (unsigned char *)s;
        while ((ss = next_utf8_char(ss, &character)) != NULL) {
                int glyph = SerifTypeface.CharacterMap[character];
                if (character >= MAXFONTPATH - 1) {
                        continue;
                }
                if (glyph == -1) {
                        continue;                          //glyph is undefined
                }
                tw += size * SerifTypeface.GlyphAdvances[glyph] / 65536.0f;
        }
        return tw;
}

int getMaxFontSizeByWidth(const char* text, int w) {

    VGfloat tw=0;
    int fontsize = 1;

    while (tw<w) {

        tw = TextWidth(text, fontsize);
        fontsize++;
    }

    return (fontsize - 2);
}

// Get max font size based on provided height value
int getMaxFontSize(int h) {

        VGfloat th = 0;
        int fontsize = 1;

        while (th<h) {
            th = TextHeight(fontsize);
            fontsize ++;
        }

        return (fontsize-2);
}


// display rotation mode. 0,1,2,3 for 0,90,180,270 degree rotation resp
// Initialize with 0 degree rotation
int display_rotation = 0;

// Sets display rotation index by greping and parsing the output of /boot/config.txt
void setDisplayRotationIndex()
{

  FILE *fp;
  char path[1035];

  /* Open the command for reading. */
  fp = popen("grep display_rotate /boot/config.txt", "r");
  if (fp == NULL) {
    printf("Failed to run command\n" );
    return;
  }

  /* Read the output a line at a time - output it. */
  while (fgets(path, sizeof(path)-1, fp) != NULL) {
    printf("%s", path);
  }

  char *token, *string, *tofree;

  tofree = string = strdup(path);

  int i =0;
  while ((token = strsep(&string, "=")) != NULL)
  {
    if (i==1) {
        sscanf(token, "%d", &display_rotation);
    }

    i ++;
  }

  free(tofree);

  /* close */
  pclose(fp);
}


// loadfont loads font path data
// derived from http://web.archive.org/web/20070808195131/http://developer.hybrid.fi/font2openvg/renderFont.cpp.txt
Fontinfo loadfont(const int *Points,
		  const int *PointIndices,
		  const unsigned char *Instructions,
		  const int *InstructionIndices, const int *InstructionCounts, const int *adv, const short *cmap, int ng) {

	Fontinfo f;
	int i;

	memset(f.Glyphs, 0, MAXFONTPATH * sizeof(VGPath));
	if (ng > MAXFONTPATH) {
		return f;
	}
	for (i = 0; i < ng; i++) {
		const int *p = &Points[PointIndices[i] * 2];
		const unsigned char *instructions = &Instructions[InstructionIndices[i]];
		int ic = InstructionCounts[i];
		VGPath path = vgCreatePath(VG_PATH_FORMAT_STANDARD, VG_PATH_DATATYPE_S_32,
					   1.0f / 65536.0f, 0.0f, 0, 0,
					   VG_PATH_CAPABILITY_ALL);
		f.Glyphs[i] = path;
		if (ic) {
			vgAppendPathData(path, ic, instructions, p);
		}
	}
	f.CharacterMap = cmap;
	f.GlyphAdvances = adv;
	f.Count = ng;
	f.descender_height = 0;
	f.font_height = 0;
	return f;
}

// unloadfont frees font path data
void unloadfont(VGPath * glyphs, int n) {
	int i;
	for (i = 0; i < n; i++) {
		vgDestroyPath(glyphs[i]);
	}
}

/***********************************************************
 * Name: init_ogl
 *
 * Arguments:
 *       CUBE_STATE_T *state - holds OGLES model info
 *
 * Description: Sets the display, OpenGL|ES context and screen stuff
 *
 * Returns: void
 *
 ***********************************************************/
static void init_ogl(CUBE_STATE_T *state)
{
   int32_t success = 0;
   EGLBoolean result;
   EGLint num_config;

   static EGL_DISPMANX_WINDOW_T nativewindow;

   DISPMANX_UPDATE_HANDLE_T dispman_update;
   VC_RECT_T dst_rect;
   VC_RECT_T src_rect;

   static const EGLint attribute_list[] =
   {
      EGL_RED_SIZE, 8,
      EGL_GREEN_SIZE, 8,
      EGL_BLUE_SIZE, 8,
      EGL_ALPHA_SIZE, 8,
      EGL_RENDERABLE_TYPE, EGL_OPENVG_BIT,
      EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
      EGL_NONE
   };
   
   EGLConfig config;

   // get an EGL display connection
   state->display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
   assert(state->display!=EGL_NO_DISPLAY);

   // initialize the EGL display connection
   result = eglInitialize(state->display, NULL, NULL);
   assert(EGL_FALSE != result);
   
   result = eglBindAPI(EGL_OPENVG_API);
   assert(EGL_FALSE != result);

   // get an appropriate EGL frame buffer configuration
   result = eglChooseConfig(state->display, attribute_list, &config, 1, &num_config);
   assert(EGL_FALSE != result);

   // create an EGL rendering context
   state->context = eglCreateContext(state->display, config, EGL_NO_CONTEXT, NULL);
   assert(state->context!=EGL_NO_CONTEXT);

   // create an EGL window surface
   success = graphics_get_display_size(0 /* LCD */, &state->screen_width, &state->screen_height);
   assert( success >= 0 );

   if (display_rotation == 0)
   {

       // Origin is top left
       dst_rect.x = state->screen_width/4;
       dst_rect.y = (state->screen_height/10);

       state->screen_height = (state->screen_height/4);
       state->screen_width -= (state->screen_width/2);

       dst_rect.width = state->screen_width;
       dst_rect.height = state->screen_height;

   }
   else if (display_rotation == 1)
   {
       // Top left is origin
       dst_rect.x = state->screen_width/2;
       dst_rect.y = state->screen_height/4;

       state->screen_width   = (state->screen_width/8);
       state->screen_height  = (state->screen_height/2);

       dst_rect.width = state->screen_width;
       dst_rect.height = state->screen_height;
      
   }
   else if (display_rotation == 2)
   {
       // Origin is top left
       dst_rect.x = state->screen_width/4;
       dst_rect.y = state->screen_height - (state->screen_height/10) - (state->screen_height/4);

       state->screen_height = (state->screen_height/4);
       state->screen_width -= (state->screen_width/2);

       dst_rect.width = state->screen_width;
       dst_rect.height = state->screen_height;

   }
   else if (display_rotation == 3)
   {
       // Top left is origin
       dst_rect.x = state->screen_width/2;
       dst_rect.y = state->screen_height/4;

       state->screen_width   = (state->screen_width/8);
       state->screen_height  = (state->screen_height/2);

       dst_rect.width = state->screen_width;
       dst_rect.height = state->screen_height;
 
   }

 
   src_rect.x = 0;
   src_rect.y = 0;
   src_rect.width = state->screen_width << 16;
   src_rect.height = state->screen_height << 16;        

   state->dispman_display = vc_dispmanx_display_open( 0 /* LCD */);
   dispman_update = vc_dispmanx_update_start( 0 );
         
   state->dispman_element = vc_dispmanx_element_add ( dispman_update, state->dispman_display,
      10/*layer*/, &dst_rect, 0/*src*/,
      &src_rect, DISPMANX_PROTECTION_NONE, 0 /*alpha*/, 0/*clamp*/, 0/*transform*/);
      
   nativewindow.element = state->dispman_element;
   nativewindow.width = state->screen_width;
   nativewindow.height = state->screen_height;
   vc_dispmanx_update_submit_sync( dispman_update );
      
   state->surface = eglCreateWindowSurface( state->display, config, &nativewindow, NULL );
   assert(state->surface != EGL_NO_SURFACE);

   // connect the context to the surface
   result = eglMakeCurrent(state->display, state->surface, state->surface, state->context);
   assert(EGL_FALSE != result);
   
   SerifTypeface = loadfont(DejaVuSerif_glyphPoints,
							DejaVuSerif_glyphPointIndices,
							DejaVuSerif_glyphInstructions,
							DejaVuSerif_glyphInstructionIndices,
							DejaVuSerif_glyphInstructionCounts,
							DejaVuSerif_glyphAdvances, 
							DejaVuSerif_characterMap, 
							DejaVuSerif_glyphCount);
	
	SerifTypeface.descender_height = DejaVuSerif_descender_height;
	SerifTypeface.font_height = DejaVuSerif_font_height;

}

//
// Color functions
//
//

// RGBA fills a color vectors from a RGBA quad.
void RGBA(unsigned int r, unsigned int g, unsigned int b, VGfloat a, VGfloat color[4]) {
	if (r > 255) {
		r = 0;
	}
	if (g > 255) {
		g = 0;
	}
	if (b > 255) {
		b = 0;
	}
	if (a < 0.0 || a > 1.0) {
		a = 1.0;
	}
	color[0] = (VGfloat) r / 255.0f;
	color[1] = (VGfloat) g / 255.0f;
	color[2] = (VGfloat) b / 255.0f;
	color[3] = a;
}

// RGB returns a solid color from a RGB triple
void RGB(unsigned int r, unsigned int g, unsigned int b, VGfloat color[4]) {
	RGBA(r, g, b, 1.0f, color);
}

// setfill sets the fill color
void setfill(VGfloat color[4]) {
	VGPaint fillPaint = vgCreatePaint();
	vgSetParameteri(fillPaint, VG_PAINT_TYPE, VG_PAINT_TYPE_COLOR);
	vgSetParameterfv(fillPaint, VG_PAINT_COLOR, 4, color);
	vgSetPaint(fillPaint, VG_FILL_PATH);
	vgDestroyPaint(fillPaint);
}

// setstroke sets the stroke color
void setstroke(VGfloat color[4]) {
	VGPaint strokePaint = vgCreatePaint();
	vgSetParameteri(strokePaint, VG_PAINT_TYPE, VG_PAINT_TYPE_COLOR);
	vgSetParameterfv(strokePaint, VG_PAINT_COLOR, 4, color);
	vgSetPaint(strokePaint, VG_STROKE_PATH);
	vgDestroyPaint(strokePaint);
}

// StrokeWidth sets the stroke width
void StrokeWidth(VGfloat width) {
	vgSetf(VG_STROKE_LINE_WIDTH, width);
	vgSeti(VG_STROKE_CAP_STYLE, VG_CAP_BUTT);
	vgSeti(VG_STROKE_JOIN_STYLE, VG_JOIN_MITER);
}

// Start begins the picture, clearing a rectangular region with a specified color
void Start(int width, int height) {
	VGfloat color[4] = { 1, 1, 1, 1 };
	vgSetfv(VG_CLEAR_COLOR, 4, color);
	vgClear(0, 0, width, height);
	color[0] = 0, color[1] = 0, color[2] = 0;
	setfill(color);
	setstroke(color);
	StrokeWidth(0);
	vgLoadIdentity();
}

// End checks for errors, and renders to the display
void End() {
	assert(vgGetError() == VG_NO_ERROR);
	eglSwapBuffers(state->display, state->surface);
	assert(eglGetError() == EGL_SUCCESS);
}

// Backgroud clears the screen to a solid background color
void Background(unsigned int r, unsigned int g, unsigned int b) {
	VGfloat colour[4];
	RGB(r, g, b, colour);
	vgSetfv(VG_CLEAR_COLOR, 4, colour);
	vgClear(0, 0, state->screen_width, state->screen_height);
}

void Translate(VGfloat x, VGfloat y) {
	vgTranslate(x, y);
}

// Fill sets the fillcolor, defined as a RGBA quad.
void Fill(unsigned int r, unsigned int g, unsigned int b, VGfloat a) {
	VGfloat color[4];
	RGBA(r, g, b, a, color);
	setfill(color);
}

void Text(VGfloat x, VGfloat y, const char *s, Fontinfo f, int pointsize) {
	VGfloat size = (VGfloat) pointsize, xx = x, mm[9];
	vgGetMatrix(mm);
	int character;
	unsigned char *ss = (unsigned char *)s;
	while ((ss = next_utf8_char(ss, &character)) != NULL) {
		int glyph = f.CharacterMap[character];
		if (character >= MAXFONTPATH - 1) {
			continue;
		}
		if (glyph == -1) {
			continue;			   //glyph is undefined
		}
		VGfloat mat[9] = {
			size, 0.0f, 0.0f,
			0.0f, size, 0.0f,
			xx, y, 1.0f
		};
		vgLoadMatrix(mm);
		vgMultMatrix(mat);
		vgDrawPath(f.Glyphs[glyph], VG_FILL_PATH);
		xx += size * f.GlyphAdvances[glyph] / 65536.0f;
	}
	vgLoadMatrix(mm);
}

// Rotate around angle r
void Rotate(VGfloat r) {
	vgRotate(r);
}

// rotext draws text, rotated around the center of the screen, progressively faded
void rotext(char *s) {

	int size = 0;

	if (display_rotation == 0) {
            size = getMaxFontSizeByWidth(s, state->screen_width);
	} 
	else if (display_rotation == 1) {
            size = getMaxFontSizeByWidth(s, state->screen_height);

	    // Translate is relative to window position ot dst structure in init_ogl
	    Translate(0, state->screen_height);
            Rotate(-90);
	}
	else if (display_rotation == 2) {
            size = getMaxFontSizeByWidth(s, state->screen_width);

	    // After 180 rotation the rectangle offsets from its original position. Use translate to bring it back
	    Translate(state->screen_width, state->screen_height);
	    Rotate(-180);
	}
	else if (display_rotation == 3) {
            size = getMaxFontSizeByWidth(s, state->screen_height);

	    // Translate is relative to window position ot dst structure in init_ogl
	    Translate(state->screen_width, 0);
            Rotate(90);
	}

	//Start(w, h);
	Background(1, 0, 0);
	//Translate(x, y);

        // Set the text color
	Fill(255, 255, 255, 255);
	Text(0, state->screen_height/5, s, SerifTypeface, size);

	End();
}

// finish cleans up
void finish() {
	unloadfont(SerifTypeface.Glyphs, SerifTypeface.Count);
	eglSwapBuffers(state->display, state->surface);
	eglMakeCurrent(state->display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
	eglDestroySurface(state->display, state->surface);
	eglDestroyContext(state->display, state->context);
	eglTerminate(state->display);
}

/*==============================================================================

This file is used for displayed OTA key
Usage : ./binary "123456" 10
Handles display rotation as well

============================================================================== */

int main (int argc, char** argv)
{

   if (argc != 3) {
       printf("\nNot sufficient params for rendering OTA\n");
       return 0;
   }

   char * text = argv[1];
   int duration = atoi(argv[2]);
   
   bcm_host_init();

   // Clear application state
   memset( state, 0, sizeof( *state ) );

   setDisplayRotationIndex();
      
   init_ogl(state);
   
   rotext(text);

   printf("\nDISPLAYOTA:SUCCESS");
  
   sleep(duration); 
   
   finish();
   
   return 0;
}

