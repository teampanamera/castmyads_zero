from os import path, listdir, system
from sys import exit
from subprocess import PIPE, Popen

'''
 The purpose of this file is to authenticate the newly uploaded target binary
 It outputs only two values :
 UPDATE   : All is well, go ahead and update the target binary
 CONTINUE : Something is wrong with the new target binary, don't update
'''

files = listdir(r"/home/pi/IVZ/bin/updated_bin")

if files != []:
    #print("UPDATE")
    #print(files[0])

    extract_command = "openssl enc -aes-256-cbc -d -in " + path.join(r"/home/pi/IVZ/bin/updated_bin", files[0]) + " -out " + path.join(r"/home/pi/IVZ/bin/updated_bin", files[0].split('.enc')[0]) + " -k OgFxxMSBIDat7pj"

    #print(extract_command)

    p = Popen([extract_command], shell=True, stdout=PIPE, stderr=PIPE)
    # Read the std error
    res = p.communicate()[1]

    #print(res)

    if res != '':
        #print("Failed to extract encrypted file")
        print("CONTINUE")
        system("sudo rm /home/pi/IVZ/bin/updated_bin/*")
        exit(0)

    file_command = "file " + path.join(r"/home/pi/IVZ/bin/updated_bin", files[0].split('.enc')[0])  

    p = Popen([file_command], shell=True, stdout=PIPE, stderr=PIPE)
    # Read the std output
    res = p.communicate()[0]

    if "ELF 32-bit LSB executable, ARM, EABI5 version 1 (SYSV), dynamically linked, interpreter" not in res:
        #print("Invalid file found")
        print("CONTINUE")
        system("sudo rm /home/pi/IVZ/bin/updated_bin/*")
        exit(0)
    #else:
    #    print("Found valid file")

    # Give access rights for execution
    access_rights = "sudo chmod +x " + path.join(r"/home/pi/IVZ/bin/updated_bin", files[0].split('.enc')[0])  
    system(access_rights)

    security_question_1 = path.join(r"/home/pi/IVZ/bin/updated_bin", files[0].split('.enc')[0]) + " organization"
    p = Popen([security_question_1], shell=True, stdout=PIPE, stderr=PIPE)
    # Read the std output
    res = p.communicate()[0].replace('\n', '')

    if res != "panamera":
        #print("Invalid file found 1")
        print("CONTINUE")
        system("sudo rm /home/pi/IVZ/bin/updated_bin/*")
        exit(0)

    security_question_2 = path.join(r"/home/pi/IVZ/bin/updated_bin", files[0].split('.enc')[0]) + " location"
    p = Popen([security_question_2], shell=True, stdout=PIPE, stderr=PIPE)
    # Read the std output
    res = p.communicate()[0].replace('\n', '')

    if res != "sangli":
        #print("Invalid file found 2")
        print("CONTINUE")
        system("sudo rm /home/pi/IVZ/bin/updated_bin/*")
        exit(0)


    #copy_command = "sudo cp " + path.join(r"/home/pi/IVZ/bin/updated_bin", files[0].split('.enc')[0]) + " /home/pi/"
    #print("UPDATE")

    #print(path.join(r"/home/pi/IVZ/bin/updated_bin", files[0].split('.enc')[0]))

    system("sudo rm /home/pi/IVZ/bin/ivzlauncher*")

    copy_cmd = "sudo cp " + path.join(r"/home/pi/IVZ/bin/updated_bin", files[0].split('.enc')[0]) + " /home/pi/IVZ/bin"
    system(copy_cmd)
    #print(copy_cmd)

    soft_link_cmd = "sudo ln -s " + path.join("/home/pi/IVZ/bin", files[0].split('.enc')[0]) + " /home/pi/IVZ/bin/ivzlauncher"
    system(soft_link_cmd)
    #print(soft_link_cmd)

    system("sudo rm /home/pi/IVZ/bin/updated_bin/*")

else:
    print("CONTINUE")
