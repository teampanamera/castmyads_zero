#!/bin/bash

CONFIG_COUNT=$(cat /boot/config.txt | wc -l)

if [ "$CONFIG_COUNT" = 2 ]; then
    echo "Something went wrong with config.txt !!! Copying again ..."
    sudo cp /home/pi/IVZ/scripts/config.txt /boot/config.txt
    sudo reboot
fi

export DISPLAY=:0
sudo X :0 -nocursor -noreset &
#sleep 2
#sudo xset s off
#sudo xset -dpms
#sudo xset s noblank
#sudo metacity &

python /home/pi/IVZ/scripts/verify_hdmi_mode.py

cd /home/pi/IVZ/bin/

#start the boot animation and coninue
/home/pi/IVZ/scripts/boot_animation.sh &

# Delete the for previous run
if [ -e /home/pi/IVZ/logs/IVZ_1.log ]
then
  sudo rm /home/pi/IVZ/logs/IVZ_1.log
fi

# Delete the dmesg log if any
if [ -e /home/pi/IVZ/logs/dmesg.log ]
then
  sudo rm /home/pi/IVZ/logs/dmesg.log
fi

# Delete the zipped log file if any
if [ -e /home/pi/IVZ/logs/InfoWiz_Diagnostic.enc ]
then
  sudo rm /home/pi/IVZ/logs/InfoWiz_Diagnostic.enc
fi

if [ -e /home/pi/IVZ/logs/IVZ.log ]
then
  cp /home/pi/IVZ/logs/IVZ.log /home/pi/IVZ/logs/IVZ_1.log
  sudo rm /home/pi/IVZ/logs/IVZ.log 
fi

# Check if we have to update the binary
python /home/pi/IVZ/IVZEXTERN/binaryVerification.py

sleep 1

#echo "dnsmasq not running .... Trying to start"
sudo /home/pi/IVZ/scripts/start_ap_wlan0.sh 
res=$(sudo service --status-all | grep "dnsmasq")

while :
do
  echo $res
  if [[ "$res" != *dnsmasq* ]]; then
    echo "dnsmasq not running .... Trying to start"
    sudo service dnsmasq start
    res=$(sudo service --status-all | grep "dnsmasq")
  else
    break
  fi
done

cd /home/pi/IVZ/bin/

sudo pkill -f boot_animation.sh
sudo pkill -f omxplayer

sudo nice -n -20 ./ivzlauncher > /home/pi/IVZ/logs/IVZ.log 2>&1
#sudo nice -n 20 script -c ./ivzlauncher -f IVZ.log > /dev/null
