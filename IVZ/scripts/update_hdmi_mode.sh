#!/bin/bash
STATUS=
CONFIG=/home/pi/config.txt

# Copy the file to /tmp first
sudo cp /boot/config.txt $CONFIG

#echo "Printing /home/pi/config.txt file : "
#cat $CONFIG >> /home/pi/IVZ/logs/hdmi.log

HDMI_GROUP=
HDMI_MODE=

# $1 is hdmi_group and $2 is hdmi_mode

if [ "$1" = '' ]; then
	echo "Pass hdmi_group as first parameter"
	exit 0
else
	HDMI_GROUP=hdmi_group=$1
        echo "Received hdmi group : $1"
fi

if [ "$2" = '' ]; then
    echo "Pass hdmi_mode as second parameter"
	exit 0
else
	HDMI_MODE=hdmi_mode=$2
        echo "Received hdmi mode : $2"
fi

NEED_REBOOT=0

if [ -f "$CONFIG" ]; then
    # Process hdmi_group
    if grep -q "hdmi_group=" ${CONFIG}; then
        echo "Found hdmi_group"
        RES_GROUP=$(grep "hdmi_group=" ${CONFIG})
        arr=(${RES_GROUP//hdmi_group=/ })
        echo "Printing array"
        echo $arr
        if [ $1 == "$arr" ]; then
            echo "hdmi_group is $1 . Same as requested mode"
        else
            echo "Updating hdmi group."
	    sed -i "s/hdmi_group=.*/${HDMI_GROUP}/" ${CONFIG}
            NEED_REBOOT=1
        fi
    else
        echo "hdmi_group not present in config. Appending now ..."
        echo ${HDMI_GROUP} >> ${CONFIG}
        NEED_REBOOT=1
    fi


	# Process hdmi_mode
    if grep -q "hdmi_mode=" ${CONFIG}; then
        echo "Found hdmi_mode"
        RES_MODE=$(grep "hdmi_mode=" $CONFIG)

        arr=(${RES_MODE//hdmi_mode=/ })
        echo "Printing array"
        echo $arr

        if [ $2 == "$arr" ]; then
            echo "hdmi_mode is $2 . Same as requested mode"
        else
            echo "Updating hdmi mode."
	    sed -i "s/hdmi_mode=.*/${HDMI_MODE}/" ${CONFIG}
            NEED_REBOOT=1
        fi

    else
        echo "hdmi_mode not present in config. Appending now ..."
        echo ${HDMI_MODE} >> ${CONFIG}
        NEED_REBOOT=1
    fi


    sudo cp $CONFIG /boot/config.txt

    if [ $NEED_REBOOT == 1 ]; then
        echo "Rebooting now"
        sudo reboot
    fi

else
  echo "$CONFIG does not exists ! Exiting now ..."
  exit 0
fi
