# bash ssid-change.sh stratux-100
STATUS=
BOOT_CONFIG=/boot/config.txt

DISPLAY_ROTATION=
if [ "$1" = '' ]; then
	STATUS="FAILED"
	exit 0
else
	DISPLAY_ROTATION=\#display_rotate=$1
fi

if [ -f "$BOOT_CONFIG" ]; then

    if grep -q "^#display_rotate=" ${BOOT_CONFIG}; then
        sed -i "s/^#display_rotate=.*/${DISPLAY_ROTATION}/" ${BOOT_CONFIG}
    else
        echo ${DISPLAY_ROTATION} >> ${BOOT_CONFIG}
    fi

    STATUS="PASSED"
else
    STATUS="FAILED"
fi

echo $STATUS
