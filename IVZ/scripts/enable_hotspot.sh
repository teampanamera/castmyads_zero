#!/bin/bash

network_packages="dnsmasq dhcpcd hostapd openssh-server"

# This function will install package
install_packages() {

	for package in ${network_packages[@]}; do
        	PKG_OK=$(dpkg-query -W --showformat='${Status}\n' $package | grep "install ok installed")
        	if [ "" == "$PKG_OK" ]; then
          		echo "$package is not installed.\nInstalling now ..."
          		sudo apt-get --assume-yes --allow-change-held-packages install --fix-missing $package
        	else
          		echo "$package already installed !!!"
        	fi
	done
}

config_hostapd_deamon_conf(){

    if [ -e /etc/default/hostapd ]
    then
       echo "/etc/default/hostapd exists !"
       echo "Cheking if DEAMON_CONF is set"
       cat /etc/default/hostapd | grep "DAEMON_CONF=\"/etc/hostapd/hostapd.conf\"" &> /dev/null
       if [ $? == 0 ]; then
          echo "DEAMON_CONF is already set !"
       else
          echo "Setting up DEAMON_CONF now ..."
          # tell hostapd where to find the config file
          echo DAEMON_CONF="/etc/hostapd/hostapd.conf" >> /etc/default/hostapd
       fi
    else
       echo "/etc/default/hostapd does not exists !\nCreating now"
       echo DAEMON_CONF="/etc/hostapd/hostapd.conf" >> /etc/default/hostapd
    fi
}

set_udev_rules(){

	MAC_ADDRESS="$(cat /sys/class/net/wlan0/address)"

	# Populate `/etc/udev/rules.d/70-persistent-net.rules`
	if [ -e /etc/udev/rules.d/70-persistent-net.rules ]; then
		echo "/etc/udev/rules.d/70-persistent-net.rules exists !"
		echo "Cheking if RUN is set"
		cat /etc/default/hostapd | grep "RUN+=\"/bin/ip link set ap0 address ${MAC_ADDRESS}\"" &> /dev/null
		if [ $? == 0 ]; then
			echo "RUN is already set !"
		else
			echo "Setting up RUN now ..."
sudo bash -c 'cat > /etc/udev/rules.d/70-persistent-net.rules' << EOF
SUBSYSTEM=="ieee80211", ACTION=="add|change", ATTR{macaddress}=="${MAC_ADDRESS}", KERNEL=="phy0", \
  RUN+="/sbin/iw phy phy0 interface add ap0 type __ap", \
  RUN+="/bin/ip link set ap0 address ${MAC_ADDRESS}"
EOF
		fi
	else
		echo "/etc/udev/rules.d/70-persistent-net.rules does not exists ! Creating now"
sudo bash -c 'cat > /etc/udev/rules.d/70-persistent-net.rules' << EOF
SUBSYSTEM=="ieee80211", ACTION=="add|change", ATTR{macaddress}=="${MAC_ADDRESS}", KERNEL=="phy0", \
  RUN+="/sbin/iw phy phy0 interface add ap0 type __ap", \
  RUN+="/bin/ip link set ap0 address ${MAC_ADDRESS}"
EOF
	fi
}

enable_ssh(){

	systemctl list-unit-files | grep enabled | grep "ssh.service" &> /dev/null
	if [ $? == 0 ]; then
   		echo "ssh is alredy enabled !"
	else
   		echo "Enabling ssh now ..."
   		sudo service ssh start
   		sudo systemctl enable ssh
	fi
}

add_startup_nw_config(){

	CHECKCRONTAB=$(crontab -l | grep "start_ap_wlan0.sh")
	if [ "" == "$CHECKCRONTAB" ]; then
  		echo "Cronjob is not defined !!!"
  		# Create cronjob to make sure we launch ivz on every boot
  		(echo "@reboot /home/pi/IVZ/scripts/start_ap_wlan0.sh" ; crontab -l)| crontab -
	else
  		echo "Cronjob already defined !!!"
	fi
}

#install network dependencies
install_packages

#configure hostapd deamon configuration
config_hostapd_deamon_conf

# configure udev rules
set_udev_rules

cp /home/pi/IVZ/scripts/dnsmasq.conf.ivz /etc/dnsmasq.conf
cp /home/pi/IVZ/scripts/hostapd.conf.ivz /etc/hostapd/hostapd.conf
cp /home/pi/IVZ/scripts/wpa_supplicant.conf.ivz /etc/wpa_supplicant/wpa_supplicant.conf
cp /home/pi/IVZ/scripts/interfaces.ivz /etc/network/interfaces

# Enable ssh on every boot
enable_ssh

# Check whether cronjob is alredy defined for starting up AP and wlan0
#add_startup_nw_config

echo "Restarting in 6 seconds ..."
sleep 6
reboot
