#!/bin/bash

# Add your packages to be installed in the array
packages="dnsmasq dhcpcd hostapd openssh-server p7zip-full omxplayer indent libfreetype6-dev ttf-dejavu-core  libdevil-dev libdevil1c2 libfreeimage-dev"

##############################################################################
#							Local functions
##############################################################################

# This function will cross check whether required package is installed
verify_installed_package(){
	if ! grep -q $1 /home/pi/installed_packages.txt; then
        echo "Failed to install $1 package ..."
        return 0
	else
		return 1
    fi
}

# This function will cross check whether required packages are installed
verify_installed_packages(){

	# Declare an array of string with type
	
	result=1
	
	# remove older list of installed packages
	if [ -e /home/pi/installed_packages.txt ]; then
		sudo rm /home/pi/installed_packages.txt
	fi

	# get new list of installed packages
	sudo apt list --installed > /home/pi/installed_packages.txt

	# verify each package if it is installed
    if [ -e /home/pi/installed_packages.txt ]; then
	 
		# Iterate the string array using for loop
		for val in ${packages[@]}; do
			verify_installed_package $val
			result=$?
			if [ $result == 0 ]; then
				break
			fi
		done
		
	else
		result=0
	fi

    return $result
}

# This function will install package
install_package() {
	PKG_OK=$(dpkg-query -W --showformat='${Status}\n' $1 | grep "install ok installed")
	if [ "" == "$PKG_OK" ]; then
	  echo "$1 is not installed.\nInstalling now ..."
	  sudo apt-get --assume-yes --allow-change-held-packages install --fix-missing $1
	else
	  echo "$1 already installed !!!"
	fi
}

# This function will install all dependencies
install_packages() {

	sudo apt -y update
	sudo apt -y upgrade

	for val in ${packages[@]}; do
		install_package $val
	done
}

customize_boot_screen(){

	# Replace /boot/config.txt
	# This is to disable rainbow screen
	if [ -e /boot/config.txt ]; then
		sudo mv /boot/config.txt /boot/config.txt_backup
		sudo cp /home/pi/IVZ/scripts/config.txt /boot/config.txt
	else
		sudo cp /home/pi/IVZ/scripts/config.txt /boot/config.txt
	fi

	# Replace pix.script file
	# This is to remove text messages under splash image
	if [ -e /usr/share/plymouth/themes/pix/pix.script ]; then
		sudo mv /usr/share/plymouth/themes/pix/pix.script /usr/share/plymouth/themes/pix/pix.script_backup
		sudo cp /home/pi/IVZ/scripts/pix.script /usr/share/plymouth/themes/pix/pix.script
	fi

	# Replace cmdline.txt file
	# It does following things :
	# 1. Disable splash image
	# 2. Disable boot messages
	# 3. Remove raspbery pi logo from top left
	# 4. Removes blinking cursor
	if [ -e /boot/cmdline.txt ]; then
		tr -d '\n' < /boot/cmdline.txt > /tmp/cmdline_new.txt
		echo " logo.nologo vt.global_cursor_default=0 loglevel=0" >> /tmp/cmdline_new.txt
		sudo sed -i 's/tty1/tty3/g' /tmp/cmdline_new.txt
		sudo mv /boot/cmdline.txt /boot/cmdline.txt_backup
		sudo cp /tmp/cmdline_new.txt /boot/cmdline.txt
	fi

	# Disable the splash image, just in case above doesn't work
	if [ -e /usr/share/plymouth/themes/pix/splash.png ]; then
		sudo mv /usr/share/plymouth/themes/pix/splash.png /usr/share/plymouth/themes/pix/splash.png_backup
	fi
}

customize_desktop(){

	# Disable console autologin
	sudo ln -fs /lib/systemd/system/getty@.service /etc/systemd/system/getty.target.wants/getty@tty1.service
	sudo systemctl disable getty@tty1.service

	# Disable lightdm service
	sudo systemctl disable lightdm
}

infowiz_init(){
	# Check whether cronjob is alredy defined for infovizlauncher
	CHECKCRONTAB=$(crontab -l | grep "infovizlauncher.sh")
	if [ "" == "$CHECKCRONTAB" ]; then
	  echo "Cronjob is not defined !!!"
	  # Create cronjob to make sure we launch ivz on every boot
	  (crontab -l ; echo "@reboot /home/pi/IVZ/scripts/infovizlauncher.sh")| crontab -
	else
	  echo "Cronjob already defined !!!"
	fi

	# Initialize mac address file
	python /home/pi/IVZ/IVZEXTERN/init_mac.py

	# Copy hind font to system path and enable them
	# Hind font is necessary for Devnagari characters display in marquee text
	sudo cp /home/pi/IVZ/IVZEXTERN/fonts/* /usr/local/share/fonts/
	fc-cache -fv
}

cooling_fan_init(){
	
	# Copy and enable fan control script
	sudo cp /home/pi/IVZ/scripts/fanctrl.service /lib/systemd/system
	sudo systemctl enable fanctrl.service
}

##############################################################################
#							Start Point
##############################################################################

# Install dependencies
install_packages

# verify if all packages are installed correctly
verify_installed_packages
return_value=$?
if [ $return_value == 0 ]; then
    echo "Some of the necessary packages failed to install ..."
    echo "Exiting installation ..."
    exit 1
else
    echo "Necessary packages installed successfuly ..."
fi

# customize boot screen
customize_boot_screen

# customize desktop
customize_desktop

# initialize INFOWiZ
infowiz_init

# initialize cooling fan (raspbery pi 3B+ only)
#cooling_fan_init

# setup network interfaces
# enable_hotspot reboots the system
sudo /home/pi/IVZ/scripts/enable_hotspot.sh
