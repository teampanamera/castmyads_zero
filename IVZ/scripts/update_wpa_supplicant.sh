#!/bin/bash
# Script used to update ssid and psk in wpa supplicant file
# Usage :  update_ssid_password.sh NEW_SSID NEW_PSK

STATUS=
WPA_SUPPLICANT=/etc/wpa_supplicant/wpa_supplicant.conf
#WPA_SUPPLICANT=/home/pi/test/wpa_supplicant.conf

SSID=
if [ "$1" = '' ]; then
	STATUS="FAILED"
        echo $STATUS
        exit 0
else
	SSID=ssid=\""$1"\"
fi

if [ "$2" = '' ]; then
	STATUS="FAILED"
        echo $STATUS
	exit 0
else
	PSK=psk=\""$2"\"
fi


if [ -f "$WPA_SUPPLICANT" ]; then

    if grep -q "ssid=" ${WPA_SUPPLICANT}; then
	sed -i "s/^    ssid=.*/    ${SSID}/" ${WPA_SUPPLICANT}
    else
        STATUS="FAILED"
    fi

    if grep -q "psk=" ${WPA_SUPPLICANT}; then
	sed -i "s/^    psk=.*/    ${PSK}/" ${WPA_SUPPLICANT}
    else
        STATUS="FAILED"
    fi

    STATUS="PASSED"
else
    STATUS="FAILED"
fi

echo $STATUS
