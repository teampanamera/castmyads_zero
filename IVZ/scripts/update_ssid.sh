# Log in as root and copy this file then pass the
# desired ssid as a command line option, e.g.:
#
# bash ssid-change.sh stratux-100
#

STATUS=
HOSTAPD=/etc/hostapd/hostapd.conf
#HOSTAPD=hostapd.conf

SSID=
if [ "$1" = '' ]; then
	STATUS="FAILED"
	exit 0
else
	SSID=ssid=$1
fi

#### /etc/hostapd/hostapd.conf
####
if [ -f "$HOSTAPD" ]; then

    if grep -q "^ssid=" ${HOSTAPD}; then
        sed -i "s/^ssid=.*/${SSID}/" ${HOSTAPD}
    else
        echo ${SSID} >> ${HOSTAPD}
    fi

    STATUS="PASSED"
else
    STATUS="FAILED"
fi

echo $STATUS
