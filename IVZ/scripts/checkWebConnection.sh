#!/bin/bash

RES=$(grep ssid /etc/wpa_supplicant/wpa_supplicant.conf)

if [ "ssid=\"\"" == $RES ]; then
    #echo "wpa supplicant is empty"
    echo "NOT_CONNECTED"
else
    #echo "wpa supplicant is not empty"
    IP_ADD=$(ip r | grep default | cut -d ' ' -f 3)

    if [ "" == "$IP_ADD" ]; then
	echo "NOT_CONNECTED"
    else
        ping -q -w 1 -c 1 $IP_ADD > /dev/null && echo CONNECTED || echo NOT_CONNECTED
    fi
fi



