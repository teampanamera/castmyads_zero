from os import system, path
from sys import exit
from subprocess import Popen, PIPE

if path.exists('/tmp/edid.dat'):
    # if edid.dat already exists, delete it first
    command = "sudo rm /tmp/edid.dat"
    system(command)

# Run the tvservice command to output the result to a file.
command = "sudo tvservice -d /tmp/edid.dat"
system(command)

if not path.exists('/tmp/edid.dat'):
    print("verify_hdmi_mode : Failed to create edid.dat")
    exit(0)

# Pipe the file to edidparser to generate a readable text file
command = "sudo edidparser /tmp/edid.dat > /tmp/edid.txt"
system(command)

if not path.exists('/tmp/edid.txt'):
    print("verify_hdmi_mode : Failed to create edid.txt")
    exit(0)


found = False
latest_hdmi_group = 0
latest_hdmi_mode  = 0

with open('/tmp/edid.txt') as f:

    for line in f:

        if "preferred mode remained as" in line:

            if line.split(" preferred mode remained as ")[1].split(' ')[0] == 'CEA':

                latest_hdmi_group = 1

            elif line.split(" preferred mode remained as ")[1].split(' ')[0] == 'DMT':

                latest_hdmi_group = 2
            else:
                exit(0)

            #latest_hdmi_mode = line.split(" preferred mode remained as ")[1].split(' ')[1][1]

            latest_hdmi_mode = line.split(" preferred mode remained as ")[1].split(' ')[1]

            latest_hdmi_mode = latest_hdmi_mode.replace('(','')
            latest_hdmi_mode = latest_hdmi_mode.replace(')','')

            found = True
            break

if latest_hdmi_group == 0 or latest_hdmi_mode == 0:
    print("verify_hdmi_mode : Failed to find hdmi_mode or hdmi_group in edid.txt")
    exit(0)

if found is False:
    print("verify_hdmi_mode : Failed to find preferred mode in edid.txt")
    exit(0)


# Invoke the script to actually update hdmi_group and hdmi_mode
command = 'sudo /home/pi/IVZ/scripts/update_hdmi_mode.sh ' + str(latest_hdmi_group) + ' ' + str(latest_hdmi_mode)
system(command)
#print(command)
#Popen([command], shell=True, stdout=PIPE)
