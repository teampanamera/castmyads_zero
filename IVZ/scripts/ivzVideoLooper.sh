#!/bin/bash

while getopts ":a:b:c:d:p:i:o:" itr; do
    case "${itr}" in
        a)
            a=${OPTARG}
            ;;
        b)
            b=${OPTARG}
            ;;
        c)
            c=${OPTARG}
            ;;
        d)
            d=${OPTARG}
            ;;
        p)
            p=${OPTARG}
            ;;
        i)
            i=${OPTARG}
            ;;
        o)
            o=${OPTARG}
            ;;
        *)
            echo "IVZ Video Looper : Invalid Command Line Arguments"
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${a}" ] || [ -z "${b}" ] || [ -z "${c}" ] || [ -z "${d}" ] || [ -z "${p}" ] || [ -z "${i}" ] || [ -z "${o}" ]; then
    echo "ERROR Playing Video"
fi

while [ $i -gt 0 ]; do
    command="sudo nice -n -20 omxplayer --orientation $o --no-osd --win \"$a $b $c $d\" \"$p\""
    eval $command > /dev/null
    let i=i-1
done
