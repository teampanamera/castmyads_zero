#!/bin/bash

ROTATE=$(sed -n 's/#display_rotate=\(.*\)/\1/p' < /boot/config.txt)
ORIENTATION=0

if [ "1" == $ROTATE ]; then
	ORIENTATION=90
elif [ "2" == $ROTATE ]; then
	ORIENTATION=180
elif [ "3" == $ROTATE ]; then
	ORIENTATION=270
else
	ORIENTATION=0
fi

omxplayer --no-osd --orientation $ORIENTATION --layer 2 /home/pi/IVZ/IVZEXTERN/boot_animation.mp4 &
sleep 13
omxplayer --loop --no-osd --orientation $ORIENTATION --layer 1 /home/pi/IVZ/IVZEXTERN/start_wait_animation.mp4

