# Log in as root and copy this file then pass the
# desired ssid as a command line option, e.g.:
#
# bash ssid-change.sh stratux-100
#

STATUS=
HOSTAPD=/etc/hostapd/hostapd.conf
#HOSTAPD=hostapd.conf

PASSWORD=
if [ "$1" = '' ]; then
    echo "FAILED"
	STATUS="FAILED"
	exit 0
else
	PASSWORD=wpa_passphrase=$1
fi

#### /etc/hostapd/hostapd.conf
####
if [ -f "$HOSTAPD" ]; then

    if grep -q "^wpa_passphrase=" ${HOSTAPD}; then
        sed -i "s/^wpa_passphrase=.*/${PASSWORD}/" ${HOSTAPD}

        STATUS="PASSED"
    else
        STATUS="FAILED"
    fi

else
    STATUS="FAILED"
fi

echo $STATUS
