from IVZSCHEDULER import ivzDataStructures
from IVZPARSER import ivzxmlparser
#from IVZGUI import ivzgui, ivzanimations
from IVZGLOBALS import ivzConstants
from IVZMGR import ivzProjectManager

try:
    from Tkinter import Canvas
except ImportError:
    from tkinter import Canvas

from PIL import Image
from os import path, system, listdir
from glob import glob

def ivzThumbnailProcessLauncher(thumbnailRequestQueue):
    """
    Purpose    : Landing function for thumbnail creation process
    Parameters : thumbnailRequestQueue - queue for reading requests
    """

    # Check whether there are projects in display queue for which thumbnail is not created
    # Put such projects in queue and launch them at startup itself
    result = ivzProjectManager.getDisplaySequence()

    if result != None:

        for i in result:

            pname = i[0]

            # If project dir in thumbnail path does not exists, put it in queue
            if not path.exists(path.join(ivzConstants.ivz_thumbnails_dir, pname)):
                thumbnailRequestQueue.put(pname)
            else:
                # Directory exists, but thumbnail does not. Put it in queue
                if not path.exists(path.join(ivzConstants.ivz_thumbnails_dir, pname, 'thumbnail.mp4')):
                    thumbnailRequestQueue.put(pname)

    # This is a dummy empty call to ensure that updates to the queue are flushed
    thumbnailRequestQueue.empty()

    while True:

        projectName = thumbnailRequestQueue.get()
       
        if not path.exists(path.join(ivzConstants.projects_dir, projectName)):

            if ivzConstants.ivz_debug_level >= 1:
                print("ivzThumbnailProcessLauncher : Project does not exists :  " + str(projectName))

            continue

        if ivzConstants.ivz_debug_level >= 3:
            print("ivzThumbnailProcessLauncher : Creating thumbnails for : " + str(projectName))

        ivzCreateThumbnails(projectName)

def ivzCreateThumbnails(project):
    """
        Purpose : Generate thumbnail GIF/video for the given project
        Paramaterts : 
                project - Name of the project
    """

    # parse project configuration file
    projectResources = ivzxmlparser.parseProjectConfiguration(path.join(ivzConstants.projects_dir, project, 'project_configuration.xml'))

    #print("ivzCreateThumbnails : projectResources = \n" + str(projectResources))


    # result of parsing configuration xml looks like following :
    # [{'screen0': [('IMAGE', 'image0')]}, {'screen1': [('VIDEO', 'video0')]}, {'screen2': [('IMAGE', 'image1'), ('VIDEO', 'video1')]}, {'screen2': [('IMAGE', 'image1'), ('VIDEO', 'video1')]}]

    projectObject = ivzDataStructures.ivzProjectData(project)

    xsize, ysize = ivzxmlparser.getWindowSize(path.join(ivzConstants.projects_dir, project, 'project.xml'))

    projectObject.setDimensions(xsize, ysize)

    for screen in projectResources:

       screenName = screen.keys()[0]

       #print(screenName)

       projectObject.screensOrderedList.append(screenName)

       projectObject.addScreen(screenName)

       widgetList = screen[screenName]

       for w in widgetList:

           widgetName = w[1]

           widgetXML = path.join(ivzConstants.projects_dir, project, 'metadata', widgetName+'.xml')

           if w[0] == 'IMAGE':

               #print('Image widget : ' + str(widgetName))

               bgcolor, x, y, width, height, imageResource = ivzxmlparser.parseImageWidget(widgetXML)

               imageResource = path.join(ivzConstants.projects_dir, project, 'images', imageResource)

               if not path.exists(str(imageResource)):
                   continue

               imageWidgetObject = ivzDataStructures.ivzImageWidget(widgetName, width, height, x, y, bgcolor, imageResource)

               projectObject.addWidgetToScreen(screenName, imageWidgetObject)

           elif w[0] == 'VIDEO':

               #print('Video widget : ' + str(widgetName))

               bgcolor, x, y, width, height, videoResource, iterations = ivzxmlparser.parseVideoWidget(widgetXML)

               videoResource = path.join(ivzConstants.projects_dir, project, 'videos', videoResource)

               if not path.exists(str(videoResource)):
                   continue

               videoWidgetObject = ivzDataStructures.ivzVideoWidget(widgetName, width, height, x, y, bgcolor, videoResource)

               videoWidgetObject.setIterations(iterations)

               projectObject.addWidgetToScreen(screenName, videoWidgetObject)

    #for screen in project.screens.keys():
    for screen in projectObject.screensOrderedList:

        # Set the background color for this creen
        color = ivzxmlparser.getScreenBackgroundColor(path.join(ivzConstants.projects_dir, project, 'metadata', screen+'.xml'))
        projectObject.setScreenBackgroundColor(screen, str(color))


    createThumbnailVideo(projectObject)


def createThumbnailVideo(project):
    """
    Purpose : generates images for every screen in project
    Params : projectObject which is populated using XML files
    """

    # If scratch directory for this project already exists somehow, remove it first
    # This is to avoid any overwrite warning messages we might get if we dont delete it
    if path.exists(path.join(ivzConstants.ivz_thumbnails_scratch, str(project.name))):
        #print("Cleaning directory before starting")
        cleancommand = "rm -rf " + "\'" + str(path.join(ivzConstants.ivz_thumbnails_scratch, str(project.name))) + "\'"
        system(cleancommand)

    if not path.exists(str(path.join(ivzConstants.ivz_thumbnails_scratch, str(project.name)))):
        # Create a temporary directory to work with
        command = 'mkdir -p ' + "\'"  + str(path.join(ivzConstants.ivz_thumbnails_scratch, str(project.name))) + "\'"
        system(command)

    #print("project.screensOrderedList = \n" + str(project.screensOrderedList))
    #print("project.screenBackgroundColors = \n" + str(project.screenBackgroundColors))

    for screen in project.screensOrderedList:

        # Create a temporary directory for this screen
        screenPath = str(path.join(ivzConstants.ivz_thumbnails_scratch, str(project.name), str(screen)))
        command = 'mkdir ' + "\'"  + screenPath + "\'"
        #print(command)
        system(command)

        dim = str(project.xSize) + 'x' + str(project.ySize)

        # Commenting this code
        # Tkinter windo is not required for thumbnail creation code

        '''
        win = ivzgui.ivzWindow(True)
        win.setwindowdimensions(dim)
        win.setbackgroundcolor(project.screenBackgroundColors[screen])
        win.withdraw()
        '''

        bgcolor = '#' + project.screenBackgroundColors[screen] 

        #print(type(project.xSize)) 
        #print(type(project.ySize))

        rootImage  = Image.new("RGB", (int(project.xSize), int(project.ySize)), bgcolor)

        widgetList = project.screens[screen]

        hasVideo = False
        vidDuration = 0

        for widget in widgetList:

            if isinstance(widget, ivzDataStructures.ivzVideoWidget):

                if not path.exists(widget.mediaPath):
                    continue

                hasVideo = True
                vidDuration = ivzxmlparser.getVideoDuration(path.join(ivzConstants.projects_dir, project.name, 'metadata', 'media_resources.xml'),widget.mediaPath)
                break

        if hasVideo == False:

            for widget in widgetList:

                wImage = Image.open(widget.mediaPath)
                resizedImage = wImage.resize((int(widget.width), int(widget.height)), Image.ANTIALIAS)

                rootImage.paste(resizedImage, (int(widget.X), int(widget.Y)))

                wImage.close()
                resizedImage.close()
        
            # Save the image
            filename = screen + '.jpg'
            filepath = path.join(screenPath, filename)
            #rootImage.save(filepath)
            resizedRoot = rootImage.resize((320,240), Image.ANTIALIAS)
            resizedRoot.save(filepath)

            # Create a video file from the saved image
            videoName = path.join(screenPath, 'thumbnail.mp4')
            #videoCreateCommand = "ffmpeg -r 0.5 -f image2 -i " + str(filepath) + " -vcodec libx264 -crf 25 -pix_fmt yuv420p " + videoName
            videoCreateCommand = "ffmpeg -loop 1 -t 2 -f image2 -i " + "\'" + str(filepath) + "\'" + " -vcodec libx264 -crf 25 -pix_fmt yuv420p " + "\'" + videoName + "\'" + " > /dev/null 2>&1"
            system(videoCreateCommand)
            # delete the thumbnail file
            delCommand = 'rm ' + "\'" + str(filepath) + "\'"
            system(delCommand)
        else:
            # in case of video we have to treat thumbnail generation differently

            for widget in widgetList:

                if isinstance(widget, ivzDataStructures.ivzImageWidget):

                    wImage = Image.open(widget.mediaPath)
                    resizedImage = wImage.resize((int(widget.width), int(widget.height)), Image.ANTIALIAS)

                    rootImage.paste(resizedImage, (int(widget.X), int(widget.Y)))

                    wImage.close()
                    resizedImage.close()

                    #win.addframe(widget.width, widget.height, widget.X, widget.Y, widget.mediaPath)

            for widget in widgetList:

                if isinstance(widget, ivzDataStructures.ivzVideoWidget):

                    command = "ffmpeg -i " + "\'"  + widget.mediaPath + "\'" + " -vf fps=1 " + "\'" + str(path.join(screenPath, 'thumb%04d.jpg')) + "\'"  + " -hide_banner > /dev/null 2>&1"

                    # Hopefully this is a waiting call
                    system(command)

                    p = path.join(screenPath, "*")

                    imageList = sorted(glob(p), key=path.getctime)

                    cnt = 1
                     
                    for image in imageList:

                        wImage = Image.open(image)
                        resizedImage = wImage.resize((int(widget.width), int(widget.height)), Image.ANTIALIAS)

                        rootImage.paste(resizedImage, (int(widget.X), int(widget.Y)))

                        wImage.close()
                        resizedImage.close()

                        deleteCommand = "rm " + "\'" + str(image) + "\'"
                        system(deleteCommand)

                        #filename = 'IMAGE' + str(cnt)  + '.jpg'
                        #filepath = path.join(screenPath, filename)
                        #print(filepath)
                        #rootImage.save(image)

                        resizedRoot = rootImage.resize((320,240), Image.ANTIALIAS)
                        resizedRoot.save(image)

                        resizedRoot.close()

                        cnt = cnt + 1

            cnt = cnt - 1
            # since we want the video to be of length 2 seconds
            fps = cnt / 2.0

            # Create a video file from the saved image
            videoName = path.join(screenPath, 'thumbnail.mp4')
            p = path.join(screenPath,'thumb%04d.jpg')
            #print(p)
            videoCreateCommand = "ffmpeg -r " + str(fps) + " -f image2 -i " + "\'" + str(p) + "\'" + " -vcodec libx264 -crf 25 -pix_fmt yuv420p " + "\'" + videoName + "\'" + " > /dev/null 2>&1"
            #print(videoCreateCommand)
            system(videoCreateCommand)

            '''

            t = path.join(screenPath, '*.jpg')

            if path.exists(screenPath):

                if glob(screenPath) != []:

                    print("ERROR : " + str(listdir(screenPath)))

                    #if listdir(screenPath) != []:
                    # delete the thumbnail file
                    delCommand = 'rm ' + "\'" + str(t) + "\'"
                    print(delCommand)
                    #print(delCommand)
                    system(delCommand)
            '''

        rootImage.close()
        rootImage = None


    vidlistfile = str(path.join(ivzConstants.ivz_thumbnails_scratch, str(project.name), 'videolist.txt'))
    screenVideoList = open(vidlistfile, "a")

    for screen in project.screensOrderedList:

        # Create a temporary directory for this screen
        screenVidPath = str(path.join(ivzConstants.ivz_thumbnails_scratch, str(project.name), str(screen), 'thumbnail.mp4'))

        if path.exists(screenVidPath):
            text = "file " + "'"  + str(screenVidPath) + "'"  + '\n'
            screenVideoList.write(text)

    screenVideoList.close()

    # merge the videos into one
    mergeCommand = "ffmpeg -f concat -safe 0 -i " + "\'" + str(vidlistfile) + "\'" + " -c copy " + "\'" + str(path.join(ivzConstants.ivz_thumbnails_scratch, str(project.name), 'thumbnail.mp4')) + "\'" + " > /dev/null 2>&1"
    #print(mergeCommand)
    system(mergeCommand)

    # delete the vidlist file
    #system("rm " + vidlistfile)

    source_path = str(path.join(ivzConstants.ivz_thumbnails_scratch, str(project.name), 'thumbnail.mp4'))
    target_path = str(path.join(ivzConstants.ivz_thumbnails_dir, str(project.name), 'thumbnail.mp4'))
    thumbnail_path = str(path.join(ivzConstants.ivz_thumbnails_dir, str(project.name)))

    # If thumbnail directory for this project does not exists, create one
    #if not path.exists(target_path):
    if not path.exists(thumbnail_path):
        command = 'mkdir ' + "\'" + str(path.join(ivzConstants.ivz_thumbnails_dir, str(project.name))) + "\'"
        system(command)

    if path.exists(source_path):
        #move the created thumbnail
        movecommand = 'mv ' + "\'" + source_path + "\'" + ' ' + "\'" + target_path + "\'"
        system(movecommand)
    else:
        if ivzConstants.ivz_debug_level >= 1:
            print("createThumbnailVideo : thumbnail creation failed for : " + str(project.name))

    if path.exists(str(path.join(ivzConstants.ivz_thumbnails_scratch, str(project.name)))):
        #cleanup
        cleancommand = 'rm -rf ' + "\'" + str(path.join(ivzConstants.ivz_thumbnails_scratch, str(project.name))) + "\'"
        system(cleancommand)


#ivzCreateThumbnails('onlyImage.ivzproj')
#ivzCreateThumbnails('10sec.ivzproj')
#ivzCreateThumbnails('allinone.ivzproj')

