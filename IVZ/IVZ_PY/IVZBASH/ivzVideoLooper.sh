#!/bin/bash
count=$6
while [ $count -gt 0 ]; do
    command="nice -n -20 omxplayer --no-osd --win \"$1 $2 $3 $4\" $5"
    eval $command
    let count=count-1
done
