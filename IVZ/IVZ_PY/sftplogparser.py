import os
import time

filepath = r'/home/pi/sftp.log'

#file = open(filepath, "r")

openfiles = []

with open(filepath, "r") as f:
    for line in f:
        if ": open " in line:

            name = line.split("]:")[1].split(" ")[2].split("\"")[1]

            openfiles.append(name)

        if ": close " in line:

            name = line.split("]:")[1].split(" ")[2].split("\"")[1]

            if name in openfiles:
                openfiles.remove(name)


files = list(set(openfiles))

#print(openfiles)

if len(files) == 0:
    print("TRUE")

else:

    # TODO : Make sure there is a deamon in place which cleans up the sftp.log file when there is no upload in progress

    flag = False

    for it in files:

        if os.path.isfile(it) == False:
            continue

        before = os.stat(it).st_size
        time.sleep(1)
        after = os.stat(it).st_size

        if before != after:
            flag = True
            print("FALSE")
            break

    if flag == False:
        print("TRUE")
