#import os
from os import path, system
from uuid import getnode

'''
    This file provides first level authentication for InfoViz.
    It needs to be run, as a part of InfoViz installation process, otherwise IVZ will not work
'''

if path.exists("/home/pi/mac.txt"):
    system("rm /home/pi/mac.txt")

f = open("/home/pi/mac.txt", "w")

mac = str(getnode())

f.write(mac)

f.close()

print("MAC Address data initialized")
#f = open("/home/pi/mac.txt", "r")
#print(f.read())
