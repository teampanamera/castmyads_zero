''' Tk_Canvas_ticker1.py
using a Tkinter canvas to create a marquee/ticker via
canvas.create_text(x, y, anchor='nw', text=s, font=font)
canvas.move(object, x_increment, y_increment)
tested with Python27 and Python33  by  vegaseat  04oct2013
'''

from time import time, sleep
#from sys import exit
from os import path

try:
    # Python2
    import Tkinter as tk
except ImportError:
    # Python3
    import tkinter as tk

from IVZSCHEDULER import ivzScheduleAndDisplay
from IVZGLOBALS import ivzConstants

def findOptimumFontSize(root, canvas_width, canvas_height):

    # Initliaze variables to minimum required values
    font_size = 6
    y_size_in_pixels = 20

    canvas = tk.Canvas(root, height=canvas_height, width=canvas_width, bg="yellow")

    while y_size_in_pixels < canvas_height:

        #font = ('courier', font_size, 'bold')
        font = ('hind-bold', font_size, 'bold')

        text = canvas.create_text(1, 2, anchor='nw', text="ABCD", font=font)

        box = canvas.bbox("all")

        y_size_in_pixels = box[3] - box[1]

        font_size = font_size + 1

        canvas.delete("all")

    return font_size - 1


def launchMarqueeText():

    displayDim = ivzScheduleAndDisplay.queryDisplayResolution()

    canvas_width  = int(float(displayDim.split(' ')[0]))
    screen_height = int(displayDim.split(' ')[1])
    canvas_height = int(screen_height / 18)

    #canvas_height = 60
    #canvas_width = 1280

    root = tk.Tk()

    location = str(canvas_width) + 'x' + str(canvas_height) + '+' + '0' + '+' + str(screen_height - canvas_height)
    root.geometry(location)

    #print(location)

    root.overrideredirect(True)
    canvas = tk.Canvas(root, height=canvas_height, width=canvas_width, bg="yellow")
    canvas.pack()

    optimal_font_size = findOptimumFontSize(root, canvas_width, canvas_height)

    #print("Suggested font size = " + str(optimal_font_size))

    font = ('hind-bold', optimal_font_size, 'bold')

    #s = "We don't really care why the chicken crossed the road. We just want to know if the chicken is on our side of the road or not. The chicken is either for us or against us. There is no middle ground here.  (George W. Bush)"
    #s = "HelloWorld"
    #s = " Welcome to Panamera Technologies... We are pleased to announce the launch of our product InfoWiz (Information Broadcasting Wizard) !!! For more information visit www.panameratech.com/infowiz or call us on 9742591552 ! Have a great day !!! "
    s = None

    # Read marquee text from text file
    with open(path.join(ivzConstants.ivz_marquee_dir,'message.txt'), 'r') as content_file:
        s = content_file.read().decode("utf-8")

    #s = s.encode('utf-8')

    if s is None:
        s = "Welcome !!!"

    # string has to be minimum 20 chars. Else add padding.
    if len(s) < 20:

        padding = 20 - len(s)
        padding = int(padding / 2)
        s = ' '.encode('utf-8')*padding + s + ' '.encode('utf-8')*padding

    x = 1
    y = 2

    text = canvas.create_text(x, y, anchor='nw', text=s, font=font)

    dx = 1
    dy = 0  # use horizontal movement only

    canvas.move(text, canvas_width, dy)

    # Get bounding box of canvas text
    # This is important to find the size in pixels for marquee reset operation
    box = canvas.bbox("all")

    #print(box)

    x_size_in_pixels = box[2] - box[0]

    def updateText():

        # move text object by increments dx, dy
        # -dx --> right to left
        canvas.move(text, -dx, dy)

        if canvas.coords(x)[0] < -x_size_in_pixels:

            #print("Moving the text now ...")
            #print(canvas.coords(x))

            canvas.move(text, x_size_in_pixels + canvas_width, dy)

        #canvas.update()
        canvas.after(8, updateText)

    canvas.after(8, updateText)
    root.mainloop()

#launchMarqueeText()
