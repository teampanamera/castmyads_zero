from Tkinter import *
from PIL import Image, ImageTk
import time
from threading import Timer

flag = 0

'''
def test():
    print("Inside test")
    renderLoop(master)
    master.after(500, test)
'''

def renderLoop(master):

    if flag:
        print("return")
        return

    print("Inside render loop")

    #master.update_idletasks()
    #master.update()

    fade_away(master)

def fade_away(root):
    alpha = root.attributes("-alpha")
    print(alpha)
    if alpha > 0:
        alpha -= .02
        root.attributes("-alpha", alpha)
        root.after(32, renderLoop, root)
        #w.after(500, renderLoop, root)
    else:
        print("Destroying window")
        root.destroy()
        flag=1

master = Tk()
#master.geometry("1536x864")
master.wm_geometry("1280x720")
#master.wm_geometry("1920x1080")

master.overrideredirect(True)

frame = Frame(master, background="black")
#frame.place(height=300, width=300, x=0, y=0)
frame.place(height=300, width=300, x=980, y=420, anchor=NW)

#frame.pack(fill='both', expand='True')

w = Canvas(frame, width=300, height=300, bg="yellow")

img = Image.open("image6.png")
resized = img.resize((300, 300),Image.ANTIALIAS)
tatras = ImageTk.PhotoImage(resized)

w.create_image(0, 0, anchor="nw", image=tatras)
w.pack(fill=BOTH, expand=1)

# Uncomment this for fadeout animation
#renderLoop(master)

master.mainloop()

print("TEST ********************************")
