from threading import Timer
#from os import system
from time import sleep
#import psutil

# This file is a placeholder for all the animations we are going to develop in IVZ
from IVZGLOBALS import ivzConstants

class ivzAlphaFadingAnimation:
    """
        This class is used for Fade In and Fade Out animations on Tkinter Windows.
        It modifies the alpha value of window to create this animation
    """

    def __init__(self):
        return

    def schedulefadeOutAnimation(self, tkRootWindow, timeout, videoProcessStatus, vidProcess):
        """
        Purpose : Launches the fade out animation after timeout seconds
        :param tkRootWindow:
        :param timeout: time in seconds after which fadeout starts
        :return:
        """

        fadeout = Timer(timeout, self.fadeOutLauncher, [tkRootWindow, videoProcessStatus, vidProcess])
        fadeout.start()

    def fadeOutLauncher(self, tkRootWindow, videoProcessStatus, vidProcess):
        """
        Purpose : This is a landing function for fade out. We wait till video is done finishing using vidProcess
        :param tkRootWindow : Window to be animated
               vidProcess   : Process handle to video process
        """

        if videoProcessStatus == "RUNNING":

            if ivzConstants.ivz_debug_level >= 3:
                print("fadeOutLauncher : Going to wait for video to finish")

            # If there is a video in screen, wait for it to finish
            #message = vidPlaybackStatus.get()
            #message = vidPlaybackStatus.get(block=True, timeout=time)

            # Busy waiting
            while vidProcess.is_alive():
                continue

            if ivzConstants.ivz_debug_level >= 3:
                print("fadeOutLauncher : Done waiting for video to finish")


        alpha = tkRootWindow.attributes("-alpha")

        #print("fadeoutAnimation - " + str(alpha))

        def fadeAlpha(alpha):

            tkRootWindow.quit()

            '''

            if alpha < 0:

                if ivzConstants.ivz_debug_level >= 3:
                    print("fadeOutLauncher:fadeAlpha : End of fadeout animation")

                if ivzConstants.ivz_debug_level >= 3:
                    print("fadeOutLauncher:fadeAlpha : Quiting window")

                tkRootWindow.quit()

            else:

                tkRootWindow.attributes("-alpha", alpha)
                alpha -= .02

                #tkRootWindow.update()
                #tkRootWindow.update_idletasks()

                tkRootWindow.after(32, fadeAlpha, alpha)
            '''

        tkRootWindow.after(2, fadeAlpha, alpha)

        return

    def launchfadeInAnimation(self, tkRootWindow):
        """
        Purpose : Launcher function for fade in animation
        :param tkRootWindow:
        :return:
        """

        # TODO : Fadein animation for first launch of the display loop doesn't work
        # Root cause is following call to set alpha value to 0.0, does not work
        # This needs to be fixed
        tkRootWindow.attributes("-alpha", 0.0)
        self.fadeInAnimation(tkRootWindow)

    def fadeInAnimation(self, tkRootWindow):
        """
        Purpose : fade in animation for window.
        :param tkRootWindow: Window to be animated
        :return:
        """

        alpha = tkRootWindow.attributes("-alpha")

        def fadeAlpha(alpha):

            if alpha > 1.0:

                if ivzConstants.ivz_debug_level >= 3:
                    print("fadeInAnimation : End of fadein animation")

                if ivzConstants.ivz_debug_level >= 3:
                    print("fadeInAnimation : Quiting window")

            else:

                tkRootWindow.attributes("-alpha", alpha)
                alpha += .02

                tkRootWindow.after(32, fadeAlpha, alpha)

        tkRootWindow.after(2, fadeAlpha, alpha)

        return
