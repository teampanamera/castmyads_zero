from random import randint

from multiprocessing import Process
#import time
from subprocess import Popen, PIPE, check_output, CalledProcessError

from IVZMGR import ivzProjectManager
from IVZGLOBALS import ivzConstants

import shlex

'''
def renderOTA(key):

    # Open a process to query current dimensions of X screen
    p = Popen(['xdpyinfo | grep dimensions'], shell=True, stdout=PIPE)
    #print(dimensions.split(':')[1])
    #print(type(dimensions))
    dimensions = p.communicate()[0].split(':')[1].split('pixels')[0].strip(' ')

    # Logic to extract screen dimensions from above result
    xSize = int(dimensions.split('x')[0])
    ySize = int(dimensions.split('x')[1])

    xSize = xSize / 2
    ySize = ySize/ 6

    root = Tk()

    #root.wait_visibility(root)
    root.attributes('-alpha', 0.4)

    f = Frame(root, background='white')

    # Font size is same as frane height
    key = Label(f, text=key, font=("Helvetica", ySize))
    key.pack(fill=BOTH, expand=1)

    # Place at top left
    f.place(width=xSize, height=ySize, x=0, y=0)

    dStr = str(xSize) + "x" + str(ySize)

    root.wm_geometry(dStr)
    root.configure(background='white')

    # This is to remove window title bar
    root.overrideredirect(True)
    root.call('wm', 'attributes', '.', '-topmost', '1')

    mainloop()
'''

def displayOTA(otaKey):
    '''
    Purpose : Display OTA on top left corner of screen. On top of all the active windows for a duration of 10 seconds
    Parameter : Randomly generated 6 digit OTA key
    '''

    #p = Process(target=renderOTA, args=(otaKey,))
    #p.start()

    #time.sleep(6)

    #p.terminate()
    
    # 10 is the time in seconds to display the otKey
    params = "/opt/vc/src/hello_pi/hello_font/hello_font.bin " + str(otaKey) + " 10"
    p = Popen(params, shell=True, stdout=PIPE)

    #p.wait()
    result = p.communicate()[0]

    #os.system("/opt/vc/src/hello_pi/hello_font/hello_font.bin 12 6")

def verifyOTA(macAddress, key):
    '''
    Purpose : Fetch key for given MAC address and compare with provided key.
    Parameters : macAddress : mac address of the host machine requesting access to target
                 key : key entered by the user for verification purpose
    '''

    storedKey = ivzProjectManager.getHostEntry(macAddress)

    print("\nverifyOTA : OTA stored in database = " + storedKey)

    if storedKey == None:
        return False

    if storedKey == key:
        return True
    else:
        return False

def generateAndDisplayOTA(macAddress, otaKey):
    '''
    Purpose : Generate 6 digit random OTA key (One Time Authentication key) and display the same.
              Also push the MAC , Key pair in database. In case multiple host machines are requesting OTA, we need to distinguish between them.
              Hence the MAC address

    Parameter : MAC address of the host machine which is requesting OTA

    '''

    # Generate 6 digit random number between 111111 and 999999
    #otaKey = randint(111111,999999)

    #command = "/opt/vc/src/hello_pi/hello_font/hello_font.bin " + str(otaKey) + " 10"
    command = "/home/pi/IVZ/IVZEXTERN/OTA " + str(otaKey) + " 10"
    cm = shlex.split(command)
    print(cm)

    retry_count = 6

    while retry_count >= 0:

        p = Popen([command], shell=True, stdout=PIPE)
        res = p.communicate()[0]

        print(res)

        if res[-18:] == 'DISPLAYOTA:SUCCESS':

            # Launch new process for displaying OTA
            #displayProcess = Process(target=displayOTA, args=(otaKey,))
            #displayOTA(otakey)
            #displayProcess.start()

            # Push mac, OTA pair to database
            result = ivzProjectManager.insertHostEntry(macAddress, str(otaKey))

            if result == "SUCCESS":

                if ivzConstants.ivz_debug_level >= 1:
                    print('generateAndDisplayOTA : OTA generation successfull ...')

                break

            else:
                if ivzConstants.ivz_debug_level >= 1:
                    print('generateAndDisplayOTA : insertHostEntry failed ...')

        else:

            if ivzConstants.ivz_debug_level >= 1:
                print('generateAndDisplayOTA : display OTA failed ...')

        retry_count = retry_count - 1


    return

#displayProcess = Process(target=displayOTA, args=("123456",))
#displayProcess.start()
#generateAndDisplayOTA("0x80a58959e488")
