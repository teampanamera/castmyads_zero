# SHREE
from os import system, path
from sys import argv, exit
from multiprocessing import Queue, Process, Event
#from uuid import getnode
from subprocess import Popen as SPopen
from subprocess import PIPE

from IVZRELAY import ivzSocketServer
from IVZSCHEDULER import ivzScheduleAndDisplay
from IVZMGR import ivzProjectManager
from IVZGLOBALS import ivzConstants
#from IVZTHUMBNAILS import ivzThumbnails
from IVZMARQUEE import ivzMarqueeManager


#def displayFun(currentDisplayQueue, statusQueue, thumbnailRequestQueue, skipCount=0):
def displayFun(currentDisplayQueue, statusQueue, skipCount=0):

    #projectObjectsList = ivzScheduleAndDisplay.initializeProjectDataStrucures(thumbnailRequestQueue)
    projectObjectsList = ivzScheduleAndDisplay.initializeProjectDataStrucures()

    #TODO Handle this carefully. Target app will not start without this.
    if projectObjectsList == None or projectObjectsList == []:

        if ivzConstants.ivz_debug_level >= 1:
            print("displayFun : Nothing to display")

        return

    ivzScheduleAndDisplay.DisplayLoop(projectObjectsList, currentDisplayQueue, statusQueue, skipCount)


def displayMarquee():

    system("/home/pi/IVZ/IVZEXTERN/MARQUEE text")

class displayData():

    def __init__(self):

        self.ackQueue              = Queue()
        self.syncQueue             = Queue()

        #self.thumbnailRequestQueue = Queue()
        self.currentDisplayProject = Queue()
        self.statusQueue = Event()

        #self.vidHandlerProcessQueue     = Queue()

        #self.displayController   = Process(target=displayFun, args=(self.currentDisplayProject, self.statusQueue, self.thumbnailRequestQueue,))
        self.displayController   = Process(target=displayFun, args=(self.currentDisplayProject, self.statusQueue,))

        #self.reqListener         = Process(target=ivzSocketServer.launchIVZSocketServer, args=(self.ackQueue, self.syncQueue, self.thumbnailRequestQueue,))
        self.reqListener         = Process(target=ivzSocketServer.launchIVZSocketServer, args=(self.ackQueue, self.syncQueue,))
        #self.thumbnailController = Process(target=ivzThumbnails.ivzThumbnailProcessLauncher, args=(self.thumbnailRequestQueue,)) 

        #self.marqueeHandler      = Process(target=ivzMarqueeManager.launchMarqueeText)
        self.marqueeHandler      = Process(target=displayMarquee)

    def launchController(self):

        # Most likely the process is already running. Check whether that is the case.
        self.displayController.start()

        if ivzConstants.ivz_debug_level >= 1:
            print("launchController : IVZPIDOF displayloop = " + str(self.displayController.pid))

        self.launchMarquee()


    '''
    def launchThumbnailProcess(self):

        self.thumbnailController.start()

        if ivzConstants.ivz_debug_level >= 1:
            print("launchThumbnailController : IVZPIDOF thumbnailcontroller = " + str(self.thumbnailController.pid))


    def terminateThumbnailProcess(self):

        self.thumbnailController.terminate()
        self.thumbnailController = None
    '''

    def launchMarquee(self):

        self.marqueeHandler.start()

        if ivzConstants.ivz_debug_level >= 1:
            print("launchMarquee : IVZPIDOF marquee = " + str(self.marqueeHandler.pid))


    def terminateMarquee(self):

        if self.marqueeHandler != None:

            system("sudo pkill MARQUEE")

            self.marqueeHandler.terminate()
            self.marqueeHandler = None
        else:
            return

    def recreateMarqueeHandler(self):

        if self.marqueeHandler == None:
            #self.marqueeHandler = Process(target=ivzMarqueeManager.launchMarqueeText)
            self.marqueeHandler = Process(target=displayMarquee)

    def launchListener(self):

        self.reqListener.start()

        if ivzConstants.ivz_debug_level >= 1:
            print("launchListener : IVZPIDOF socketserver = " + str(self.reqListener.pid))


    def terminateSocketServer(self):

        if self.reqListener != None: 

            self.reqListener.terminate()
            self.reqListener = None
        else:
            return

    def recreateDisplayQueueResources(self):
        '''
        Purpose : Delete all the Queue and Events used by display process and re-create them
        '''

        '''
        while not self.thumbnailRequestQueue.empty():
            self.thumbnailRequestQueue.get()

        self.thumbnailRequestQueue.close()
        '''

        while not self.currentDisplayProject.empty():
            self.currentDisplayProject.get()

        self.currentDisplayProject.close()

        #while not self.vidHandlerProcessQueue.empty():
        #    self.vidHandlerProcessQueue.get()

        #self.vidHandlerProcessQueue.close()

        #self.thumbnailRequestQueue  = None
        self.currentDisplayProject  = None
        #self.vidHandlerProcessQueue = None
        self.statusQueue            = None

        #self.thumbnailRequestQueue  = Queue()
        self.currentDisplayProject  = Queue()
        #self.vidHandlerProcessQueue = Queue()
        self.statusQueue            = Event()

    def terminateController(self):

        currentProject = None

        # Update current project
        while not self.currentDisplayProject.empty():
            currentProject = self.currentDisplayProject.get()

        # Video related process teardown code - START

        # Kill the looper script
        command = "sudo ps -ax | grep \"ivzVideoLooper.sh\" | grep bash"
        p = SPopen([command], shell=True, stdout=PIPE)
        result = p.communicate()[0]

        looper_process_id = result.split(' ')[1]
        command = "sudo kill -9 " + looper_process_id
        system(command)

        # Kill vidHandler
        #process_id = self.vidHandlerProcessQueue.get()
        #print("\n****************\nterminateController : process_id = " + process_id)

        #command = "sudo kill -9 " + process_id
        #system(command)
        #system('stty sane')

        # Kill omxplayer process
        system("sudo pkill omxplayer")

        # Video related process teardown code - END

        # Kill marquee
        self.terminateMarquee()

        # Terminate display process
        if self.displayController != None:
            self.displayController.terminate()
        else:
            return currentProject

        if ivzConstants.ivz_debug_level >= 2:
            print('\nterminateController : Killing displayController')

        self.displayController = None

        return currentProject

    def terminateListener(self):

        self.reqListener.terminate()

    def reCreateController(self, skipCount=0):
        '''
        Purpose : Recreate display queue
        '''

        # Clear all the queues and re-create them
        self.recreateDisplayQueueResources()

        #self.displayController = Process(target=displayFun, args=(self.currentDisplayProject, self.statusQueue, self.thumbnailRequestQueue, skipCount,))
        self.displayController = Process(target=displayFun, args=(self.currentDisplayProject, self.statusQueue, skipCount,))

        # Create marquee again
        self.recreateMarqueeHandler()


    def controller(self):

        # Get current running project
        currentProject = None

        # Index of the project name in database
        projectIndex = 0

        if ivzConstants.ivz_debug_level >= 2:
            print('controller : Launch controller')

        while True:

            command = self.ackQueue.get()

            if command == 'UPDATEQUEUE':

                currentProject = self.terminateController()

                # index will help us skip projects in display sequence and start rendering at exact point where we left
                # this is important when we terminate and relaunch the display queue
                projectIndex = ivzProjectManager.getProjectIndexInSequence(currentProject)

                if self.displayController == None:

                    self.reCreateController(projectIndex)
                    self.launchController()

                else:

                    if ivzConstants.ivz_debug_level >= 1:
                        print('Controller : Failed to terminate display controller')

                currentProject = None
                projectIndex = 0

                self.syncQueue.put('DONE')

            elif command == 'UPDATE_SEQUENCE':

                # Update sequence results into immediate display loop termination=
                self.terminateController()

                if self.displayController == None:

                    self.reCreateController()
                    self.launchController()

                    if ivzConstants.ivz_debug_level >= 1:
                        print('Controller : Display sequence updated successfully')
                else:

                    if ivzConstants.ivz_debug_level >= 1:
                        print('Controller : Failed to terminate display controller')

                self.syncQueue.put('DONE')

            elif command == 'UPDATE_MARQUEE':

                # Update sequence results into immediate display loop termination=
                self.terminateMarquee()

                self.recreateMarqueeHandler()

                self.launchMarquee()

                if ivzConstants.ivz_debug_level >= 1:
                    print('Controller : Marquee string updated successfully')

                self.syncQueue.put('DONE')

            elif command == 'DELETE_SLEEP':
   
                if ivzConstants.ivz_debug_level >= 1:
                    print('Controller : Going to sleep now')

                currentProject = self.terminateController()
                # index will help us skip projects in display sequence and start rendering at exact point where we left
                # this is important when we terminate and relaunch the display queue
                projectIndex = ivzProjectManager.getProjectIndexInSequence(currentProject)

                self.syncQueue.put('DONE')

            elif command == 'DELETE_RESUME':

                #lastProject = None

                #if not self.currentDisplayProject.empty():
                #    currentProject = self.currentDisplayProject.get()

                if ivzConstants.ivz_debug_level >= 1:
                    print('Controller : Resuming display queue here')

                if self.displayController == None:

                    self.reCreateController(projectIndex)
                    self.launchController()

                projectIndex = 0

                self.syncQueue.put('DONE')

            elif command == 'SLEEP':
   
                if ivzConstants.ivz_debug_level >= 1:
                    print('Controller : Going to sleep now')

                currentProject = self.terminateController()

                # index will help us skip projects in display sequence and start rendering at exact point where we left
                # this is important when we terminate and relaunch the display queue
                projectIndex = ivzProjectManager.getProjectIndexInSequence(currentProject)

                self.syncQueue.put('DONE')

            elif command == 'RESUME':

                if ivzConstants.ivz_debug_level >= 1:
                    print('Controller : Resuming display queue here')

                if self.displayController == None:

                    self.reCreateController(projectIndex)
                    self.launchController()

                projectIndex = 0

                self.syncQueue.put('DONE')

            elif command == 'TERMINATE_TARGET_APP':

                if ivzConstants.ivz_debug_level >= 1:
                    print('Controller : Going to terminate app now ...')

                self.terminateController()
                self.terminateSocketServer()
                self.terminateMarquee()
                #self.terminateThumbnailProcess()

                system('stty sane')

                # Reboot the target machine for new binary to take effect
                system('sudo reboot')
                break

        return


def main():

    # Query MAC address of this machine
    #currentMac = str(getnode())
    currentMac = ivzConstants.getMacAddress()

    storedMac = None

    if not path.exists(ivzConstants.ivz_tmp_dir):
        mkd_command = 'sudo mkdir ' + ivzConstants.ivz_tmp_dir
        system(mkd_command)

        pi_chown_cmd = 'sudo chown -R pi:pi ' + ivzConstants.ivz_tmp_dir
        system(pi_chown_cmd)

    else:
        # At the time when application is launched, we do not take into account any previously uploaded updated projects
        # They may be half uploaded or incomplete
        # Simpley delete those projects
        rm_command = 'rm -rf ' + ivzConstants.ivz_tmp_dir + '/*'
        system(rm_command)

    if path.exists("/home/pi/mac.txt"):
        
        f = open("/home/pi/mac.txt", "r")
        storedMac = f.read()

    if currentMac != storedMac:

        if ivzConstants.ivz_debug_level >= 1:
            print("InfoViz authentication failed !")

        return
    else:

        if ivzConstants.ivz_debug_level >= 1:
            print("Authentication done ! Launching IVZ\n")


    if not path.exists(path.join(ivzConstants.IVZ_DATABASES, 'global_databases.db')):
        ivzProjectManager.initializeDatabases()

    # Query and store display resolution details
    ivzConstants.recordDisplayResolution()

    data = displayData()
    
    data.launchController()
    data.launchListener()
    #data.launchMarquee()
    #data.launchThumbnailProcess()

    data.controller()


if __name__ == '__main__':

    # Following args are only for authentication purpose
    args = None

    if len(argv) > 1:
        # First paramater is program name. Ignore it
        args = argv[1:]

    #print(args)

    if args != None:

        # Always work on first paramater only
        option = args[0]

        #print(option)

        if option == 'organization':
            print("panamera")
            exit(0)

        if option == 'location':
            print("sangli")
            exit(0)

    main()
