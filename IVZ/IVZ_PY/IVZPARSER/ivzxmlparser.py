#from xml.dom.minidom import getDOMImplementation
from os import path, listdir
import xml.dom.minidom as minidom
from IVZGLOBALS import ivzConstants

def parseProjectConfiguration(filepath):

    '''
    Purpose : parse project_configuration.xml file and create a dictionary of screens with widgetList as its values
    Example :  [{'screen0': [('IMAGE', 'image0')]}, {'screen1': [('VIDEO', 'video0')]}, {'screen2': [('IMAGE', 'image1'), ('VIDEO', 'video1')]}, {'screen2': [('IMAGE', 'image1'), ('VIDEO', 'video1')]}]

    params :
            filepath : path of project_configuration.xml file
    '''
   
    if not path.exists(filepath):

        if ivzConstants.ivz_debug_level >= 1:
            print('parseProjectConfiguration : File does not exists')

        return None
        
    # dictionary of screens
    result = []
    
    #doc = minidom.parse("project_configuration.xml")
    doc = minidom.parse(filepath)
   
    try:
        screens = doc.getElementsByTagName("parent0")
    except Exception as e:

        if ivzConstants.ivz_debug_level >= 1:
            print('parseProjectConfiguration : parent0 element does not exist !')

        return None
    
    for screen in screens:

        try:
            screen_name = str(screen.getElementsByTagName("name")[0].firstChild.data)
        except Exception as e:

            if ivzConstants.ivz_debug_level >= 1:
                print('parseProjectConfiguration : name element does not exist !')

            return None
 
        #print('\nparseProjectConfiguration : ' + screen_name + '\n')
        
        #minidom.Element.
        tmp_result = {screen_name:[]}
       
        try:
            widgetList = screen.getElementsByTagName("child_widget")
        except Exception as e:

            if ivzConstants.ivz_debug_level >= 1:
                print('parseProjectConfiguration : child_widget element does not exist !')
            return None

        
        for widget in widgetList:

            try:
        
                widgetType = str(widget.getElementsByTagName("type")[0].firstChild.data)
                wname = str(widget.getElementsByTagName("name")[0].firstChild.data)

            except Exception as e:

                if ivzConstants.ivz_debug_level >= 1:
                    print('parseProjectConfiguration : type/name element does not exist !')
                return None
            
            if widgetType == "image":
                
                tmp_result[screen_name].append(('IMAGE', wname))
                
            elif widgetType == "video":
            
                tmp_result[screen_name].append(('VIDEO', wname))
                
            else:
                print("parseProjectConfiguration : Incorrect type")
                continue
                
        result.append(tmp_result)

    return result

def parseImageWidget(filepath):

    '''
    Purpose : Parse image widget xml file and retrive its parameters
    Params:
            filepath : path of image widget file
    '''

    if not path.exists(filepath):

        if ivzConstants.ivz_debug_level >= 1:
            print('parseImageWidget : File does not exists')

        return None
        
    # dictionary of screens
    #result = []
    
    #doc = minidom.parse("project_configuration.xml")

    try:
        doc = minidom.parse(filepath)
    
        properties = doc.getElementsByTagName("properties")

        bgcolor = str(properties[0].getElementsByTagName("bgcolor")[0].firstChild.data)
        x = str(properties[0].getElementsByTagName("x")[0].firstChild.data)
        y = str(properties[0].getElementsByTagName("y")[0].firstChild.data)
        width = str(properties[0].getElementsByTagName("width")[0].firstChild.data)
        height = str(properties[0].getElementsByTagName("height")[0].firstChild.data)
        imageResource = str(properties[0].getElementsByTagName("image")[0].firstChild.data)

    except Exception as e:

        if ivzConstants.ivz_debug_level >= 1:
            print('parseImageWidget : bgcolor/x/y/width/height/image one of these elements is missing from xml !')

        return None

    #print(bgcolor + '\t' + x + '\t' + y + '\t' + width + '\t' + height + '\t' + imageResource)

    return (bgcolor, x, y, width, height, imageResource)
            
def parseVideoWidget(filepath):

    '''
    Purpose : Parse video widget xml file and retrive its parameters
    Params:
            filepath : path of video widget file
    '''

    if not path.exists(filepath):

        if ivzConstants.ivz_debug_level >= 1:
            print('parseVideoWidget : File does not exists')

        return None
        
    # dictionary of screens
    #result = []

    #print(filepath)
    
    #doc = minidom.parse("project_configuration.xml")

    try:
        doc = minidom.parse(filepath)
    
        properties = doc.getElementsByTagName("properties")

        bgcolor = str(properties[0].getElementsByTagName("bgcolor")[0].firstChild.data)
        x = str(properties[0].getElementsByTagName("x")[0].firstChild.data)
        y = str(properties[0].getElementsByTagName("y")[0].firstChild.data)
        width = str(properties[0].getElementsByTagName("width")[0].firstChild.data)
        height = str(properties[0].getElementsByTagName("height")[0].firstChild.data)
        videoResource = str(properties[0].getElementsByTagName("video")[0].firstChild.data)
        iterations = str(properties[0].getElementsByTagName("iterations")[0].firstChild.data)

    except Exception as e:

        if ivzConstants.ivz_debug_level >= 1:
            print('parseVideoWidget : bgcolor/x/y/width/height/image one of these elements is missing from xml !')

        return None

    #print(bgcolor + '\t' + x + '\t' + y + '\t' + width + '\t' + height + '\t' + videoResource + '\t' + iterations)

    return (bgcolor, x, y, width, height, videoResource, iterations)


def getWindowSize(xmlPath):
    '''
    Purpose : Read window size from project.xml file
    params :
            xmlPath : path to project.xml file
    Returns : width and height of project. (0,0) if something goes wrong
    '''

    if not path.exists(xmlPath):

        if ivzConstants.ivz_debug_level >= 1:
            print('getWindowSize : File does not exists \t' + xmlPath)

        return (0,0)
        
   
    #print(xmlPath)

    try:

        doc = minidom.parse(xmlPath)
    
        properties = doc.getElementsByTagName("properties")

        xsize = str(properties[0].getElementsByTagName("width")[0].firstChild.data)
        ysize = str(properties[0].getElementsByTagName("height")[0].firstChild.data)

    except Exception as e:

        if ivzConstants.ivz_debug_level >= 1:
            print('getWindowSize : width/height one of these elements is missing from xml !')

        return (0,0)

    return (xsize, ysize)

def getScreenBackgroundColor(xmlPath):

    '''
    Purpose : Read the background color field in the provided screen.xml file

    params :
            xmlPath : path to xml file from which we read the background color value

    '''

    if not path.exists(xmlPath):

        if ivzConstants.ivz_debug_level >= 1:
            print('getScreenBackgroundColor : File does not exists \t' + xmlPath)

        return 0
        
   
    #print(xmlPath)

    try:
        doc = minidom.parse(xmlPath)
    
        properties = doc.getElementsByTagName("properties")

        bgcolor = str(properties[0].getElementsByTagName("bgcolor")[0].firstChild.data)

    except Exception as e:

        if ivzConstants.ivz_debug_level >= 1:
            print('getScreenBackgroundColor : properties/bgcolor one of these elements is missing from xml !')

        return 0

    return bgcolor


def getScreenDuration(xmlPath):

    '''
    Purpose : Read the time field in the provided screen.xml file

    params :
            xmlPath : path to xml file from which we read the time attribute

    '''

    if not path.exists(xmlPath):

        if ivzConstants.ivz_debug_level >= 1:
            print('getScreenDuration : File does not exists \t' + xmlPath)

        return 0
        
   
    #print(xmlPath)

    try:
        doc = minidom.parse(xmlPath)
    
        properties = doc.getElementsByTagName("properties")

        time = str(properties[0].getElementsByTagName("time")[0].firstChild.data)

    except Exception as e:

        if ivzConstants.ivz_debug_level >= 1:
            print('getScreenDuration : properties/time one of these elements is missing from xml !')

        return 0


    return time


def getVideoDuration(xmlPath, widgetName):

    '''
    Purpose : Purpose of this function is to parse media_resources.xml file 
              and retrive the duration of video file by matching the widgetName

    Params :
            xmlPath : Path to media_resources.xml file
            widgetName : name of video widget to be searched
    '''

    if not path.exists(xmlPath):

        if ivzConstants.ivz_debug_level >= 1:
            print('getVideoDuration : File does not exists \t' + xmlPath)
        return 0
        
   
    try:

        doc = minidom.parse(xmlPath)
    
        mResources = doc.getElementsByTagName("media_resources")

        #bgcolor = str(properties[0].getElementsByTagName("bgcolor")[0].firstChild.data)

        resourceList = mResources[0].getElementsByTagName("resource")

        for resource in resourceList:

            name = str(resource.getElementsByTagName("name")[0].firstChild.data)
 
            if name == widgetName:

                #print(str(resource.getElementsByTagName("type")[0].firstChild.data))
                #print(str(resource.getElementsByTagName("name")[0].firstChild.data))

                duration = str(resource.getElementsByTagName("duration")[0].firstChild.data)

                return duration

    except Exception as e:

        if ivzConstants.ivz_debug_level >= 1:
            print('getVideoDuration : media_resources/resource/name/duration one of these elements is missing from xml !')

        return 0


    return 0


def verifyProjectConsistency(projectPath):
    """
    Purpose : Check whether project which the user is trying to upload to target is consistent.
              Consistency is defined by directory structure and xml files and tags
    :return:  True if project is consistent , False otherwise
    """

    if not path.exists(projectPath):
        return False

    if not path.isdir(projectPath):
        return False

    dirs = [name for name in listdir(projectPath) if
            path.isdir(path.join(projectPath, name))]

    if len(dirs) == 4 and 'images' in dirs and 'videos' in dirs and 'metadata' in dirs and 'thumbnail' in dirs:

        if not path.exists(path.join(projectPath, 'thumbnail', 'thumbnail.mp4')):
            if ivzConstants.ivz_debug_level >= 1:
                print("verifyProjectConsistency : Thumbnail video does not exist !")
            return False

        if not path.exists(path.join(projectPath, 'project.xml')):
            if ivzConstants.ivz_debug_level >= 1:
                print("verifyProjectConsistency : project.xml does not exist !")
            return False

        if not path.exists(path.join(projectPath, 'project_configuration.xml')):
            if ivzConstants.ivz_debug_level >= 1: 
                print("verifyProjectConsistency : project_configuration.xml does not exist !")
            return False

        result_project_configuration = parseProjectConfiguration(path.join(projectPath, 'project_configuration.xml'))

        # At least one screen is necessary in IVZ project
        if result_project_configuration is None:
            if ivzConstants.ivz_debug_level >= 1:
                print("verifyProjectConsistency : At least one screen is necessary in IVZ project !")
            return False

        result_project = getWindowSize(path.join(projectPath, 'project.xml'))

        if result_project == (0,0):
            if ivzConstants.ivz_debug_level >= 1:
                print("verifyProjectConsistency : getWindowSize failed !")
            return False

        #print(result_project_configuration)

        for screen in result_project_configuration:

           allKeys = list(screen.keys())

           screenName = allKeys[0]

           widgetList = screen[screenName]

           for w in widgetList:

               widgetName = w[1]

               # If widget exists but corresponding XML is missing, treat the project as inconsistent
               if not path.exists(path.join(projectPath, 'metadata', widgetName+'.xml')):
                   if ivzConstants.ivz_debug_level >= 1:
                       print("verifyProjectConsistency : If widget exists but corresponding XML is missing, treat the project as inconsistent !")
                   return False

               widgetXML = path.join(projectPath, 'metadata', widgetName+'.xml')

               if w[0] == 'IMAGE':

                   result_parse_image_widget = parseImageWidget(widgetXML)

                   if result_parse_image_widget is None:
                       if ivzConstants.ivz_debug_level >= 1:
                           print("verifyProjectConsistency : Error parsing image widget. 17")
                       return False


               elif w[0] == 'VIDEO':

                   result_parse_video_widget = parseVideoWidget(widgetXML)

                   if result_parse_video_widget is None:
                       if ivzConstants.ivz_debug_level >= 1:
                           print("verifyProjectConsistency : Error parsing video widget. 18")
                       return False

        return True

    else:
        if ivzConstants.ivz_debug_level >= 1:
            print("verifyProjectConsistency : Incorrect directory structure")

        return False


#result = parseProjectConfiguration('/home/pi/IVZ/projects/project1.ivzproj/project_configuration.xml')
#print('\n\n')
#print(result)

#res = parseImageWidget("/home/pi/IVZ/projects/project1.ivzproj/metadata/image0.xml")
#res = parseVideoWidget("/home/pi/IVZ/projects/project1.ivzproj/metadata/video1.xml")
#print(res)


'''
for screen in result:

    tmp = screen.keys()[0]
    print(tmp)

    widgetList = screen[tmp]

    for w in widgetList:
        print(w[1])
'''

#duration = getVideoDuration(r'/home/pi/IVZ/projects/project1.ivzproj/metadata/media_resources.xml', 'demo_video')
#getScreenDuration(r'/home/pi/IVZ/projects/project1.ivzproj/metadata/screen0.xml')

#w,h = getWindowSize(r'/home/pi/IVZ/projects/project1.ivzproj/project.xml')
#print(str(w) + '\t' + str(h))
