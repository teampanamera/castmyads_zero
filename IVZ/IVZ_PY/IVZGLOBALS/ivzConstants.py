from os import popen, system
from multiprocessing import Queue, Process, Lock, Pipe
from checksumdir import dirhash
from subprocess import Popen, PIPE

IVZ_ROOT = r'/home/pi/IVZ/'
projects_dir = r'/home/pi/IVZ/projects/'
default_projects_dir = r'/home/pi/IVZ/default_projects/'
IVZ_DATABASES = r'/home/pi/IVZ/databases/'
ivz_thumbnails_dir = r'/home/pi/IVZ/thumbnails/'
ivz_thumbnails_scratch = r'/tmp/IVZ/'
ivz_tmp_dir = r'/home/pi/ivz_tmp/'
ivz_logs_dir = r'/home/pi/IVZ/logs'
ivz_marquee_dir = r'/home/pi/IVZ/marquee'

# time in seconds after which target app sends web query to check for updates
web_query_time_slice = 15

# Key used to decrypt the uploaded project
ivz_encryption_key = b'YBd3u5SN4ETV0o1Gr_IIUeVoWy6CMTA1'

# Debug levels are used to control the logs on target
# Higher the number, higher is the verbocity of logs
# 1 : Very few logs : ivzSocketServer logs, exceptions, errors, failures
# 2 : Few logs : ivzSocketServer logs, exceptions, errors, failures, media file paths in display loop
# 3 : Every print statement in target code
ivz_debug_level = 3

# Stores the display resolution at app launch time
displayWidth = 0
displayHeight = 0

def getRotationFromConfig():
    """
    Purpose : grep in /boot/config.txt for display_rotate string and return the rotation value
    """

    rotation = 0

    p = Popen(['grep display_rotate /boot/config.txt'], shell=True, stdout=PIPE)

    x = p.communicate()[0]

    if x == '':
        if ivzConstants.ivz_debug_level >= 1:
            print("No rotation specified in /boot/config.txt")

        return 0

    rotation = x.split('=')[1].strip('\n')

    return int(rotation)


def recordDisplayResolution():
    """
        Query the resolution of currently attached display using xdpyinfo and save it in global variable
    """

    displayDim = Popen("xdpyinfo -display :0 | awk -F'[ x]+' '/dimensions:/{print $3, $4}'", stdout=PIPE, shell=True).communicate()[0]
    displayDim = displayDim.rstrip()
   
    global displayWidth
    global displayHeight

    # Following are the width and height of attached display
    displayWidth = float(displayDim.split(' ')[0])
    displayHeight = float(displayDim.split(' ')[1])

    print("\ndisplayWidth = " + str(displayWidth) + "    displayHeight = " + str(displayHeight) + "\n")

    # Kill the X server
    system("sudo pkill X")


def getMacAddress():
    '''
    Purpose : Return ethernet mac address using ifconfig
    Return  : Mac address if all goes well, None otherwise
    '''

    mac = None

    for line in popen("/sbin/ifconfig"):
        if line.find('ether') > -1:
           mac = line.split()[1]

    return mac

def _normalise(r, g, b, mode="rgb"):

    if mode not in ["rgb", "hls", "hsv"]:
        raise ValueError("Incorrect mode passed")

    if mode == "rgb":
        return r/255., g/255., b/255.
    elif mode in ["hls", "hsv"]:
        return r/360., g/100., b/100.


class HEX(object):
    """Class to check the validity of an hexadecimal string and get standard string

    By standard, we mean #FFFFFF (6 digits)

    ::

        >>> h = HEX()
        >>> h.is_valid_hex_color("#FFFF00")
        True

    """
    def __init__(self):
        pass

    def get_standard_hex_color(self, value):
        """Return standard hexadecimal color

        By standard, we mean a string that starts with # sign followed by 6
        character, e.g. #AABBFF
        """
        if isinstance(value, str)==False:
            raise TypeError("value must be a string")
        if len(value) <= 3:
            raise ValueError("input string must be of type 0xFFF, 0xFFFFFF or #FFF or #FFFFFF")

        if value.startswith("0x") or value.startswith("0X"):
            value =  value[2:]
        elif value.startswith("#"):
            value = value[1:]
        else:
            raise ValueError("hexa string must start with a '#' sign or '0x' string")
        value = value.upper()
        # Now, we have either FFFFFF or FFF
        # now check the length
        for x in value:
            if x not in "0123456789ABCDEF":
                raise ValueError("Found invalid hexa character {0}".format(x))

        if len(value) == 6 or len(value) == 8:
            value  = "#" + value[0:6]
        elif len(value) == 3:
            value = "#" + value[0]*2 + value[1]*2 + value[2]*2
        else:
            raise ValueError("hexa string should be 3, 6 or 8 digits. if 8 digits, last 2 are ignored")
        return value


def hex2rgb(hexcolor, normalise=False):

    hexcolor = HEX().get_standard_hex_color(hexcolor)[1:]
    r, g, b =  int(hexcolor[0:2], 16), int(hexcolor[2:4], 16), int(hexcolor[4:6], 16)
    if normalise:
        r, g, b = _normalise(r, g, b)
    return r, g, b


