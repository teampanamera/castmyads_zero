from cryptography.fernet import Fernet

class ivzencryption:

    def __init__(self):

        # Following key is generated using : Fernet.generate_key()
        self.key = b'YBd3u5SN4ETV0o1Gr_IIUeVoWy6CMTA1QQwigfPJuSE='
        self.fernet = Fernet(self.key)

    def encryptmessage(self, msg):
        """
        Purpose : Encrypt a message using a hard coded key
        :param msg: message to be encoded. This is in string format and not byte array.
        :return: encrypted message in string format
        """

        token = self.fernet.encrypt(msg.encode('utf-8'))
        return token.decode('utf-8')

    def decryptmessage(self, encryptedmessage):
        """
        Purpose : decipher the encryptedmessage
        :param encryptedmessage: encrypted string
        :return: decrypted string
        """

        val = self.fernet.decrypt(encryptedmessage)
        return val.decode('utf-8')

def ivzencryptmessage(message):

    """
    Purpose : Helper function for encrypting incoming message
    :param message: Message in string format to be encrypted
    :return: encrypted message in string format
    """

    obj = ivzencryption()
    e = obj.encryptmessage(message)
    return e

def ivzdecryptmessage(message):

    """
    Purpose : Helper function for decrypting incoming message
    :param message: Message in string format to be decrypted
    :return: decrypted message in string format
    """

    obj = ivzencryption()
    msg = obj.decryptmessage(message.encode('utf-8'))
    return msg

#a = ivzencryptmessage("Hello")
#print(a)
#print(ivzdecryptmessage(a))

