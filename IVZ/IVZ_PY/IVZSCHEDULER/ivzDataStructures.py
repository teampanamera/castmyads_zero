
class ivzWidget():

    def __init__(self, name):

        self.name = name

    def setWidgetType(self, widgetType):

        '''
            Purpose : Set the widget type
            widgetType : 1 for Image, 2 for Video
        '''

        self.widgetType = widgetType

    def setDimensions(self, width, height):

        self.width = float(width)
        self.height = float(height)

    def setPosition(self, X, Y):

        self.X = float(X)
        self.Y = float(Y)

    def setbgColor(self, color):

        self.bgColor = color

    def setMediaResourcePath(self, resource):

        self.mediaPath = resource


class ivzImageWidget(ivzWidget):

    def __init__(self, name, width, height, X, Y, color, mediaResource):

        ivzWidget.__init__(self, name)

        ivzWidget.setDimensions(self, width, height)

        ivzWidget.setPosition(self, X, Y)

        ivzWidget.setbgColor(self, color)

        # 1 for image widget
        ivzWidget.setWidgetType(self, 1)

        ivzWidget.setMediaResourcePath(self, mediaResource)

        #print('Inside init ivzImageWidget')


class ivzVideoWidget(ivzWidget):

    def __init__(self, name, width, height, X, Y, color, mediaResource):

        ivzWidget.__init__(self, name)

        ivzWidget.setDimensions(self, width, height)

        ivzWidget.setPosition(self, X, Y)

        ivzWidget.setbgColor(self, color)

        # 2 for video vidget
        ivzWidget.setWidgetType(self, 2)

        ivzWidget.setMediaResourcePath(self, mediaResource)

        #print('Inside init ivzVideoWidget')

    def setIterations(self, n):

        self.iterations = n


class ivzProjectData():

    def __init__(self, name):

        self.name = name

        self.screens = {}

        # This list helps maintaining the order of screens as per configuration
        self.screensOrderedList = []

        # dictionary containing screen name and corresponding display duration
        self.screenDurations = {}

        # dictionary containing screen name and corresponding background color value
        self.screenBackgroundColors = {}

        self.xSize = 1280
        self.ySize = 720

    def addScreen(self, screenName):

        self.screens[screenName] = []

    def addWidgetToScreen(self, screenName, widgetObject):

        self.screens[screenName].append(widgetObject)

    def setScreenDuration(self, screenName, duration):

        self.screenDurations[screenName] = duration

    def setScreenBackgroundColor(self, screenName, color):

        self.screenBackgroundColors[screenName] = color

    def setDimensions(self, xsize, ysize):

        self.xSize = xsize
        self.ySize = ysize



