from os import listdir, path, system, remove, mkdir
from multiprocessing import Queue, Process, Lock, Pipe
from checksumdir import dirhash
from subprocess import Popen, PIPE
from zipfile import ZipFile
#from colormap import hex2rgb
from time import sleep

from IVZSCHEDULER import ivzDataStructures
from IVZPARSER import ivzxmlparser
from IVZGLOBALS import ivzConstants
from IVZMGR import ivzProjectManager
from IVZRELAY import ivzGetDirtyList

#def initializeProjectDataStrucures(thumbnailRequestQueue):
def initializeProjectDataStrucures():

    # get list of available projects
    #projectList = listdir(ivzConstants.projects_dir)

    projectList = []

    project_lookup_dir = ivzConstants.projects_dir

    #result = ivzProjectManager.getProjectList()
    result = ivzProjectManager.getDisplaySequence()

    # This is very important
    # Initially when no projects are available, target app will launch default projects
    if result == None:

        if ivzConstants.ivz_debug_level >= 1:
            print("\ninitializeProjectDataStrucures : No uploaded projects found !!!  Launching default projects ...\n")

        try:
            # get list of available default projects
            projectList = listdir(ivzConstants.default_projects_dir)
        except Exception as e:
            if ivzConstants.ivz_debug_level >= 1 :
                print("initializeProjectDataStrucures : ERROR fetching default projects. Error = "+ str(e))

        #print(ivzConstants.default_projects_dir)
        #print(projectList)

        project_lookup_dir = ivzConstants.default_projects_dir

        #for i in result:
        #    projectList.append(i)
    else:

        # Check if there are any partial uploads done
        # Delete what is not required and copy media files, xml files

        updated_projects = listdir(ivzConstants.ivz_tmp_dir)

        if updated_projects != []:

            for project in updated_projects:

                # Delete existing copy of the project
                clean_command = 'sudo rm -rf ' + path.join(ivzConstants.projects_dir, str(project))
                system(clean_command)

                # Project is already deflated in ivz_tmp_dir
                move_command = 'sudo mv ' + str(path.join(ivzConstants.ivz_tmp_dir, project)) + ' ' + str(path.join(ivzConstants.projects_dir, str(project)))
                system(move_command)

                # Give enought permissions to project directory,
                # Otherwise operations like thumbnail download will fail
                pi_chown_cmd = 'sudo chown -R pi:pi ' + str(path.join(ivzConstants.projects_dir, project))
                system(pi_chown_cmd)


                '''
                # Path to the project in ivz tmp directory
                projectPath = path.join(ivzConstants.ivz_tmp_dir, project)

                # Final path of the project
                finalPath = path.join(ivzConstants.projects_dir, project)

                # If the enc file does not exists, continue
                if not path.exists(projectPath):
                    print("initializeProjectDataStructure : Path does not exists : " + str(projectPath))
                    continue

                tmp_zip = path.join('/tmp', project + '.zip')

                try:

                    # Deflate .zip file from enc file
                    ivzProjectManager.decrypt_file(projectPath, tmp_zip)

	            # Remove enc file
                    #remove(projectPath)

                    clean_command = 'sudo rm -rf ' + str(projectPath)
                    system(clean_command)

                except Exception as e:

                    if ivzConstants.ivz_debug_level >= 1:
                        print('initalizeProjectDatastructures : deflation failed with exception : ' + str(e))

                    continue

   	        # Unzip file
                try:

                    unzip_data = ZipFile(tmp_zip, 'r')
                    unzip_data.extractall(finalPath)
                    unzip_data.close()

                except Exception as e:

                    if ivzConstants.ivz_debug_level >= 1:
                        print('initializeProjectDataStructures : extractall failed with exception : ' + str(e))

                    continue

                # Delete the zip file
                remove(tmp_zip)

                # Cleanup
                #clean_command = 'sudo rm -rf ' + path.join(ivzConstants.ivz_tmp_dir, str(project))
                #system(clean_command)
                '''

        for i in result:
            projectList.append(i[0])

    if projectList == []:
        return None

    #print("initializeProjectDataStrucures : projectList = " + str(projectList))

    projectObjectsList = []

    #print(projectList)

    for project in projectList:

        #print(project)

        # parse project configuration file
        #projectResources = ivzxmlparser.parseProjectConfiguration(path.join(ivzConstants.projects_dir, project, 'project_configuration.xml'))
        projectResources = ivzxmlparser.parseProjectConfiguration(path.join(project_lookup_dir, project, 'project_configuration.xml'))

        if projectResources == None:
            if ivzConstants.ivz_debug_level >= 1:
                print("\ninitializeProjectDataStrucures : Error parsing project directory - " + str(project))

            continue

        # result of parsing configuration xml looks like following :
        # [{'screen0': [('IMAGE', 'image0')]}, {'screen1': [('VIDEO', 'video0')]}, {'screen2': [('IMAGE', 'image1'), ('VIDEO', 'video1')]}, {'screen2': [('IMAGE', 'image1'), ('VIDEO', 'video1')]}]

        projectObject = ivzDataStructures.ivzProjectData(project)

        #xsize, ysize = ivzxmlparser.getWindowSize(path.join(ivzConstants.projects_dir, project, 'project.xml'))
        xsize, ysize = ivzxmlparser.getWindowSize(path.join(project_lookup_dir, project, 'project.xml'))

        projectObject.setDimensions(xsize, ysize)

        for screen in projectResources:
           #print(screen)

           screenName = screen.keys()[0]

           #print(screenName)

           projectObject.screensOrderedList.append(screenName)

           projectObject.addScreen(screenName)

           widgetList = screen[screenName]

           for w in widgetList:

               widgetName = w[1]

               #widgetXML = path.join(ivzConstants.projects_dir, project, 'metadata', widgetName+'.xml')
               widgetXML = path.join(project_lookup_dir, project, 'metadata', widgetName+'.xml')

               if w[0] == 'IMAGE':

                   #print('Image widget : ' + str(widgetName))

                   bgcolor, x, y, width, height, imageResource = ivzxmlparser.parseImageWidget(widgetXML)

                   #imageResource = path.join(ivzConstants.projects_dir, project, 'images', imageResource)
                   imageResource = path.join(project_lookup_dir, project, 'images', imageResource)

                   imageWidgetObject = ivzDataStructures.ivzImageWidget(widgetName, width, height, x, y, bgcolor, imageResource)

                   projectObject.addWidgetToScreen(screenName, imageWidgetObject)

               elif w[0] == 'VIDEO':

                   #print('Video widget : ' + str(widgetName))

                   bgcolor, x, y, width, height, videoResource, iterations = ivzxmlparser.parseVideoWidget(widgetXML)

                   #videoResource = path.join(ivzConstants.projects_dir, project, 'videos', videoResource)
                   videoResource = path.join(project_lookup_dir, project, 'videos', videoResource)

                   videoWidgetObject = ivzDataStructures.ivzVideoWidget(widgetName, width, height, x, y, bgcolor, videoResource)

                   videoWidgetObject.setIterations(iterations)

                   projectObject.addWidgetToScreen(screenName, videoWidgetObject)


        projectObjectsList.append(projectObject)



    # Calculate screen duration for each screeen in each project
    for project in projectObjectsList:

        # To store the total display duration of a project
        totalDuration = 0

        if ivzConstants.ivz_debug_level >= 3:
            print('\ninitializeProjectDataStrucures : Project = ' + project.name  + ' '  +  str(project.screensOrderedList))

        #print(project.name)

        #for screen in project.screens.keys():
        for screen in project.screensOrderedList:

            # Set the background color for this creen
            #color = ivzxmlparser.getScreenBackgroundColor(path.join(ivzConstants.projects_dir, project.name, 'metadata', screen+'.xml'))
            color = ivzxmlparser.getScreenBackgroundColor(path.join(project_lookup_dir, project.name, 'metadata', screen+'.xml'))
            project.setScreenBackgroundColor(screen, str(color))

            widgetList = project.screens[screen]

            # initialize the screen duration to 0
            project.setScreenDuration(screen, 0)

            #print(screen)

            hasVideo = False

            dur = 0

            # TODO : At this moment we only support one video per screen
            # In future if we support multiple videos in a screen, this loop will need to find video with maximum length
            for widget in widgetList:

                if not path.exists(widget.mediaPath):
                    continue

                if isinstance(widget, ivzDataStructures.ivzVideoWidget):

                    #print('Found Video \t' + widget.mediaPath)

                    #mediaName = widget.mediaPath.split('/')[len(widget.mediaPath.split('/')) - 1]
                    #mediaName = mediaName.split('.')[0]

                    mediaName = path.basename(widget.mediaPath)
                    mediaName = path.splitext(mediaName)[0]

                    #print(mediaName)

                    #vidDuration = ivzxmlparser.getVideoDuration(path.join(ivzConstants.projects_dir, project.name, 'metadata', 'media_resources.xml'), mediaName)
                    vidDuration = ivzxmlparser.getVideoDuration(path.join(project_lookup_dir, project.name, 'metadata', 'media_resources.xml'), mediaName)

                    #print(widget.iterations)

                    if widget.iterations == 0:

                        dur = float(vidDuration)

                    else:

                        dur = float(widget.iterations) * float(vidDuration)

                    # Screen duration could be more than video time
                    # Handle this scenario
                    xml_dur = ivzxmlparser.getScreenDuration(path.join(project_lookup_dir, project.name, 'metadata', screen+'.xml'))

                    if float(xml_dur) > dur:
                        dur = float(xml_dur)

                    project.setScreenDuration(screen, dur)

                    #print(widget.name)

                    hasVideo = True

                    break

            if hasVideo is False:

                #dur = ivzxmlparser.getScreenDuration(path.join(ivzConstants.projects_dir, project.name, 'metadata', screen+'.xml'))
                dur = ivzxmlparser.getScreenDuration(path.join(project_lookup_dir, project.name, 'metadata', screen+'.xml'))
                project.setScreenDuration(screen, float(dur))

                #print('No video in this screen')

            totalDuration = totalDuration + float(dur)

            #print(project.screenDurations[screen])

        if ivzConstants.ivz_debug_level >= 3:
            print("initializeProjectDataStrucures : Setting Project : " + project.name + "    DURATION = " + str(totalDuration))

        ivzProjectManager.insertOrUpdateProjectDuration(project.name, totalDuration)

    #DisplayLoop(projectObjectsList)

    return projectObjectsList


def queryDisplayResolution():
    """
        Query the resolution of currently attached display using xdpyinfo
    """

    res = Popen("xdpyinfo -display :0 | awk -F'[ x]+' '/dimensions:/{print $3, $4}'", stdout=PIPE, shell=True).communicate()[0]
    res = res.rstrip()
    return res


def videoHandler(vidQueue, vidPlaybackStatus):
    '''
        Purpose : Receive video launch parameters in queue and launch the corresponding video using videoLooper script
        Parameters :
             vidQueue : queue which is pollulated by displayLoop for video playback
    '''

    while True:
        
        command = vidQueue.get()

        if ivzConstants.ivz_debug_level >= 3:
            print("videoHandler : Received command : " + str(command))

        #vidPlaybackStatus.acquire()

        try:
            #system(command)
            res = Popen(command, stdout=PIPE, shell=True).communicate()

            print("videoHandler : Result of video : " + str(res))

        except Exception as e:
            if ivzConstants.ivz_debug_level >= 1:
                print("videoHandler : something went wrong while launching video ... : " + str(e))

        if ivzConstants.ivz_debug_level >= 3:
            print("videoHandler : Putting DONE in vidPlaybackStatus")

        # Put DONE message after video finishes rendering
        #vidPlaybackStatus.put("DONE")
        #vidPlaybackStatus.release()

        vidPlaybackStatus.send("DONE")


def imageRunner(command):
    """
    Purpose : Simply receive the command and run it using system call
              This method is used for images playback
    """

    system(command)

def videoRunner(command):
    """
    Purpose : Simply receive the command and run it using system call
              This method is used for video playback
    """

    system(command)

'''
def getRotationFromConfig():
    """
    Purpose : grep in /boot/config.txt for display_rotate string and return the rotation value
    """

    rotation = 0

    p = Popen(['grep display_rotate /boot/config.txt'], shell=True, stdout=PIPE)

    x = p.communicate()[0]

    if x == '':
        if ivzConstants.ivz_debug_level >= 1:
            print("No rotation specified in /boot/config.txt")

        return 0

    rotation = x.split('=')[1].strip('\n')

    return int(rotation)

'''

#def DisplayLoop(projectObjectsList, currentDisplayQueue, statusQueue, vidHandlerProcessQueue, skipCount):
def DisplayLoop(projectObjectsList, currentDisplayQueue, statusQueue, skipCount):

    if ivzConstants.ivz_debug_level >= 1:
        print('\nDisplayLoop :  Launching Display loop \n')

    #win = ivzgui.ivzWindow()
    #time.sleep(6)

    # TODO : Uncomment this
    #displayDim = queryDisplayResolution()

    # Following are the width and height of attached display
    #displayWidth = float(displayDim.split(' ')[0])
    #displayHeight = float(displayDim.split(' ')[1])

    rotation = ivzConstants.getRotationFromConfig()

    # If display rotation is 90 or 270 , swap the width and height
    # This is because we are not using display rotation from OS anymore, we are simulating this instead
    # This is important to make video decoding work on Zero which is apparently not good at rotating videos at OS level
    if rotation == 1 or rotation == 3:

        displayWidth = ivzConstants.displayHeight
        displayHeight = ivzConstants.displayWidth

    else:

        displayWidth = ivzConstants.displayWidth
        displayHeight = ivzConstants.displayHeight

    #print(" ** " + str(ivzConstants.displayWidth) + " " + str(ivzConstants.displayHeight))

    #print("\ndisplayWidth = " + str(displayWidth) + "    displayHeight = " + str(displayHeight) + "\n")

    # Make space for marquee text
    displayHeight = displayHeight - ivzConstants.displayHeight/18;

    localSkipCount = skipCount

    if ivzConstants.ivz_debug_level >= 1:
        print('\nDisplayLoop : Skip count = ' + str(localSkipCount))

    vidQueue            = Queue()
    #vidPlaybackStatus   = Queue()
    #parent_vidPlaybackStatus, child_vidPlaybackStatus   = Pipe()

    vidProcess = None
    imageProcess = None

    #videoHandlerProcess = Process(target=videoHandler, args=(vidQueue, child_vidPlaybackStatus,))
    #videoHandlerProcess.start()
    #print("\nDispayLoop : vidProcess ID = " + str(videoHandlerProcess.pid))

    #vidHandlerProcessQueue.put(str(videoHandlerProcess.pid))

    while True:

        for project in projectObjectsList:

            if localSkipCount > 0:
                localSkipCount = localSkipCount - 1
                # Skip count helps us jump in display sequence queue and start rendering from where we left before
                # This is important for scenarios where we terminate and relaunch the display queue
                continue

            if ivzConstants.ivz_debug_level >= 3:
                print('\nDisplayLoop : Project name : ' + str(project.name)  + ' Dimensions are : ' + str(project.xSize) + '\t' + str(project.ySize))

            # Empty the current display queue first
            while not currentDisplayQueue.empty():
                currentDisplayQueue.get()

            currentDisplayQueue.put(project.name)

            statusQueue.clear()

            #print(str(displayWidth) + " " + str(displayHeight))
            #print(str(project.xSize) + " " + str(project.ySize))

            # Calculate the scaling needed to be applied, in case project dimensions does not match screen resolution
            # Note that, scaling factors are specific to each project and not global
            #xScale = displayWidth / float(project.xSize)
            #yScale = displayHeight / float(project.ySize)
            xScale = displayWidth / float(project.xSize)
            yScale = displayHeight / float(project.ySize)

            if ivzConstants.ivz_debug_level >= 3:
                print('\nDisplayLoop : project name = ' + str(project.name) + ' xScale = ' + str(xScale) + ' yScale = ' + str(yScale))

            widgetList = []

            #for screen in project.screens.keys():
            for screen in project.screensOrderedList:

                if ivzConstants.ivz_debug_level >= 3:
                    print('\nDisplayLoop : Processing screen : ' + screen )

                if project.screenDurations[screen] == 0:
                    if ivzConstants.ivz_debug_level >= 3:
                        print("\nDisplayLoop : Screen duration is 0 skipping")

                    # Dont process the screen if duration is 0
                    # Simply skip such screen
                    continue

                scaledDimX = int(float(project.xSize) * xScale)
                scaledDimY = int(float(project.ySize) * yScale)

                #dim = str(scaledDimX) + 'x' + str(scaledDimY)

                #win.setwindowdimensions(dim)

                #win.setbackgroundcolor(project.screenBackgroundColors[screen])

                widgetList = project.screens[screen]

                videoProcessStatus = None

                hasVideo = False

                for widget in widgetList:

                    # Handle the case where medi does not exists of path is incorrect
                    # simply dont process the widget in that case
                    if not path.exists(widget.mediaPath):
                        continue

                    # There are strange cases where no image or video file is provided in IVZ project
                    # Simply don't treat such widget
                    if path.basename(widget.mediaPath) == 'None':
                        continue

                    if isinstance(widget, ivzDataStructures.ivzVideoWidget):

                        x2 = float(widget.X) + float(widget.width)
                        y2 = float(widget.Y) + float(widget.height)

			# Apply scaling factors
                        scaledX1 = widget.X * xScale
                        scaledY1 = widget.Y * yScale

                        scaledX2 = x2 * xScale
                        scaledY2 = y2 * yScale

                        #print("\n" + str(widget.X) + " " + str(widget.Y) + " " + str(widget.width) + " " + str(widget.height) + "\n")
                        #print("\n" + str(scaledX1) + " " + str(scaledY1) + " " + str(scaledX2) + " " + str(scaledY2) + "\n")
                        #print("\n" + str(xScale) + " " + str(yScale))

                        orientation = 0

                        # Remember that rotation 1 means counter clockwise 90 degree rotation for InfoWiz

                        if rotation == 0:

                            orientation = 0

                            vidX1 = scaledX1
 
                            vidY1 = scaledY1 

                            vidX2 = scaledX2

                            vidY2 = scaledY2

                        elif rotation == 1:

                            orientation = 90

                            vidX1 = ivzConstants.displayWidth - scaledY2 
 
                            vidY1 = scaledX1 

                            #vidX2 = vidX1 + float(widget.width) * xScale
                            #vidY2 = vidY1 + float(widget.height) * yScale

                            vidX2 = ivzConstants.displayWidth - scaledY1
                            vidY2 = scaledX2


                        elif rotation == 2:

                            orientation = 180

                            vidX1 = ivzConstants.displayWidth - scaledX2
 
                            vidY1 = ivzConstants.displayHeight - scaledY2

                            vidX2 = ivzConstants.displayWidth - scaledX1

                            vidY2 = ivzConstants.displayHeight - scaledY1


                        elif rotation == 3:

                            orientation = 270

                            vidX1 = scaledY1
 
                            vidY1 = ivzConstants.displayHeight - scaledX2

                            vidX2 = scaledY2

                            vidY2 = ivzConstants.displayHeight - scaledX1


                        #print("\n" + str(scaledX1) + " " + str(scaledY1) + " " + str(scaledX2) + " " + str(scaledY2) + "\n")

                        #vidCommand = 'sudo /home/pi/IVZ/scripts/ivzVideoLooper.sh ' + '-a '  + str(scaledX1) + ' -b ' + str(scaledY1) + ' -c ' + str(scaledX2) + ' -d ' + str(scaledY2) + ' -p ' + "\'"  + str(widget.mediaPath) + "\'"  + ' -i ' + str(widget.iterations) + ' -o ' + str(orientation)

                        vidCommand = 'sudo /home/pi/IVZ/scripts/ivzVideoLooper.sh ' + '-a '  + str(vidX1) + ' -b ' + str(vidY1) + ' -c ' + str(vidX2) + ' -d ' + str(vidY2) + ' -p ' + "\'"  + str(widget.mediaPath) + "\'"  + ' -i ' + str(widget.iterations) + ' -o ' + str(orientation)


                        #print(vidCommand)

                        vidProcess = Process(target=videoRunner, args=(vidCommand,))
                        vidProcess.start()

                        videoProcessStatus = "RUNNING"

                        if videoProcessStatus == "RUNNING":

                            if ivzConstants.ivz_debug_level >= 2:
                                print("DisplayLoop : Video launched successfully. Mediapath =  " + str(widget.mediaPath))

                            hasVideo = True

                    widget = None

                # convert hex color value to RGB
                screenbgcolor = ivzConstants.hex2rgb('#' + str(project.screenBackgroundColors[screen]), True)
                imageLaunchCommand = "/home/pi/IVZ/IVZEXTERN/IMAGE_RENDERER " + str(project.screenDurations[screen]) + ' ' + str(screenbgcolor[0]) + ' ' + str(screenbgcolor[1]) + ' ' + str(screenbgcolor[2])

                # Maintain count of image widgets. If there are no image widgets, we can use triangle2 to simply clear screen color
                imagecount = 0

                # subCommand accumulcates the params for individual image launches
                subCommand = ''

                for widget in widgetList:

                    # Handle the case where medi does not exists of path is incorrect
                    # simply dont process the widget in that case
                    if not path.exists(widget.mediaPath):
                        continue

                    # There are strange cases where no image or video file is provided in IVZ project
                    # Simply don't treat such widget
                    if path.basename(widget.mediaPath) == 'None':
                        continue

                    if isinstance(widget, ivzDataStructures.ivzImageWidget):
    
                        if ivzConstants.ivz_debug_level >= 2:
                            print('DisplayLoop : Image MediaPath : ' + widget.mediaPath)

                        imagecount = imagecount + 1

                        try:

			    # Apply scaling factors
                            #scaledWidth = widget.width * xScale
                            #scaledHeight = widget.height * yScale

                            if rotation == 1 or rotation == 3:

                                scaledHeight = widget.width * xScale
                                scaledWidth = widget.height * yScale
                                #scaledWidth = widget.height * ivzConstants.displayWidth / float(project.ySize)

                            if rotation == 0 or rotation == 2:

                                scaledWidth = widget.width * xScale
                                scaledHeight = widget.height * yScale


                            # *******************************************

                            x2 = float(widget.X) + float(widget.width)
                            y2 = float(widget.Y) + float(widget.height)

                            scaledX1 = widget.X * xScale
                            scaledY1 = widget.Y * yScale

                            scaledX2 = x2 * xScale
                            scaledY2 = y2 * yScale

                            #print("\n" + str(widget.X) + " " + str(widget.Y) + " " + str(widget.width) + " " + str(widget.height) + "\n")
                            #print("\n" + str(scaledX1) + " " + str(scaledY1) + " " + str(scaledX2) + " " + str(scaledY2) + "\n")
                            #print("\n" + str(xScale) + " " + str(yScale))

                            if rotation == 0:

                                scaledX = widget.X * xScale
                                scaledY = widget.Y * yScale

                            elif rotation == 1:

                                #scaledX = widget.X * xScale
                                #scaledY = widget.Y * yScale

                                #scaledX = ivzConstants.displayWidth - scaledY2 
                                #print("rotation = 1 : " + str(displayHeight) + " " + str(scaledY2))
                                scaledX = displayHeight - scaledY2 
                                scaledY = scaledX1 

                            elif rotation == 2:

                                scaledX = ivzConstants.displayWidth - scaledX2
                                scaledY = ivzConstants.displayHeight - scaledY2

                            elif rotation == 3:

                                scaledX = scaledY1
                                scaledY = ivzConstants.displayHeight - scaledX2

                            # ******************************************


                            subCommand = subCommand + ' ' + str(widget.mediaPath) + ' ' + str(scaledX) + ' ' + str(scaledY) + ' ' + str(scaledWidth) + ' ' + str(scaledHeight)

                        except Exception as e:
                            if ivzConstants.ivz_debug_level >= 1:
                                print("DisplayLoop : Cought error while adding image frame : "+str(e))

                    widget = None

                # If there are no image widgets, use triangle2 to simply clear bgcolor
                if imagecount == 0:
                    imageLaunchCommand = imageLaunchCommand + ' ' + '1 0 NONE 0 0 0 0'
                else:
                    imageLaunchCommand = imageLaunchCommand + ' ' + str(imagecount)
                    # Add fullScreenFlag as 0
                    imageLaunchCommand = imageLaunchCommand + ' ' + '0'
                    imageLaunchCommand = imageLaunchCommand + subCommand


                #print(imageLaunchCommand)
                # Launch image rendering code. This will be run for each screen irrespective of whether it contains image widgets
                imageProcess = Process(target=imageRunner, args=(imageLaunchCommand,))
                imageProcess.start()

                if ivzConstants.ivz_debug_level >= 3:
                    print('DisplayLoop : Screen duration : ' + str(project.screenDurations[screen]))

                # Put this loop to sleep
                sleep(project.screenDurations[screen])

                if videoProcessStatus == "RUNNING":

                    if ivzConstants.ivz_debug_level >= 3:
                        print("DisplayLoop : Going to wait for video to finish")

                    # Busy waiting
                    while vidProcess.is_alive():
                        continue

                    if ivzConstants.ivz_debug_level >= 3:
                        print("DisplayLoop : Done waiting for video to finish")


                if ivzConstants.ivz_debug_level >= 3:
                    print("DisplayLoop : Going to wait for image to finish")

                # Busy waiting
                while imageProcess.is_alive():
                    continue

                if ivzConstants.ivz_debug_level >= 3:
                    print("DisplayLoop : Done waiting for image to finish")


                #win.getwindowhandle().mainloop()

                if ivzConstants.ivz_debug_level >= 3:
                    print('DisplayLoop : About to break.')

                if vidProcess != None:
                    vidProcess.terminate()
                    vidProcess = None

                if imageProcess != None:
                    imageProcess.terminate()
                    imageProcess = None


                '''
                for f in win.frameList:
                    f.clearCanvas()
                    f.destroy()
                    f = None

                #win.frameList.clear()
                del win.frameList[:]
                win.frameList = []
                '''

            #del widgetList[:]
            widgetList = []

                #break

            if ivzConstants.ivz_debug_level >= 3:
                print('DisplayLoop : project display Done')
            #statusQueue.put('DONE')
            statusQueue.set()

            project = None

#initializeProjectDataStrucures()
