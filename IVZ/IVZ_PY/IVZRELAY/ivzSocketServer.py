#!/usr/bin/python           

'''
    This file is responsible for establishing communication between host and target
    There are several Operational Codes which can be passed to this server

    PUT : Insert projectname into database
    GET : Get ID of projectname
    SET : Set download status of projectname to COMPLETE or IN_PROGRESS
    SETPROGRESS : Set download status of projectname to IN_PROGRESS
    REMOVE : Remove projectname from database


'''

from socket import socket, AF_INET, SOCK_STREAM, SOL_SOCKET, SO_REUSEADDR, error
from sys import getsizeof
from time import sleep
from subprocess import Popen, PIPE
from os import path, listdir, system
from checksumdir import dirhash
from multiprocessing import Process
from hashlib import sha1
from ast import literal_eval
from os import remove, mkdir
#from zipfile import ZipFile, ZIP_DEFLATED

#from project_manager import *
#from getDirtyList import *
#from ivzOTA import *

from requests import post

from IVZMGR import ivzProjectManager
from IVZRELAY import ivzGetDirtyList
from IVZAUTH import ivzOTA
from IVZGLOBALS import ivzConstants
from IVZCRYPTO import ivzencryption
#from IVZTHUMBNAILS import ivzThumbnails
from IVZLOCK import ivzlockmanager
from IVZPARSER import ivzxmlparser
from IVZCLOUD import ivzWebInterface

from random import randint

#projects_dir = r'/home/pi/IVZ/projects'

def hash_file(filename):
    """
    Purpose : This function returns the SHA-1 hash of the file passed into it
    :param filename: full path of file
    :return: hash value of the provided file,
            None if file does not exist
    """

    if not path.exists(filename):
        return None

    # make a hash object
    h = sha1()

    # open file for reading in binary mode
    with open(filename,'rb') as file:

       chunk = 0
       while chunk != b'':
           # read only 1024 bytes at a time
           chunk = file.read(1024)
           h.update(chunk)

    # return the hex representation of digest
    return h.hexdigest()


def prepareEncProject(projectPath, projectName):
    """
    Purpose : Handle the process of creating .iwzproj from existing project
    """

    if path.exists(projectPath):

        dest = str(path.join(ivzConstants.ivz_tmp_dir, projectName)) + '.7z'

        command = "7z a " + str(dest) + " "  + str(path.join(ivzConstants.projects_dir, projectName, '*'))
        system(command)

        if not path.exists(dest):

            if ivzConstants.ivz_debug_level >= 1:
                print('prepareEncProject : 7z compression failed !')
                return


        result = ivzProjectManager.encrypt_file(ivzConstants.ivz_encryption_key, dest, path.join(ivzConstants.ivz_tmp_dir, projectName))

        if result == False:

            if ivzConstants.ivz_debug_level >= 1:
                print('prepareEncProject : encrypt_file failed !')


        # Remove .zip file
        remove(dest)

        # Give enought permissions to project directory,
        # Otherwise operations like thumbnail download will fail
        pi_chown_cmd = 'sudo chown -R pi:pi ' + str(ivzConstants.ivz_tmp_dir)
        system(pi_chown_cmd)


'''
def deflate_zip_project(projectPath, projectname):
    """
    Purpose : Helper function to handle project zip file deflation process
    """

    #tmp_zip = path.join('/tmp', projectname + '.zip')
    tmp_zip = path.join('/tmp', projectname + '.7z')

    try:

        # Deflate .zip file from enc file
        ivzProjectManager.decrypt_file(projectPath, tmp_zip)

	# Remove enc file
        remove(projectPath)

	# Create the project directory
        mkdir(projectPath)

    except Exception as e:

        if ivzConstants.ivz_debug_level >= 1:
            print('deflate_zip_project : deflate_zip_project : deflation failed with exception : ' + str(e))

        return

    # Unzip file
    try:

        #unzip_data = ZipFile(tmp_zip, 'r')
        #unzip_data.extractall(projectPath)
        #unzip_data.close()

        command = "7z x " + str(tmp_zip) + " -o" + str(projectPath)
        system(command)

    except Exception as e:
        if ivzConstants.ivz_debug_level >= 1:
            print('deflate_zip_project : deflate_zip_project : extractall failed with exception : ' + str(e))

        return

    # Delete the zip file
    remove(tmp_zip)
'''

def imageRunner(command):
    """
    Purpose : Simply receive the command and run it using system call
              This method is used for images playback
    """

    system(command)


def launchIVZSocketServer(ackQueue, syncQueue):

    """
    Purpose : Launcher function for socket server. The success/failure of Target app lies in the stability of this socket server
    Parameters : ackQueue  - Establishes a communication channel between socket server and display controller
                 syncQueue - Helps to acheive sync between socket server and display controller. This is mostly applicable for DELETE_SLEEP
    """

    if ivzConstants.ivz_debug_level >= 1:
        print('launchIVZSocketServer : **** Launching server ****')

    s = socket(AF_INET, SOCK_STREAM)

    s.settimeout(ivzConstants.web_query_time_slice)

    # This call is to overcome "Address already in use error"
    # It tell kernel to reuse a local socket in TIME_WAIT state,
    # without waiting for its natural lock to expire
    s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)

    # gethostname returns panamera_pi but we cannot connect to this socket using hostname. 
    # We need IP address instead
    #host = socket.gethostname()

    host = '172.24.1.1'
    port = 80

    # This is dictionary to store the result of QUERY_RESPONSE_SIZE_GETDIRTYLIST API
    # GETDIRTYLIST API called after QUERY_RESPONSE_SIZE_GETDIRTYLIST will return the actual result
    dirtyListBuffer = {}

    # This is a string to store the result of QUERY_RESPONSE_SIZE_PROJECTLIST API
    # PROJECTLIST API called after QUERY_RESPONSE_SIZE_PROJECTLIST will return the actual project list
    projectListBuffer = None

    # Number of bytes to read from socket server
    # This variable is useful to read variable number of bytes. Specially applicable when reading large requests from host
    readBufferSize = 4096

    # initialize the global lock object
    ivzlock = ivzlockmanager.lockmanager()

    # Handle to deflate process
    deflate_process = None

    # Handle to deflate process
    enflate_process = None

    # This variable will be set when SLEEP is called
    # RESUME will reset this flag again
    # Once this flag is set, all incoming requests will be ignored
    sleepMode = False

    # This is a string which is accumulated with ACC_TARGET_BUFFER API
    # The next API other than ACC_TARGET_BUFFER will consume this targetAccBuffer as it is
    targetAccBuffer = ''

    # This is a string to store huge result of database query from host app
    # Once poppulated, this buffer will transfer data in chunks for host to read
    hostAccBuffer = ''

    # Chunk size for copying string data to host. This refers to string length subset we are going to copy
    CHUNK = 128

    # Global variable to maintain the status of copy operation
    hostAccIndex = 0

    while True:
        
        try:

            s.bind((host, port))
            # Allow upto 6 connections now
            s.listen(6)

            break

        except Exception as e:

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Failed to launch socket server. Retrying in 6 seconds ...')
                print('launchIVZSocketServer : Exception is : ' + str(e))

            sleep(6)


    while True:

        try:

            # accept is a blocking call. Unless a connection establishes, control remains here
            c, addr = s.accept()     # Establish connection with client.

        except error:

            # Handle web query here

            print("\nTimedout here :\n")
            print(error)
            continue

        if not ivzlock.isLockAcquired():
            # Check if lock is yet to be taken by any client
            ivzlock.acquireLock(str(addr[0]))

            if ivzConstants.ivz_debug_level >= 3:
                print("launchIVZSocketServer : client " + str(addr[0]) + " acquired lock.")

        else:

            if not ivzlock.requestTargetAccess(str(addr[0])):
                # Chekc if target can be accessed by requesting client
                if ivzConstants.ivz_debug_level >= 1:
                    # Put error message in the queue
                    print("launchIVZSocketServer : Lock acquired by someone else. Your request will be ignored.")

                try:
                    
                    # Consume the request. Just to make sure it goes away
                    #request = c.recv(4096)
                    request = c.recv(readBufferSize)

                except error as msg:

                    if ivzConstants.ivz_debug_level >= 1:
                       print("launchIVZSocketServer : recv failed with error message : " + str(msg))

                    continue


                # Send message to host, telling the request was ignored
                resultmessage = ivzencryption.ivzencryptmessage("REQUESTIGNORED")
                c.send(resultmessage)

                continue

            else:

                if ivzConstants.ivz_debug_level >= 3:
                    print("launchIVZSocketServer : client " + str(addr[0]) + " extended lock.")

                ivzlock.extendLock(str(addr[0]))


        if ivzConstants.ivz_debug_level >= 1:
            print('launchIVZSocketServer : Got connection from', addr)

        try:

            #request = c.recv(4096)
            request = c.recv(readBufferSize)

        except error as msg:

            if ivzConstants.ivz_debug_level >= 1:
                print("launchIVZSocketServer : recv failed with error message : " + str(msg))

            continue

        #print('launchIVZSocketServer : Printing request : ' + str(request))

        # Any changes made to readBufferSize are only applicable for one IVZ request ONLY
        # This is to ensure that we dont accidently end up reading invalid number of bytes
        if readBufferSize != 4096:

            if ivzConstants.ivz_debug_level >= 1:
                print("launchIVZSocketServer : Resetting readBufferSize to 4096")

            readBufferSize = 4096

        try:
            #decrypt the message received from host
            #ivzmessage = ivzencryption.ivzdecryptmessage(request)

            # Android writeUTF adds 2 bytes at the beginning to represent the size
            ivzmessage = request.decode('utf-8')[2:]

            if ivzConstants.ivz_debug_level >= 2:
                print('launchIVZSocketServer : Received request = ' + str(ivzmessage))

        except Exception as e:

            if ivzConstants.ivz_debug_level >= 1:
                print("launchIVZSocketServer : Caught exception " + str(e))

            continue


        decoded = ivzmessage.split('*IVZ*')

        print(str(decoded))

        # We have agreed to allow processing APIs on target even in sleep mode
        # Under one condition that dislay loop should not resume
        '''
        if sleepMode == True and decoded[0] not in ['RESUME', 'GET_TARGET_MAC_ADDRESS'] :

            if ivzConstants.ivz_debug_level >= 1:
                print("launchIVZSocketServer : Device is in sleep mode. Ignoring request")

            # Send a response to host saying that target is sleeping
            resultmessage = ivzencryption.ivzencryptmessage("SLEEPMODE")
            c.send(resultmessage)

            continue
        '''


        if decoded[0] == 'PUT':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received PUT command')

            result = ivzProjectManager.insertProjectEntry(decoded[1])

            # Empty the syncQueue first
            #while not syncQueue.empty():
            #    syncQueue.get()

            #syncQueue.put(decoded[1])

            resultmessage = ivzencryption.ivzencryptmessage(result)
            c.send(resultmessage)

        elif decoded[0] == 'PING':
            # This message is only utilized to keep the acquired lock alive by client
            # it does not perform any action.
            # This is particularly important when client is uploading a project and wish to keep the lock alive

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received PING command')

            resultmessage = ivzencryption.ivzencryptmessage("ACK")
            c.send(resultmessage)


        elif decoded[0] == 'RESET_ACC_TARGET_BUFFER':
            # This API will simply resets the targetAccBuffer

            if ivzConstants.ivz_debug_level >= 3:
                print('launchIVZSocketServer : Received RESET_ACC_TARGET_BUFFER command')

            targetAccBuffer = ''

            resultmessage = ivzencryption.ivzencryptmessage("ACK")
            c.send(resultmessage)


        elif decoded[0] == 'ACC_TARGET_BUFFER':
            # This API will simply accumulate the targetAccBuffer to be consumed by some API

            if ivzConstants.ivz_debug_level >= 3:
                print('launchIVZSocketServer : Received ACC_TARGET_BUFFER command')

            targetAccBuffer += decoded[1]

            resultmessage = ivzencryption.ivzencryptmessage("ACK")
            c.send(resultmessage)


        elif decoded[0] == 'ACC_HOST_BUFFER':
            # This API will copy CHUNK characters from hostAccBuffer in parts to host app using hostAccIndex for tracking

            if ivzConstants.ivz_debug_level >= 3:
                print('launchIVZSocketServer : Received ACC_HOST_BUFFER command')


            if hostAccIndex >= len(hostAccBuffer):

                hostAccIndex = 0
                hostAccBuffer = ''

                resultmessage = ivzencryption.ivzencryptmessage("ACC_HOST_BUFFER_CONSUMED*IVZ*")
                c.send(resultmessage)

            else:

                resultmessage = ivzencryption.ivzencryptmessage(hostAccBuffer[hostAccIndex:hostAccIndex + CHUNK])
                c.send(resultmessage)

                hostAccIndex += CHUNK


        elif decoded[0] == 'SET':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received SET command')

            projectName = decoded[1]
            status = decoded[2]
            result = ivzProjectManager.setProjectStatus(projectName, status)

            if ivzConstants.ivz_debug_level >= 1:
                print("launchIVZSocketServer : setProjectStatus returned : " + str(result))

            resultmessage = ivzencryption.ivzencryptmessage(result)
            c.send(resultmessage)

        elif decoded[0] == 'QUERY_RESPONSE_SIZE_GETDIRTYLIST':

            # This API not only calculates the response size but also executes actual API and stores the result
            # call to GETDIRTYLIST will then actually return the dirty list
            # QUERY_RESPONSE_SIZE_GETDIRTYLIST and GETDIRTYLIST should be used in conjunction and same order

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received QUERY_RESPONSE_SIZE_GETDIRTYLIST COMMAND')

            projectPath = decoded[1]
            filename = decoded[2]
            result = ivzGetDirtyList.findMissingMedia(projectPath, filename)

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Printing result of findMissingMedia : ' + str(result))

            resultmessage = ivzencryption.ivzencryptmessage(result)

            dirtyListBuffer['projectPath'] = projectPath
            dirtyListBuffer['encryptedDirtyList'] = resultmessage

            # Add 256 extra bytes, just to be on safer side
            responseSize = getsizeof(resultmessage) + 256

            # Return size of the response 
            c.send(ivzencryption.ivzencryptmessage(str(responseSize)))

        elif decoded[0] == 'GETDIRTYLIST':

            '''
            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received GETDIRTYLIST COMMAND')

            projectPath = decoded[1]
            filename = decoded[2]
            result = ivzGetDirtyList.findMissingMedia(projectPath, filename)

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Printing result of findMissingMedia : ' + str(result))

            resultmessage = ivzencryption.ivzencryptmessage(result)
            c.send(resultmessage)
            '''

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received GETDIRTYLIST COMMAND')

            projectPath = decoded[1]

            if dirtyListBuffer == {}:

                if ivzConstants.ivz_debug_level >= 1:
                    print('launchIVZSocketServer : GETDIRTYLIST : Something went wrong !')

                # Something went wrong. Host has asked for a project for which QUERY_RESPONSE_SIZE_GETDIRTYLIST was not envoked
                c.send(ivzencryption.ivzencryptmessage("ERROR"))
            
            elif dirtyListBuffer['projectPath'] == projectPath:
                
                # Return encrypted dirty list string
                c.send(str(dirtyListBuffer['encryptedDirtyList']))

                # After reading from the buffer, reset it to empty
                dirtyListBuffer = {}

            else:

                if ivzConstants.ivz_debug_level >= 1:
                    print('launchIVZSocketServer : Received incorrect projectPath = ' + str(projectPath))

                # Something went wrong. Host has asked for a project for which QUERY_RESPONSE_SIZE_GETDIRTYLIST was not envoked
                c.send(ivzencryption.ivzencryptmessage("ERROR"))

        elif decoded[0] == 'REMOVE':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received REMOVE COMMAND')

            if not sleepMode:

                # Terminate the display queue
                ackQueue.put('DELETE_SLEEP')

                # wait till sleep finishes
                res = syncQueue.get()
            
            projectName = decoded[1]
            result = ivzProjectManager.deleteProject(projectName)

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Printing result of deleteProject : ' + str(result))

            #resultmessage = ivzencryption.ivzencryptmessage(str(result))
            #c.send(resultmessage)

            # Empty the syncQueue first
            #while not syncQueue.empty():
            #    syncQueue.get()

            #syncQueue.put(decoded[1])

            resultmessage = ivzencryption.ivzencryptmessage(str(result))
            c.send(resultmessage)

            if not sleepMode:

                # Resume the display queue
                ackQueue.put('DELETE_RESUME')

                # wait till resume happens
                res = syncQueue.get()


        elif decoded[0] == 'GENERATE_ENC':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received GENERATE_ENC COMMAND')

            projectName = decoded[1]
            projectPath = path.join(ivzConstants.projects_dir, projectName)

            enflate_process = Process(target=prepareEncProject, args=(projectPath, projectName,))
            enflate_process.start()

            resultmessage = ivzencryption.ivzencryptmessage(str("DONE"))
            c.send(resultmessage)

        elif decoded[0] == 'IS_PROJECT_ENCRYPTION_RUNNING':

            # This API is used to query whether project encryption is happening or not
            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received IS_PROJECT_ENCRYPTION_RUNNING')

            result_message = "None"

            if enflate_process == None:
                result_message = "ERROR"

            elif enflate_process.is_alive():
                result_message = "RUNNING"

            else:
                result_message = "COMPLETE"
                enflate_process.terminate()
                enflate_process = None

            resultmessage = ivzencryption.ivzencryptmessage(result_message)
            c.send(resultmessage)


        elif decoded[0] == 'DONE_ENC_DOWNLOAD':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received DONE_ENC_DOWNLOAD')

            projectName = decoded[1]

            result = None

            if path.exists(path.join(ivzConstants.ivz_tmp_dir, projectName)):

                remove(path.join(ivzConstants.ivz_tmp_dir, projectName))
                result = "PASS"
            else:
                result = "ERROR"

            resultmessage = ivzencryption.ivzencryptmessage(str(result))
            c.send(resultmessage)


        elif decoded[0] == 'UPDATEQUEUE':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received UPDATEQUEUE')

            # Make sure we do not relaunch display queue in sleep mode
            if sleepMode:

                resultmessage = ivzencryption.ivzencryptmessage("DONE")
                c.send(resultmessage)
                continue

            ackQueue.put('UPDATEQUEUE')

            # wait till sleep finishes
            res = syncQueue.get()
 
            resultmessage = ivzencryption.ivzencryptmessage("DONE")
            c.send(resultmessage)

        elif decoded[0] == 'SLEEP':

            # Empty the syncQueue first
            #while not syncQueue.empty():
            #    syncQueue.get()

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received SLEEP')

            resultmessage = ivzencryption.ivzencryptmessage("DONE")

            ackQueue.put('SLEEP')

            # wait till sleep is done
            res = syncQueue.get()


            if decoded[1] == 'NETWORK_OP':

                rotation = ivzConstants.getRotationFromConfig()
				
                if decoded[2] == 'UPLOADING':

                    imageLaunchCommand = "/home/pi/IVZ/IVZEXTERN/IMAGE_RENDERER 3600 0 0 0 1 1 /home/pi/IVZ/IVZEXTERN/uploading.png"

                elif decoded[2] == 'DOWNLOADING':

                    imageLaunchCommand = "/home/pi/IVZ/IVZEXTERN/IMAGE_RENDERER 3600 0 0 0 1 1 /home/pi/IVZ/IVZEXTERN/downloading.png"

                xPos = 0
                yPos = 0
                wd = ivzConstants.displayWidth
                ht = ivzConstants.displayHeight 


                subCommand = " " + str(xPos) + " " + str(yPos) + " " + str(wd) + " " + str(ht)

                imageLaunchCommand = imageLaunchCommand + subCommand

                imageProcess = Process(target=imageRunner, args=(imageLaunchCommand,))
                imageProcess.start()


            #syncQueue.put(decoded[1])

            #c.send('DONE')
            c.send(resultmessage)

            # Set the variable to actually ignore incoming requests
            sleepMode = True

        elif decoded[0] == 'RESUME':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received RESUME')

            if decoded[1] == 'NETWORK_OP':
                system("sudo pkill IMAGE_RENDERER")

            ackQueue.put('RESUME')

            # wait till resume is done
            res = syncQueue.get()

            resultmessage = ivzencryption.ivzencryptmessage("DONE")
            c.send(resultmessage)

            # Reset the variable to resume device
            sleepMode = False

        elif decoded[0] == 'TERMINATE_TARGET_APP':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received TERMINATE_TARGET_APP')

            resultmessage = ivzencryption.ivzencryptmessage("DONE")

            ackQueue.put('TERMINATE_TARGET_APP')
            c.send(resultmessage) 

        elif decoded[0] == 'DISPLAYOTA':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received DISPLAYOTA')

            # Generate 6 digit random number between 111111 and 999999
            key = randint(111111,999999)

            #key = ivzOTA.generateAndDisplayOTA(decoded[1])

            print('launchIVZSocketServer : Printing OTA : ' + str(key))

            if key == None:
                resultmessage = ivzencryption.ivzencryptmessage("ERROR")
                c.send(resultmessage)
            else:
                #resultmessage = ivzencryption.ivzencryptmessage(str(key))
                #c.send(resultmessage)

                c.send(str(key))

                ota_process = Process(target=ivzOTA.generateAndDisplayOTA, args=(decoded[1], key,))
                ota_process.start()
                ota_process.join()

        elif decoded[0] == 'VERIFYOTA':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received VERIFYOTA')

            result = ivzOTA.verifyOTA(decoded[1], decoded[2])

            if result == True:
                #resultmessage = ivzencryption.ivzencryptmessage("PASS")
                resultmessage = "PASS"
                c.send(resultmessage)
            else:
                #resultmessage = ivzencryption.ivzencryptmessage("FAIL")
                resultmessage = "FAIL"
                c.send(resultmessage)

        elif decoded[0] == 'QUERYMEM':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received QUERYMEM')

            # Return avalibalbe memory on target device at /roPopen, PIPEsubprocess.Popen(['df -k --output=avail /root'], shell=True, stdout=subprocess.PIPE)
            p = Popen(['df -k --output=avail /root'], shell=True, stdout=PIPE)
            availableMem = p.communicate()[0].split('\n')[1]

            resultmessage = ivzencryption.ivzencryptmessage(availableMem)
            c.send(resultmessage)

        elif decoded[0] == 'PROJECTSIZE':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received PROJECTSIZE')

            # Query the size of existing project
            projectPath = path.join(ivzConstants.projects_dir, decoded[1])
            command = 'du -sk ' + "\'" + projectPath + "\'"

            if path.exists(projectPath):

                p = Popen([command], shell=True, stdout=PIPE)
                projectSize = p.communicate()[0].split('\t')[0]

                resultmessage = ivzencryption.ivzencryptmessage(projectSize)
                c.send(resultmessage)
            else:

                if ivzConstants.ivz_debug_level >= 1:
                    print('launchIVZSocketServer : Invalid project name')

                resultmessage = ivzencryption.ivzencryptmessage("-1")
                c.send(resultmessage)

        elif decoded[0] == 'QUERY_RESPONSE_SIZE_PROJECTLIST':

            # This API not only calculates the response size but also executes actual API and stores the result
            # call to PROJECTLIST will then actually return the dirty list
            # QUERY_RESPONSE_SIZE_PROJECTLIST and PROJECTLIST should be used in conjunction and in the same order

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received command QUERY_RESPONSE_SIZE_PROJECTLIST')

            projectList = []

            result = ivzProjectManager.getProjectList()

            if result != None :
                for i in result:
                    projectList.append(i[0])

            if projectList == []:
                projectListBuffer = ivzencryption.ivzencryptmessage("FAIL")
            else:
                projectListBuffer = ivzencryption.ivzencryptmessage(str(projectList))

            # Add 256 extra bytes, just to be on safer side
            responseSize = getsizeof(projectListBuffer) + 256

            # Return size of the response 
            c.send(ivzencryption.ivzencryptmessage(str(responseSize)))

        elif decoded[0] == 'PROJECTLIST':

            # Returns list of available projects by querying the database

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received command PROJECTLIST')

            '''
            projectList = []

            result = ivzProjectManager.getProjectList()

            if result != None :
                for i in result:
                    projectList.append(i[0])

            if projectList == []:
                resultmessage = ivzencryption.ivzencryptmessage("FAIL")
                c.send(resultmessage)
            else:
                resultmessage = ivzencryption.ivzencryptmessage(str(projectList))
                c.send(resultmessage)
            '''

            if projectListBuffer is not None:

                c.send(projectListBuffer)
                # Reset the buffer for next usage
                projectListBuffer = None
            else:

                if ivzConstants.ivz_debug_level >= 1:
                    print('launchIVZSocketServer : Error in PROJECTLIST query. Possibly QUERY_RESPONSE_SIZE_PROJECTLIST was not envoked before PROJECTLIST')

                # Something went wrong. Possibly QUERY_RESPONSE_SIZE_PROJECTLIST was not envoked
                c.send(ivzencryption.ivzencryptmessage("ERROR"))


        elif decoded[0] == 'VERIFYTARGET':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received VERIFYTARGET')

            resultmessage = ivzencryption.ivzencryptmessage("IVZ")
            c.send(resultmessage)

        elif decoded[0] == 'VERIFYHASH':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received VERIFYHASH')

            projectName = decoded[1]
            projecthashonhost = decoded[2]

            resultmessage = ""

            if not path.exists(path.join(ivzConstants.projects_dir, projectName)):
                resultmessage = ivzencryption.ivzencryptmessage("MISMATCH")
            else:

                projecthashontarget = str(dirhash(path.join(ivzConstants.projects_dir, projectName), excluded_files=['.gitkeep']))

                if projecthashonhost == projecthashontarget:
                    resultmessage = ivzencryption.ivzencryptmessage("MATCH")
                else:
                    resultmessage = ivzencryption.ivzencryptmessage("MISMATCH")

            c.send(resultmessage)

        elif decoded[0] == 'VERIFY_ENC_HASH':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received VERIFY_ENC_HASH')

            projectName = decoded[1]
            projecthashonhost = decoded[2]

            resultmessage = ""

            if not path.exists(path.join(ivzConstants.projects_dir, projectName)):
                resultmessage = ivzencryption.ivzencryptmessage("MISMATCH")
            else:

                if not path.isfile(path.join(ivzConstants.projects_dir, projectName)):
                    resultmessage = ivzencryption.ivzencryptmessage("ERROR")
                else:

                    projecthashontarget = str(hash_file(path.join(ivzConstants.projects_dir, projectName)))

                    if projecthashonhost == projecthashontarget:
                        resultmessage = ivzencryption.ivzencryptmessage("MATCH")
                    else:
                        resultmessage = ivzencryption.ivzencryptmessage("MISMATCH")

            c.send(resultmessage)


        elif decoded[0] == 'VERIFY_ENC_HASH_TMP':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received VERIFY_ENC_HASH_TMP')

            projectName = decoded[1]
            projecthashonhost = decoded[2]

            resultmessage = ""

            if not path.exists(path.join(ivzConstants.ivz_tmp_dir, projectName)):
                resultmessage = ivzencryption.ivzencryptmessage("MISMATCH")
            else:

                projecthashontarget = str(hash_file(path.join(ivzConstants.ivz_tmp_dir, projectName)))

                if projecthashonhost == projecthashontarget:
                    resultmessage = ivzencryption.ivzencryptmessage("MATCH")
                else:
                    resultmessage = ivzencryption.ivzencryptmessage("MISMATCH")

            c.send(resultmessage)


        elif decoded[0] == 'INSERT_OR_UPDATE_PROJECT_HASH':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received INSERT_OR_UPDATE_PROJECT_HASH')

            projectname = decoded[1]
            project_hash_on_host = decoded[2]

            result = ivzProjectManager.insertOrUpdateProjectHash(projectname, project_hash_on_host)

            if result['dbfailure'] == False :

                resultmessage = ivzencryption.ivzencryptmessage("DONE")
                c.send(resultmessage)

            else:

                resultmessage = ivzencryption.ivzencryptmessage("FAILED")
                c.send(resultmessage)

        elif decoded[0] == 'CHECK_CONSISTENCY_DEFLATEED_ENC_PROJECT':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received CHECK_CONSISTENCY_DEFLATEED_ENC_PROJECT')

            projectname = decoded[1]
            duplicate_status = decoded[2]
            result = None

            if duplicate_status == 'DUPLICATE':
                result = ivzxmlparser.verifyProjectConsistency(path.join(ivzConstants.ivz_tmp_dir, projectname))
            else:
                result = ivzxmlparser.verifyProjectConsistency(path.join(ivzConstants.projects_dir, projectname))

            if result == False :
    
                if duplicate_status == 'DUPLICATE':
                    cleanup_command = 'sudo rm -rf ' + str(path.join(ivzConstants.ivz_tmp_dir, projectname))
                else:
                    cleanup_command = 'sudo rm -rf ' + str(path.join(ivzConstants.projects_dir, projectname))

                # Clean up the directory if consistency fails
                system(cleanup_command)

                resultmessage = ivzencryption.ivzencryptmessage("FAILED")
                c.send(resultmessage)

            else:

                # Give enought permissions to project directory,
                # Otherwise operations like thumbnail download will fail
                pi_chown_cmd = 'sudo chown -R pi:pi ' + str(path.join(ivzConstants.projects_dir, projectname))
                system(pi_chown_cmd)

                resultmessage = ivzencryption.ivzencryptmessage("PASS")
                c.send(resultmessage)

        elif decoded[0] == 'DEFLATE_ENC_PROJECT':

            # This API is used to deflate enc file uploaded on target

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received DEFLATE_ENC_PROJECT')

            projectname = decoded[1]
            duplicate_status = decoded[2]

            if duplicate_status == 'DUPLICATE':
                projectPath = path.join(ivzConstants.ivz_tmp_dir, projectname)
            else:
                projectPath = path.join(ivzConstants.projects_dir, projectname)


            # If the enc file does not exists, return FAILED
            if not path.exists(projectPath):

                resultmessage = ivzencryption.ivzencryptmessage("FAILED")
                c.send(resultmessage)
                continue

            deflate_process = Process(target=ivzProjectManager.deflate_zip_project, args=(projectPath, projectname,))
            deflate_process.start()

            #deflate_zip_project(projectPath, projectname)

            resultmessage = ivzencryption.ivzencryptmessage("DONE")
            c.send(resultmessage)


        elif decoded[0] == 'IS_PROJECT_DEFLATION_RUNNING':

            # This API is used to query whether project deflation is happening or not
            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received IS_PROJECT_DEFLATION_RUNNING')


            result_message = "None"

            if deflate_process == None:
                result_message = "ERROR"

            elif deflate_process.is_alive():
                result_message = "RUNNING"

            else:
                result_message = "COMPLETE"
                deflate_process.terminate()
                deflate_process = None

            resultmessage = ivzencryption.ivzencryptmessage(result_message)
            c.send(resultmessage)


        elif decoded[0] == 'GET_THUMBNAIL_PATH':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received GET_THUMBNAIL_PATH')

            projectname = decoded[1]
            resultmessage = ""

            # It is possible that the requested thumbnail is not created for some reason or is still being created
            # Return error in such a case
            if not path.exists(path.join(ivzConstants.projects_dir, projectname, 'thumbnail' , 'thumbnail.mp4')):
                resultmessage = ivzencryption.ivzencryptmessage("ERROR")
                c.send(resultmessage)
            else:
                localpath = path.join(ivzConstants.projects_dir, projectname, 'thumbnail' , 'thumbnail.mp4')
                resultmessage = ivzencryption.ivzencryptmessage(str(localpath))
                c.send(resultmessage)

        elif decoded[0] == 'VERIFY_THUMBNAIL_HASH':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received VERIFY_THUMBNAIL_HASH')

            projectname = decoded[1]
            hash_value_on_host = decoded[2]
            resultmessage = ""

            # It is possible that the requested thumbnail is not created for some reason or is still being created
            # Return error in such a case
            if not path.exists(path.join(ivzConstants.projects_dir, projectname, 'thumbnail','thumbnail.mp4')):
                resultmessage = ivzencryption.ivzencryptmessage("ERROR")
                c.send(resultmessage)
            else:
                hash_value_on_target = hash_file(path.join(ivzConstants.projects_dir, projectname, 'thumbnail','thumbnail.mp4'))

                if hash_value_on_host == hash_value_on_target:
                    resultmessage = ivzencryption.ivzencryptmessage("MATCH")
                    c.send(resultmessage)
                else:
                    resultmessage = ivzencryption.ivzencryptmessage("MISMATCH")
                    c.send(resultmessage)

        elif decoded[0] == 'GET_TARGET_MAC_ADDRESS':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received GET_TARGET_MAC_ADDRESS')

            # Query the mac address
            # Note that getnode() returns MAC address of the eth0 interface
            # The returned mac address is in decimal dormat

            try:
                #mac = getnode()
                mac = ivzConstants.getMacAddress()
                mac_int = int(mac.translate(None,":.-"),16)
                resultmessage = ivzencryption.ivzencryptmessage(str(mac_int))
                c.send(resultmessage)
            except Exception as e:

                if ivzConstants.ivz_debug_level >= 1:
                    print("launchIVZSocketServer : GET_TARGET_MAC_ADDRESS . Cought exception - " + str(e))

                resultmessage = ivzencryption.ivzencryptmessage("FAIL")
                c.send(resultmessage)

        elif decoded[0] == 'INSERT_PROJECT_AFTER':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received INSERT_PROJECT_AFTER')

            afterproject = decoded[1]
            projectname = decoded[2]

            result = ivzProjectManager.insertProjectInDisplayQueue(afterproject, projectname)

            if result == False:
                resultmessage = ivzencryption.ivzencryptmessage("FAIL")
                c.send(resultmessage)
            else:
                resultmessage = ivzencryption.ivzencryptmessage("PASS")
                c.send(resultmessage)

        elif decoded[0] == 'DELETE_PROJECT_IN_DISPLAY_SEQUENCE':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received DELETE_PROJECT_IN_DISPLAY_SEQUENCE')

            projectname = decoded[1]

            result = ivzProjectManager.deleteProjectFromDisplaySequence(projectname)

            if result == False:
                resultmessage = ivzencryption.ivzencryptmessage("FAIL")
                c.send(resultmessage)
            else:
                resultmessage = ivzencryption.ivzencryptmessage("PASS")
                c.send(resultmessage)

        elif decoded[0] == 'SET_READ_RESPONSE_SIZE':

            # Sets readResponseSize variable. This is particularly important when host wants to send a huge request string

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received command - SET_READ_RESPONSE_SIZE')
                print('launchIVZSocketServer : Setting readBufferSize to : ' + str(decoded[1]))

            readBufferSize = int(decoded[1])

            c.send(ivzencryption.ivzencryptmessage("PASS"))

        elif decoded[0] == 'UPDATE_DISPLAY_SEQUENCE':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received command - UPDATE_DISPLAY_SEQUENCE')

            #tmp = decoded[1]
            #suppliedProjectList = literal_eval(tmp)

            # In case targetAccBuffer is NULL, we have hit error condition
            if targetAccBuffer == '':

                if ivzConstants.ivz_debug_level >= 1:
                    print('launchIVZSocketServer : Failed to update display sequence. Possibly because ACC_TARGET_BUFFER was not envoked ...')

                resultmessage = ivzencryption.ivzencryptmessage("ERROR")
                c.send(resultmessage)
                continue

            else:
                suppliedProjectList = literal_eval(targetAccBuffer)

                # Reset the accumulate buffer
                targetAccBuffer = ''


            result = ivzProjectManager.updateDisplayQueueSequence(suppliedProjectList)

            if result == False:
                resultmessage = ivzencryption.ivzencryptmessage("NOT_UPDATED")
                c.send(resultmessage)
            else:

                # Make sure we do not relaunch display queue in sleep mode
                if sleepMode:

                    resultmessage = ivzencryption.ivzencryptmessage("UPDATED")
                    c.send(resultmessage)
                    continue

                # Actually send command to update the queue
                ackQueue.put('UPDATE_SEQUENCE')

                # wait till update display sequence finishes
                res = syncQueue.get()

                resultmessage = ivzencryption.ivzencryptmessage("UPDATED")
                c.send(resultmessage)


        elif decoded[0] == 'UPDATE_MARQUEE_STRING':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received command - UPDATE_MARQUEE_STRING')

            #newMarqueeString = decoded[1]
            newMarqueeString = ''

            # In case targetAccBuffer is NULL, we have hit error condition
            if targetAccBuffer == '':

                if ivzConstants.ivz_debug_level >= 1:
                    print('launchIVZSocketServer : Failed to update marquee text. Possibly because ACC_TARGET_BUFFER was not envoked ...')

                resultmessage = ivzencryption.ivzencryptmessage("ERROR")
                c.send(resultmessage)
                continue

            else:

                newMarqueeString = targetAccBuffer.encode('utf-8')

                # Reset the accumulate buffer
                targetAccBuffer = ''


            result = True

            # Write the text to message.txt
            try:

                with open(path.join(ivzConstants.ivz_marquee_dir,'message.txt'), 'w') as content_file:
                    content_file.write(newMarqueeString)

            except Exception as e:

                if ivzConstants.ivz_debug_level >= 1:
                    print('launchIVZSocketServer : UPDATE_MARQUEE_STRING failed : ' + str(e))

                result = False


            if result == False:
                resultmessage = ivzencryption.ivzencryptmessage("ERROR")
                c.send(resultmessage)
            else:

                # Actually send command to update the queue
                ackQueue.put('UPDATE_MARQUEE')

                # wait till update display sequence finishes
                res = syncQueue.get()

                resultmessage = ivzencryption.ivzencryptmessage("UPDATED")
                c.send(resultmessage)


        elif decoded[0] == 'IS_PART_OF_DISPLAY_SEQUENCE':

            # checks whether project is part of display sequence
            projectName = decoded[1]

            projectName = (projectName,)

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received command IS_PART_OF_DISPLAY_SEQUENCE')

            displaySequence = ivzProjectManager.getDisplaySequence()

            result = None

            if displaySequence is not None:
                if projectName not in displaySequence:
                    result = "FALSE"
                else:
                    result = "True"
            else:
                result = "FALSE"

            resultmessage = ivzencryption.ivzencryptmessage(result)
            c.send(resultmessage)


        elif decoded[0] == 'QUERY_RESPONSE_GET_MARQUEE_STRING':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received command QUERY_RESPONSE_GET_MARQUEE_STRING')

            marqueeString = None

            try:

                with open(path.join(ivzConstants.ivz_marquee_dir,'message.txt'), 'r') as content_file:
                   marqueeString  = content_file.read().decode("utf-8")

            except Exception as e:

                if ivzConstants.ivz_debug_level >= 1:
                    print('launchIVZSocketServer : marquee string query failed : ' + str(e))

                c.send(ivzencryption.ivzencryptmessage("ERROR"))

            if marqueeString == None:
                c.send(ivzencryption.ivzencryptmessage("NONE"))
            else:

                #hostAccBuffer =  str(marqueeString)
                hostAccBuffer =  marqueeString
                c.send(ivzencryption.ivzencryptmessage("DONE"))


        elif decoded[0] == 'QUERY_RESPONSE_GET_DISPLAY_SEQUENCE':
            # Execute getDisplaySequence API and store the result in hostAccBuffer for reading later

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received command QUERY_RESPONSE_GET_DISPLAY_SEQUENCE')

            displaySequence = []

            result = ivzProjectManager.getDisplaySequence()

            if result == None:
                c.send(ivzencryption.ivzencryptmessage("NONE"))
                continue
            else:

                for i in result:
                    displaySequence.append(i[0])

            if displaySequence == []:
                c.send(ivzencryption.ivzencryptmessage("NONE"))
                continue

            # Poppulate hostAccBuffer and read in chunks later
            hostAccBuffer = str(displaySequence)

            c.send(ivzencryption.ivzencryptmessage("DONE"))


        elif decoded[0] == 'GET_PROJECT_DURATION':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received GET_PROJECT_DURATION')

            projectName = decoded[1]

            result = ivzProjectManager.getProjectDuration(projectName)

            resultmessage = ivzencryption.ivzencryptmessage(str(result))
            c.send(resultmessage)

        elif decoded[0] == 'GET_PROJECT_RESOLUTION':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received GET_PROJECT_RESOLUTION')

            projectName = decoded[1]

            filepath = path.join(ivzConstants.projects_dir, projectName, 'project.xml')

            xsize, ysize = ivzxmlparser.getWindowSize(filepath)

            result = str(xsize) + "X" + str(ysize)

            resultmessage = ivzencryption.ivzencryptmessage(str(result))
            c.send(resultmessage)

        elif decoded[0] == 'GET_TARGET_BINARY_NAME':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received GET_TARGET_BINARY_NAME')

            result = "FALSE"

            li = listdir(path.join(ivzConstants.IVZ_ROOT, 'bin'))

            for i in li:
                if 'launcher_V' in i:
                    result = i

            resultmessage = ivzencryption.ivzencryptmessage(str(result))
            c.send(resultmessage)

        elif decoded[0] == 'UPDATE_SSID':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received UPDATE_SSID')

            if not path.exists("/etc/hostapd/hostapd.conf"):
                resultmessage = ivzencryption.ivzencryptmessage("ERROR")
                c.send(resultmessage)
                continue

            newName = decoded[1]

            update_command = "sudo /home/pi/IVZ/scripts/update_ssid.sh " + str(newName)

            system(update_command)

            resultmessage = ivzencryption.ivzencryptmessage("SUCCESS")
            c.send(resultmessage)
            
            # Update ssid requires hostapd to be restarted
            # Restarting hostapd service is making hotspot non discoverble as of this writing
            #system("sudo service hostapd restart")

            # This will reboot the system
            ackQueue.put('TERMINATE_TARGET_APP')

        elif decoded[0] == 'UPDATE_WLAN_PARAPHRASE':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received UPDATE_WLAN_PARAPHRASE')

            if not path.exists("/etc/hostapd/hostapd.conf"):
                resultmessage = ivzencryption.ivzencryptmessage("ERROR")
                c.send(resultmessage)
                continue

            newPhrase = decoded[1]

            update_command = "sudo /home/pi/IVZ/scripts/update_wlan_password.sh " + str(newPhrase)

            system(update_command)

            resultmessage = ivzencryption.ivzencryptmessage("SUCCESS")
            c.send(resultmessage)
            
            # Update ssid requires hostapd to be restarted
            # Restarting hostapd service is making hotspot non discoverble as of this writing
            #system("sudo service hostapd restart")

            # This will reboot the system
            ackQueue.put('TERMINATE_TARGET_APP')

        elif decoded[0] == 'QUERY_WLAN_CREDENTIALS':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received QUERY_WLAN_CREDENTIALS')

            if not path.exists("/etc/hostapd/hostapd.conf"):
                resultmessage = ivzencryption.ivzencryptmessage("ERROR")
                c.send(resultmessage)
                continue
 
            command = "sudo grep -n \"This is the name of the network\" /etc/hostapd/hostapd.conf | cut -f1 -d:"
            p = Popen([command], shell=True, stdout=PIPE, stderr=PIPE)
            number = p.communicate()[0].replace('\n', '')

            if not number.isdigit():

                resultmessage = ivzencryption.ivzencryptmessage("ERROR")
                c.send(resultmessage)
                continue

            number = int(number) + 1

            extract_line = "sudo sed -n '" + str(number) + "p'" " /etc/hostapd/hostapd.conf"
            p = Popen([extract_line], shell=True, stdout=PIPE, stderr=PIPE)
            result = p.communicate()[0].replace('\n','')

            ssid = result.split('=')[1]

            #print(ssid)

            command = "sudo grep -n \"The network passphrase\" /etc/hostapd/hostapd.conf | cut -f1 -d:"
            p = Popen([command], shell=True, stdout=PIPE, stderr=PIPE)
            number = p.communicate()[0].replace('\n', '')

            if not number.isdigit():

                resultmessage = ivzencryption.ivzencryptmessage("ERROR")
                c.send(resultmessage)
                continue

            number = int(number) + 1

            extract_line = "sudo sed -n '" + str(number) + "p'" " /etc/hostapd/hostapd.conf"
            p = Popen([extract_line], shell=True, stdout=PIPE, stderr=PIPE)
            result = p.communicate()[0].replace('\n','')

            paraphrase = result.split('=')[1]

            #print(paraphrase)

            result = str(ssid) + "*IVZ*" + str(paraphrase)

            resultmessage = ivzencryption.ivzencryptmessage(result)
            c.send(resultmessage)

        elif decoded[0] == 'GET_UPDATED_BINARY_SIZE':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received GET_UPDATED_BINARY_SIZE')

            binaryPath = path.join("/home/pi/IVZ/bin/updated_bin", decoded[1])

            # If file does not exists, return 0
            if not path.exists(binaryPath):
                resultmessage = ivzencryption.ivzencryptmessage("0")
                c.send(resultmessage)
                continue

            command = 'du -sk ' + "\'" + binaryPath + "\'"

            if path.exists(binaryPath):

                p = Popen([command], shell=True, stdout=PIPE)
                binarySize = p.communicate()[0].split('\t')[0]

                resultmessage = ivzencryption.ivzencryptmessage(binarySize)
                c.send(resultmessage)

            else:

                if ivzConstants.ivz_debug_level >= 1:
                    print('launchIVZSocketServer : Invalid project name')

                resultmessage = ivzencryption.ivzencryptmessage("0")
                c.send(resultmessage)


        elif decoded[0] == 'RELEASE_GLOBAL_LOCK':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received RELEASE_GLOBAL_LOCK')

            result = "FAILED"

            if ivzlock.isLockAcquired():

                ivzlock.releaseLock()
                result = "SUCCESS"

            resultmessage = ivzencryption.ivzencryptmessage(result)
            c.send(resultmessage)

        elif decoded[0] == 'UPDATE_SLEEP':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received REMOVE COMMAND')

            # Terminate the display queue
            ackQueue.put('DELETE_SLEEP')

            # wait till sleep finishes
            res = syncQueue.get()

            print("launchIVZSocketServer : ERROR : UPDATE_SLEEP : res = " + str(res))
            
            resultmessage = ivzencryption.ivzencryptmessage(str("DONE"))
            c.send(resultmessage)

        elif decoded[0] == 'UPDATE_RESUME':

            # Resume the display queue
            ackQueue.put('DELETE_RESUME')

            # wait till resume happens
            res = syncQueue.get()
            print("launchIVZSocketServer : ERROR : UPDATE_RESUME : res = " + str(res))

            resultmessage = ivzencryption.ivzencryptmessage(str("DONE"))
            c.send(resultmessage)

        elif decoded[0] == 'CREATE_TMP_STRUCTURE':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received CREATE_TMP_STRUCTURE')

            if not path.exists(ivzConstants.ivz_tmp_dir):
                mkd_command = 'mkdir ' + ivzConstants.ivz_tmp_dir
                system(mkd_command)

            projectName = decoded[1]
            tmp_project_path = path.join(ivzConstants.ivz_tmp_dir, str(projectName))
 
            # If project already exists in tmp directory, treat it as incomplete upload and delete
            if path.exists(tmp_project_path):
                clean_command = 'rm -rf ' + path.join(ivzConstants.ivz_tmp_dir, str(projectName))
                system(clean_command)

            # Create the necessary directory structure           
            system('mkdir ' + tmp_project_path)
            system('mkdir ' + path.join(tmp_project_path, 'images'))
            system('mkdir ' + path.join(tmp_project_path, 'videos'))
            system('mkdir ' + path.join(tmp_project_path, 'metadata'))

            pi_chown_cmd = 'sudo chown -R pi:pi ' + tmp_project_path
            system(pi_chown_cmd)

            resultmessage = None

            # Check if the folder creation passed or not
            # If not mark this API as FAILED
            if path.exists(tmp_project_path):
                resultmessage = ivzencryption.ivzencryptmessage("SUCCESS")
            else:
                resultmessage = ivzencryption.ivzencryptmessage("FAILED")

            c.send(resultmessage)


        elif decoded[0] == 'TMP_PROJECTSIZE':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received TMP_PROJECTSIZE')

            projectPath = path.join(ivzConstants.ivz_tmp_dir, decoded[1])

            if path.exists(projectPath):

                # Query the size of existing project
                command = 'du -sk ' + "\'" + projectPath + "\'"

                p = Popen([command], shell=True, stdout=PIPE)
                projectSize = p.communicate()[0].split('\t')[0]

                resultmessage = ivzencryption.ivzencryptmessage(projectSize)
                c.send(resultmessage)

            else:

                if ivzConstants.ivz_debug_level >= 1:
                    print('launchIVZSocketServer : TMP_PROJECTSIZE invalid project name')

                resultmessage = ivzencryption.ivzencryptmessage("-1")
                c.send(resultmessage)


        elif decoded[0] == 'UPDATE_ROTATION':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received UPDATE_ROTATION')

            if not path.exists("/boot/config.txt"):
                resultmessage = ivzencryption.ivzencryptmessage("ERROR")
                c.send(resultmessage)
                continue

            rotation_angle = decoded[1]

            possible_values = ['0', '90', '180', '270']

            if rotation_angle not in possible_values:

                resultmessage = ivzencryption.ivzencryptmessage("ERROR")
                c.send(resultmessage)
                continue

            else:

                rotation_index = possible_values.index(rotation_angle)

                update_command = "sudo /home/pi/IVZ/scripts/update_rotation.sh " + str(rotation_index)

                system(update_command)

                resultmessage = ivzencryption.ivzencryptmessage("SUCCESS")
                c.send(resultmessage)
            
                # Update rotation requires a reboot

                # This will reboot the system
                ackQueue.put('TERMINATE_TARGET_APP')


        elif decoded[0] == 'GET_DISPLAY_ROTATION':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received GET_DISPLAY_ROTATION')

            if not path.exists("/boot/config.txt"):
                resultmessage = ivzencryption.ivzencryptmessage("ERROR")
                c.send(resultmessage)
                continue

            command = "sudo grep display_rotate /boot/config.txt"
            p = Popen([command], shell=True, stdout=PIPE, stderr=PIPE)
            res = p.communicate()[0].replace('\n','')
            rot_index = int(res.split('=')[1])

            if rot_index > 3:
                resultmessage = ivzencryption.ivzencryptmessage("ERROR")
                c.send(resultmessage)
                continue

            possible_values = ['0', '90', '180', '270']

            current_rotation = possible_values[rot_index]

            resultmessage = ivzencryption.ivzencryptmessage(current_rotation)
            c.send(resultmessage)


        elif decoded[0] == 'IS_TARGET_WEB_CONNECTED':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received IS_TARGET_WEB_CONNECTED')

            command = "sudo /home/pi/IVZ/scripts/checkWebConnection.sh"
            p = Popen([command], shell=True, stdout=PIPE, stderr=PIPE)
            res = p.communicate()[0].replace('\n','')

            if res == "CONNECTED":
                #resultmessage = ivzencryption.ivzencryptmessage("TRUE")
                resultmessage = "TRUE"
            else:
                #resultmessage = ivzencryption.ivzencryptmessage("FALSE")
                resultmessage = "FALSE"

            c.send(resultmessage)

        elif decoded[0] == 'UPDATE_WPA_SUPPLICANT':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received UPDATE_WPA_SUPPLICANT')

            if not path.exists("/etc/wpa_supplicant/wpa_supplicant.conf"):
                resultmessage = ivzencryption.ivzencryptmessage("ERROR")
                c.send(resultmessage)
                continue

            newSSID = decoded[1]
            newPSK  =  decoded[2]

            update_command = "sudo /home/pi/IVZ/scripts/update_wpa_supplicant.sh " + str(newSSID) + " " + str(newPSK)
            system(update_command)

            #resultmessage = ivzencryption.ivzencryptmessage("SUCCESS")
            resultmessage = "SUCCESS"
            c.send(resultmessage)
            
            # This will reboot the system
            ackQueue.put('TERMINATE_TARGET_APP')


        elif decoded[0] == 'VERIFY_CMA_CREDENTIALS':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received VERIFY_CMA_CREDENTIALS')

            uname = decoded[1]
            passwd  =  decoded[2]

            try :

                res = post('http://192.168.0.104/castmyads/app/login.php', data={'username': uname, 'password': passwd})
                #res = post('http://172.24.1.135/castmyads/app/login.php', data={'username':uname, 'password':passwd})

            except Exception as e:

                if ivzConstants.ivz_debug_level >= 1:
                    print('launchIVZSocketServer : Caught exception ' + str(e))

                resultmessage = ivzencryption.ivzencryptmessage("FAILED")
                c.send(resultmessage)
                continue

            result = str(res.content)

            print(result)

            if result == 'SUCCESS':

                resultmessage = ivzencryption.ivzencryptmessage("SUCCESS")

            elif result == 'FAILED':

                resultmessage = ivzencryption.ivzencryptmessage("FAILED")

            elif result == 'INVALID_CREDENTIALS':

                resultmessage = ivzencryption.ivzencryptmessage("INVALID_CREDENTIALS")

            else:

                resultmessage = ivzencryption.ivzencryptmessage("FAILED")

            c.send(resultmessage)


        elif decoded[0] == 'QUERY_CPU_ID':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received QUERY_CPU_ID')

            command = "cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2"
            p = Popen([command], shell=True, stdout=PIPE, stderr=PIPE)
            cpu_id = p.communicate()[0].replace('\n','')

            c.send(str(cpu_id))


        elif decoded[0] == 'REGISTER_TARGET_DEVICE':

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received REGISTER_TARGET_DEVICE')

            customerID = decoded[1]
            location = decoded[2]

            command = "cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2"
            p = Popen([command], shell=True, stdout=PIPE, stderr=PIPE)
            cpu_id = p.communicate()[0].replace('\n','')            

            try :

                res = post('http://192.168.0.104/castmyads/app/registerDevice.php', data={'customerID':customerID, 'location': location, 'cpuID': cpu_id})

            except Exception as e:

                if ivzConstants.ivz_debug_level >= 1:
                    print('launchIVZSocketServer : Caught exception ' + str(e))

                resultmessage = ivzencryption.ivzencryptmessage("FAILED")
                c.send(resultmessage)
                continue

            result = str(res.content)

            print(result)

            if result == 'SUCCESS':

                resultmessage = ivzencryption.ivzencryptmessage("SUCCESS")

            elif result == 'FAILED':

                resultmessage = ivzencryption.ivzencryptmessage("FAILED")

            else:

                resultmessage = ivzencryption.ivzencryptmessage("FAILED")

            
            c.send(resultmessage)
 
        elif decoded[0] == 'GET_DIAGNOSTIC_LOGS':

            # This API generates enc file with diagnostic logs and shares its path
            # Returns ERROR if something goes wrong

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received GET_DIAGNOSTIC_LOGS')

            resultmessage = ""

            if path.exists(path.join(ivzConstants.ivz_logs_dir, 'InfoWiz_Diagnostic.enc')):
                # Delete the encrypted file if it already exists
                command = 'sudo rm ' + str(path.join(ivzConstants.ivz_logs_dir, 'InfoWiz_Diagnostic.enc'))
                system(command)

            # Create dmesg log file
            command = 'sudo dmesg > ' + str(path.join(ivzConstants.ivz_logs_dir, 'dmesg.log'))
            system(command)

            if path.exists('/tmp/zipped.tar'):
                command = 'sudo rm /tmp/zipped.tar'

            # Create zipped.tar first
            # Openssh doesn't seem to support encrypting entire directory
            # So create .tar file first. And then encrypt the tar
            command = 'sudo tar -cvf /tmp/zipped.tar ' + str(path.join(ivzConstants.ivz_logs_dir))
            system(command)

            if not path.exists('/tmp/zipped.tar'):
                if ivzConstants.ivz_debug_level >= 1:
                    print('launchIVZSocketServer : Failed to create zipped.tar')

                resultmessage = ivzencryption.ivzencryptmessage("ERROR")
                c.send(resultmessage)
                continue

             # Create 'InfoWiz_Diagnostic.enc'
            command = 'sudo openssl enc -aes-256-cbc -salt -in /tmp/zipped.tar -out ' + str(path.join(ivzConstants.ivz_logs_dir, 'InfoWiz_Diagnostic.enc')) + ' -k OgFxxMSBIDat7pj'
            system(command)

            if not path.exists(path.join(ivzConstants.ivz_logs_dir, 'InfoWiz_Diagnostic.enc')):
                if ivzConstants.ivz_debug_level >= 1:
                    print('launchIVZSocketServer : Failed to create .enc file')

                resultmessage = ivzencryption.ivzencryptmessage("ERROR")
                c.send(resultmessage)
                continue

            else:

                localpath = path.join(ivzConstants.ivz_logs_dir, 'InfoWiz_Diagnostic.enc')
                resultmessage = ivzencryption.ivzencryptmessage(str(localpath))
                c.send(resultmessage)

        else:

            if ivzConstants.ivz_debug_level >= 1:
                print('launchIVZSocketServer : Received incorrect command : ' + str(request))

            resultmessage = ivzencryption.ivzencryptmessage("FALSE")
            c.send(resultmessage)

        c.close()

#ivzProjectManager.deleteProject('sample.ivzproj')

#ivzProjectManager.initializeDatabases()
launchIVZSocketServer(None, None)

#thumbnail_process = Process(target=ivzThumbnails.ivzCreateThumbnails, args=('allinone.ivzproj',))
#thumbnail_process.start()
#print(hash_file("/home/pi/IVZ/thumbnails/allinone.ivzproj/thumbnail.mp4"))

