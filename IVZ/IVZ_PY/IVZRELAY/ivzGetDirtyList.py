from os import path, listdir, system
from json import dumps
#import sys
from xml.dom.minidom import parse
#from xml.dom.minidom import getDOMImplementation
from IVZGLOBALS import ivzConstants

#def main(argv):
def findMissingMedia(filepath, filename):
    '''
    Purpose : Generate and return a list of media files missing on target for a given project
    Params  : filepath, filename : path and name of mediaLst.xml file, consumed by this function
    Return  : List of dictionaries representing missing image and video files
    '''

    # argv[1] is project path
    # argv[2] is images list
    # argv[3] is videos list

    #hostImagesList = json.loads(argv[2])
    #hostVideosList = json.loads(argv[3])

    hostImagesList = []
    hostVideosList = []

    if path.exists(path.join(filepath, filename)):
        dom = parse(path.join(filepath, filename))
    else:
        if ivzConstants.ivz_debug_level >= 1:
            print("findMissingMedia : filename = " + str(filename) + "  does not exist ...")
        return "ERROR"

    for node in dom.getElementsByTagName('videoName'):
        ustr = node.firstChild.nodeValue
        hostVideosList.append(ustr.encode('utf-8'))
        #hostVideosList.append(node.firstChild.nodeValue)

    for node in dom.getElementsByTagName('imageName'):
        ustr = node.firstChild.nodeValue
        #hostImagesList.append(node.firstChild.nodeValue)
        hostImagesList.append(ustr.encode('utf-8'))

    try:
        targetImagesList = listdir(path.join(ivzConstants.projects_dir, path.basename(filepath), 'images'))
        targetVideosList = listdir(path.join(ivzConstants.projects_dir, path.basename(filepath), 'videos'))
    except Exception as e:        
        if ivzConstants.ivz_debug_level >= 1 :
            print("findMissingMedia : ERROR fetching media list. Error = "+ str(e))

    # deletion what is not part of the project anymore
    #print(path.join(path, 'images'))
    #print("findMissingMedia : printing hostImagesList : " + str(hostImagesList))
    #print("findMissingMedia : printing targetImagesList :" + str(targetImagesList))

    '''
    for image in targetImagesList:

        if image not in hostImagesList:
            resPath = path.join(filepath, 'images', image)

            if ivzConstants.ivz_debug_level >= 3:
                print("findMissingMedia : Removing " + str(resPath))

            cmd = "sudo rm " + resPath
            system(cmd)


    for video in targetVideosList:

        if video not in hostVideosList:
            resPath = path.join(filepath, 'videos', video)
            cmd = "sudo rm " + resPath
            system(cmd)

    '''

    missingResourceList = []

    for image in hostImagesList:

        if image not in targetImagesList:
            missingResourceList.append({'image': image})

    for video in hostVideosList:

        if video not in targetVideosList:
            missingResourceList.append({'video': video})

    resList = dumps(missingResourceList)
    #print(resList)

    return str(resList)


def deleteRemovedMedia(filepath, filename):
    '''
    Purpose : delete media files that were removed in updated project
    Params  : filepath, filename : path and name of mediaLst.xml file, consumed by this function
    Return  : True if all goes well, False otherwise
    '''

    hostImagesList = []
    hostVideosList = []

    if path.exists(path.join(filepath, filename)):
        dom = parse(path.join(filepath, filename))
    else:
        if ivzConstants.ivz_debug_level >= 1:
            print("deleteRemovedMedia : filename = " + str(filename) + "  does not exist ...")
        return False

    for node in dom.getElementsByTagName('videoName'):
        ustr = node.firstChild.nodeValue
        hostVideosList.append(ustr.encode('utf-8'))
        #hostVideosList.append(node.firstChild.nodeValue)

    for node in dom.getElementsByTagName('imageName'):
        ustr = node.firstChild.nodeValue
        #hostImagesList.append(node.firstChild.nodeValue)
        hostImagesList.append(ustr.encode('utf-8'))

    try:
        targetImagesList = listdir(path.join(ivzConstants.projects_dir, path.basename(filepath), 'images'))
        targetVideosList = listdir(path.join(ivzConstants.projects_dir, path.basename(filepath), 'videos'))
    except Exception as e:        
        if ivzConstants.ivz_debug_level >= 1 :
            print("deleteRemovedMedia : ERROR fetching media list. Error = "+ str(e))
            return False

    # deletion what is not part of the project anymore
    for image in targetImagesList:

        if image not in hostImagesList:
            resPath = path.join(ivzConstants.projects_dir, path.basename(filepath), 'images', image)

            if ivzConstants.ivz_debug_level >= 3:
                print("findMissingMedia : Removing " + str(resPath))

            cmd = "sudo rm " + resPath
            system(cmd)


    for video in targetVideosList:

        if video not in hostVideosList:
            resPath = path.join(ivzConstants.projects_dir, path.basename(filepath), 'videos', video)

            if ivzConstants.ivz_debug_level >= 3:
                print("findMissingMedia : Removing " + str(resPath))

            cmd = "sudo rm " + resPath
            system(cmd)

    return True

#if __name__ == "__main__":
#    main(sys.argv[1:])
    #main(sys.argv[0:])


