from sqlite3 import connect, Error
from shutil import rmtree, move
from Crypto.Cipher import AES
from zipfile import ZipFile, ZIP_DEFLATED
from os import path, remove, urandom, walk, sep, listdir, system, mkdir
from struct import pack, unpack, calcsize
from contextlib import closing

'''
    This file is responsible for initializing global_database.db

    Possible outcomes from this file :

    SUCCESS : When insert operation is succesful
    Integer value : Result of GET operation. Prints the ID returned for given projectName 
    ERROR : If SQL query fails or
            If python file is invoked with wrong paramater or
            If GET is invoked with non existent project name or
            If SET command fails because projectName does not exist in table

    DUPLICATE : If projectName already exists and cannot be inserted
    DONE : If updating status of projectName succeeds or
           If project is successfully removed from database
'''

from IVZPARSER import ivzxmlparser
from IVZGLOBALS import ivzConstants

#IVZ_DATABASES = r'/home/pi/IVZ/databases/'
#ivzConstants.projects_dir = r'/home/pi/IVZ/projects/'

def checkProjectConsistency(projectPath):
    """
        Checks the consistency of deflated project. Cleanup if incosistent
        Return : False if inconsistent, True otherwise
    """

    result = ivzxmlparser.verifyProjectConsistency(projectPath)

    if result == False :

        cleanup_command = 'sudo rm -rf ' + str(projectPath)

        # Clean up the directory if consistency fails
        system(cleanup_command)

	return False

    else:

        # Give enought permissions to project directory,
        # Otherwise operations like thumbnail download will fail
        pi_chown_cmd = 'sudo chown -R pi:pi ' + str(projectPath)
        system(pi_chown_cmd)

	return True

def unzip_project(projectPath, projectname):
    """
    Purpose : Helper function to unzip project
    """

    tmp_zip = path.join('/tmp', projectname)

    # Unzip file
    try:

        command = "unzip -d " + str(tmp_zip) + " " + str(projectPath)
        system(command)

    except Exception as e:
        if ivzConstants.ivz_debug_level >= 1:
            print('unzip_project : unzip_project : extractall failed with exception : ' + str(e))

        return

    # Delete the zip file
    remove(projectPath)

    # Move extracted project to projects/
    move(tmp_zip, projectPath)

def deflate_zip_project(projectPath, projectname):
    """
    Purpose : Helper function to handle project zip file deflation process
    """

    #tmp_zip = path.join('/tmp', projectname + '.zip')
    tmp_zip = path.join('/tmp', projectname + '.7z')

    try:

        # Deflate .zip file from enc file
        decrypt_file(projectPath, tmp_zip)

        # Remove enc file
        remove(projectPath)

        # Create the project directory
        mkdir(projectPath)

    except Exception as e:

        if ivzConstants.ivz_debug_level >= 1:
            print('deflate_zip_project : deflate_zip_project : deflation failed with exception : ' + str(e))

        return

    # Unzip file
    try:

        #unzip_data = ZipFile(tmp_zip, 'r')
        #unzip_data.extractall(projectPath)
        #unzip_data.close()

        command = "7z x " + str(tmp_zip) + " -o" + str(projectPath)
        system(command)

    except Exception as e:
        if ivzConstants.ivz_debug_level >= 1:
            print('deflate_zip_project : deflate_zip_project : extractall failed with exception : ' + str(e))

        return

    # Delete the zip file
    remove(tmp_zip)


def getMediaList(projectName):
    """
    Purpose : Returns a list of media files for preview download
              Format is : {'IMAGES':[] , 'VIDEOS':[] }
    """

    result = {'IMAGES':[], 'VIDEOS':[], 'METADATA':[], 'PROJECTPATH':[]}

    try:

        imagesList = listdir(path.join(ivzConstants.projects_dir, projectName, 'images'))
    except Exception as e:
        if ivzConstants.ivz_debug_level >= 1 :
            print("getMediaList : ERROR fetching imagelist. Error = "+ str(e))
       

    for i in imagesList:
        result['IMAGES'].append(i)

    try:
        videosList = listdir(path.join(ivzConstants.projects_dir, projectName, 'videos'))
    except Exception as e:
        if ivzConstants.ivz_debug_level >= 1 :
            print("getMediaList : ERROR fetching videolist. Error = "+ str(e))
 
    for i in videosList:
        result['VIDEOS'].append(i)

    try:
        metadataList = listdir(path.join(ivzConstants.projects_dir, projectName, 'metadata'))
    except Exception as e:
        if ivzConstants.ivz_debug_level >= 1 :
            print("getMediaList : ERROR fetching metadata. Error = "+ str(e))
 
    for i in metadataList:
        result['METADATA'].append(i)

    result['PROJECTPATH'] = path.join(ivzConstants.projects_dir, projectName)

    return str(result)


def initializeDatabases():
        """
        Purpose : Initializes the local database.
        :return:
        """

        createdbCmd = "touch "+ ivzConstants.IVZ_DATABASES + "global_database.db"

        system(createdbCmd)
        
        # TODO Consider using installation directory for database path Ex : c:/ProgramFiles/InfoViz/resources/db/username_database.db
        conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

        tablename = "PROJECTSLOG"

        createTableQuery = "CREATE TABLE IF NOT EXISTS " + tablename + " (ID INTEGER PRIMARY KEY  AUTOINCREMENT, \
               NAME             TEXT    NOT NULL,\
               STATUS           TEXT    NOT NULL);"

        conn.execute(createTableQuery)

        createHostTableQuery = "CREATE TABLE IF NOT EXISTS HOSTLIST (ID INTEGER PRIMARY KEY  AUTOINCREMENT, \
               MACADDRESS    TEXT    NOT NULL,\
               OTA           TEXT    NOT NULL);"

        conn.execute(createHostTableQuery)

        createHostTableQuery = "CREATE TABLE IF NOT EXISTS HASHDATA (ID INTEGER PRIMARY KEY  AUTOINCREMENT, \
               PROJECTNAME    TEXT    NOT NULL,\
               HASH           TEXT    NOT NULL);"

        conn.execute(createHostTableQuery)

        createDisplaySequenceQuery = "CREATE TABLE IF NOT EXISTS DISPLAYSEQUENCE (ID INTEGER PRIMARY KEY  AUTOINCREMENT, \
               PROJECTNAME    TEXT    NOT NULL);"

        conn.execute(createDisplaySequenceQuery)

        # This table stores the total screen duration of a given project
        createDurationTableQuery = "CREATE TABLE IF NOT EXISTS PROJECTDURATIONS (ID INTEGER PRIMARY KEY  AUTOINCREMENT, \
               PROJECTNAME    TEXT    NOT NULL,\
               DURATION       TEXT    NOT NULL);"

        conn.execute(createDurationTableQuery)

        # This table stores the total screen duration of a given project
        createCloudStatusTableQuery = "CREATE TABLE IF NOT EXISTS TARGET_MISC_STATUS (ID INTEGER PRIMARY KEY  AUTOINCREMENT, \
               PROJECTUPDATESTATUS    TEXT    NOT NULL,\
               SEQUENCEUPDATESTATUS   TEXT    NOT NULL,\
               ORIENTATIONUPDATESTATUS   TEXT    NOT NULL,\
               MARQUEEUPDATESTATUS    TEXT    NOT NULL,\
               BINUPDATESTATUS     TEXT NOT NULL);"

        conn.execute(createCloudStatusTableQuery)

        conn.close()

        initDatabaseValues()


def initDatabaseValues():
    """
        Initialize database tables to specific values        
    """

    conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

    try:
        query = "SELECT * FROM TARGET_MISC_STATUS;"
        cursor = conn.execute(query)
            
    except Error as e:

        if ivzConstants.ivz_debug_level >= 1 :
            print("initDatabaseValues : ERROR reading from TARGET_MISC_STATUS. Error = "+ str(e))

        conn.close()
        return

    li = cursor.fetchall()

    # This is to ensure that there is only 1 entry in TARGET_MISC_INFO
    if li == []:

        try:
           conn.execute("INSERT INTO TARGET_MISC_STATUS('MARQUEEUPDATESTATUS', 'PROJECTUPDATESTATUS', 'SEQUENCEUPDATESTATUS', 'ORIENTATIONUPDATESTATUS', 'BINUPDATESTATUS') VALUES('0', '0', '0', '0', '0')")
           conn.commit()

        except Error as e:

            if ivzConstants.ivz_debug_level >= 1 :
                print("initDatabaseValues : ERROR Inserting values. Error = " + str(e))

    conn.close()
    return

def getDisplaySequence():
    """
    Purpose : Get display sequence of uploaded projects

    Return : Sepcific sequence of projects for displaying in queue

    """

    result = None

    conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

    try:
        query = "SELECT PROJECTNAME FROM DISPLAYSEQUENCE ORDER BY ID ASC;"
        cursor = conn.execute(query)
        #conn.commit()
            
    except Error as e:

        if ivzConstants.ivz_debug_level >= 1 :
            print("getDisplaySequence : ERROR fetching project list. Error = "+ str(e))

        conn.close()
        return None

    li = cursor.fetchall()
    
    #li = []

    #for row in cursor:
    #    li.append(row[0])

    if li == []:

        if ivzConstants.ivz_debug_level >= 1 :
            print ("getDisplaySequence : project list is empty")

        conn.close()
        return None
    else:
        result = li

    conn.close()
    return result

def getProjectIndexInSequence(projectName):
    """
    Purpose : Identify the index of supplied project in display sequence
    Returns : Index of project name in display sequence
              -1 in error condition
    """

    if projectName is None:
        return -1

    result = getDisplaySequence()

    displaySequence = []

    return_value = -1

    if result is not None:

        for i in result:
            displaySequence.append(i[0])

        try:
            return_value = displaySequence.index(projectName)

        except:

            if ivzConstants.ivz_debug_level >= 1 :
                print("updateDisplayQueueSequence : ERROR fetching project list. project name = " + projectName)


        return return_value

    else:
        return -1



def updateDisplayQueueSequence(updatedList):
    """
    Purpose : 
    Params :

    Return :  False if display sequence is not updated 
              True if display sequence is updated
    """

    if updatedList == []:
        return False


    conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

    try:

        query = "SELECT PROJECTNAME FROM DISPLAYSEQUENCE ORDER BY ID ASC;"
        cursor = conn.execute(query)
        #conn.commit()
            
    except Error as e:

        if ivzConstants.ivz_debug_level >= 1 :
            print("updateDisplayQueueSequence : ERROR fetching project list. Error = "+ str(e))

        conn.close()
        return False


    tmp = cursor.fetchall()

    oldDisplaySequence = []

    if tmp != []:
        for i in tmp:
            oldDisplaySequence.append(str(i[0]))


    count = 0
    # check if new sequence is same as old sequence. Bailout is such a case
    if len(oldDisplaySequence) == len(updatedList):

        i = 0

        while i < len(updatedList):

            if oldDisplaySequence[i] == updatedList[i]:
                count = count + 1

            i = i + 1

    if count == len(updatedList):

        if ivzConstants.ivz_debug_level >= 1 :
            print("\nupdateDisplayQueueSequence : Supplied project sequence is same as existing sequence")

        return False


    updateCount = 0
    filteredDisplaySequence = []

    # check if none of the project is updatedList exists
    # in such a case it doesn't make sense to update the sequence
    for project in updatedList:

        flag1 = False
        flag2 = False

        # Check if project path exists
        if path.exists(path.join(ivzConstants.projects_dir, str(project))):
            flag1 = True

        # Check if project status us marked COMPLETE
        if getProjectStatus(project) == "COMPLETE":
            flag2 = True

        if flag1 == True and flag2 == True:
            updateCount = updateCount + 1
            filteredDisplaySequence.append(project)


    if updateCount == 0 or filteredDisplaySequence == []:

        if ivzConstants.ivz_debug_level >= 1 :
            print("\nupdateDisplayQueueSequence : Display queue will not be updated")

        return False


    # All is well
    # Its time to update display queue
    try:

        # Remove existing extries from the table
        query = "DELETE FROM DISPLAYSEQUENCE;"
        cursor = conn.execute(query)
        #conn.commit()

    except Error as e:

        if ivzConstants.ivz_debug_level >= 1 :
            print("updateDisplayQueueSequence : ERROR deleting values from DISPLAYSEQUENCE Table : Error = " + str(e))

        conn.close()
        return False


    for project in filteredDisplaySequence:

        try:

            conn.execute("INSERT INTO DISPLAYSEQUENCE(PROJECTNAME) VALUES (?);", (project,))

        except Error as e:

            if ivzConstants.ivz_debug_level >= 1 :
                print("updateDisplayQueueSequence : ERROR Inserting projectname in DISPLAYSEQUENCE Table. Error = " + str(e))

            conn.close()
            return False

    conn.commit()
    conn.close()
    return True

def insertProjectInDisplayQueue(afterproject, projectname):
    """
    Purpose : Insert a given project after a specified project
              If afterproject does not exist in the table, projectname will not be inserted in display queue
    Params :
              projectname   : Name of the project to be inserted in display queue
              afterproject  : Name of the project after which projectname will be inserted

    Return :  False if something goes wrong, True otherwise
    """

    updatedList = []

    conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

    try:

        query = "SELECT PROJECTNAME FROM DISPLAYSEQUENCE ORDER BY ID ASC;"
        cursor = conn.execute(query)
        #conn.commit()
            
    except Error as e:

        if ivzConstants.ivz_debug_level >= 1 :
            print("insertProjectInDisplayQueue : ERROR fetching project list. Error = "+ str(e))

        conn.close()
        return False

    li = cursor.fetchall()
    #li = []

    #for row in cursor:
    #    li.append(row[0])


    # If afterproject is NONE, it means host wants the project to be in the beginning of display queue
    if li == [] or afterproject == "NONE" :

        #print ("insertProjectInDisplayQueue : project list is empty")
        updatedList.append(projectname)


    for project in li:

        updatedList.append(str(project[0]))

        if str(project[0]) == afterproject:
            updatedList.append(projectname)


    try:

        # Remove existing extries from the table
        query = "DELETE FROM DISPLAYSEQUENCE;"
        cursor = conn.execute(query)
        #conn.commit()

    except Error as e:

        if ivzConstants.ivz_debug_level >= 1 :
            print("insertProjectInDisplayQueue : ERROR deleting values from DISPLAYSEQUENCE Table : Error = " + str(e))

        conn.close()
        return False

    for project in updatedList:

        try:

            conn.execute("INSERT INTO DISPLAYSEQUENCE(PROJECTNAME) VALUES (?);", (project,))
            #conn.commit()

        except Error as e:

            if ivzConstants.ivz_debug_level >= 1 :
                print("insertProjectInDisplayQueue : ERROR Inserting projectname in DISPLAYSEQUENCE Table. Error = " + str(e))

            conn.close()
            return False

    conn.commit()
    conn.close()
    return True


def getProjectList():
    """
    Purpose : Get list of projects in database. Order is important since it defines sequence.
              Only projects with status as "COMPLETE" will be returned

    Return : Sepcific sequence of projects for displaying in queue

    """

    result = None

    conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')
    #conn = connect(path.join(IVZ_DATABASES + 'global_database.db'))

    try:
        query = "SELECT NAME FROM PROJECTSLOG WHERE STATUS=\"COMPLETE\" ORDER BY ID ASC;"
        cursor = conn.execute(query)
        #conn.commit()
            
    except Error as e:

        if ivzConstants.ivz_debug_level >= 1 :
            print("getProjectList : ERROR fetching project list : " + str(e))

        conn.close()
        return None

    li = cursor.fetchall()

    if li == []:

        if ivzConstants.ivz_debug_level >= 1 :
            print ("getProjectList : project list is empty")

        conn.close()
        return None
    else:
        result = li

    conn.close()
    return result

def getHostEntry(MAC):
    """
    Purpose : Fetch the OTA key pushed into the database
    Param : MAC : mac address of host machine
    Return : None in case of failure
             KEY value otherwise
    """

    result = None

    conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')
 
    try:
        query = "SELECT OTA FROM HOSTLIST WHERE MACADDRESS=" + "\"" + MAC + "\""
        #query = "UPDATE HOSTLIST SET OTA=""\"" + OTA + "\"" + " WHERE MACADDRESS=" + "\"" + MAC  + "\"" + ";"
        cursor = conn.execute(query)
        #conn.commit()
            
    except Error as e:

        if ivzConstants.ivz_debug_level >= 1 :
            print("getHostEntry : ERROR fetching OTA in HOSTLIST Table : " + str(e))

        conn.close()
        return None

    li = cursor.fetchall()

    if li == []:

        if ivzConstants.ivz_debug_level >= 1 :
            print ("getHostEntry : ERROR querying data from HOSTLIST Table")

        conn.close()

        return None
    else:
        result = li[0][0]

    conn.close()
    return result

def insertHostEntry(MAC, OTA):
        """
        Purpose : Insert MAC, OTA tuple for a host requesting connection
        Possible outcome : ERROR or SUCCESS
        """

        duplicateStatus = checkDuplicateMacEntry(MAC)

        conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

        if duplicateStatus == "TRUE":

            try:

                query = "UPDATE HOSTLIST SET OTA=""\"" + OTA + "\"" + " WHERE MACADDRESS=" + "\"" + MAC  + "\"" + ";"

                conn.execute(query)
                conn.commit()

            except Error as e:

                if ivzConstants.ivz_debug_level >= 1 :
                    print("insertHostEntry : ERROR Updating OTA in HOSTLIST Table : " + str(e))

                conn.close()
                return "ERROR"

        elif duplicateStatus == "FALSE":

            try:

                conn.execute("INSERT INTO HOSTLIST(MACADDRESS, OTA) VALUES (?,?);", (MAC,OTA,))
                conn.commit()

            except Error as e:

                if ivzConstants.ivz_debug_level >= 1 :
                    print("insertHostEntry : ERROR Inserting OTA in HOSTLIST Table : " + str(e))

                conn.close()
                return "ERROR"

        else:

            if ivzConstants.ivz_debug_level >= 1 :
                print("insertHostEntry : Wrong value returned by checkDuplicateMacEntry")


        if ivzConstants.ivz_debug_level >= 2 :
            print ("insertHostEntry : SUCCESS pushing MAC address\n")

        conn.close()
        return "SUCCESS"

def insertProjectEntry(projectName):
        """
        Purpose : Insert projectname into table to indicate the sftp has started
        Possible outcome : ERROR or SUCCESS
        """
        duplicateStatus = checkDuplicateProjectID(projectName)

        if duplicateStatus == "TRUE":

            if ivzConstants.ivz_debug_level >= 2 :
                print("insertProjectEntry : DUPLICATE")

            return "DUPLICATE"

        conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

        try:
            conn.execute("INSERT INTO PROJECTSLOG(NAME,STATUS) VALUES (?,?);", (projectName,"IN_PROGRESS",))
            conn.commit()

        except Error as e:

            if ivzConstants.ivz_debug_level >= 2 :
                print("insertProjectEntry : ERROR querying database : " + str(e))

            conn.close()
            return "ERROR"

        if ivzConstants.ivz_debug_level >= 2 :   
            print ("insertProjectEntry : SUCCESS")

        conn.close()
        return "SUCCESS"


def insertOrUpdateProjectDuration(projectName, duration):
        """
        Purpose : Insert projectname into durations table along with its timing
        Possible outcome : ERROR or SUCCESS
        """
        duplicateStatus = checkDuplicateProjectInDurations(projectName)

        if duplicateStatus == "TRUE":

            if ivzConstants.ivz_debug_level >= 3 :
                print("insertOrUpdateProjectDuration : DUPLICATE")

            conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

            try:

                query = "UPDATE PROJECTDURATIONS SET DURATION=" + "\""+ str(duration) + "\"" + " WHERE PROJECTNAME=" + "\"" +projectName + "\""

                cursor = conn.execute(query)
                conn.commit()

            except Error as e:

                if ivzConstants.ivz_debug_level >= 1 :
                    print("insertOrUpdateProjectDuration : ERROR querying database : " + str(e))

                conn.close()
                return "ERROR"

            conn.close()
            return "SUCCESS"

        elif duplicateStatus == "FALSE":

            conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

            try:
                conn.execute("INSERT INTO PROJECTDURATIONS(PROJECTNAME,DURATION) VALUES (?,?);", (projectName,str(duration),))
                conn.commit()

            except Error as e:

                if ivzConstants.ivz_debug_level >= 2 :
                    print("insertOrUpdateProjectDuration : ERROR querying database : " + str(e))

                conn.close()
                return "ERROR"

            if ivzConstants.ivz_debug_level >= 2 :   
                print ("insertOrUpdateProjectDuration : SUCCESS")

            conn.close()
            return "SUCCESS"

        else:
            return "ERROR"


def getProjectDuration(projectName):
        """
        Purpose : Get the screen duration of project
        Possible outcome : DURATION of projectName in database if project exists, -1 otherwise
        """
        conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

        try:
            query = "SELECT DURATION FROM PROJECTDURATIONS WHERE PROJECTNAME=" + "\"" +projectName + "\""

            cursor = conn.execute(query)

        except Error as e:

            if ivzConstants.ivz_debug_level >= 1 :
                print("getProjectDuration : ERROR Querying database : " + str(e))

            conn.close()
            return


        li = cursor.fetchall()

        dur = None

        if li == []:

            if ivzConstants.ivz_debug_level >= 1 :
                print ("getProjectDuration : ERROR . No values fetched from database.")

            dur = -1

        else:

            dur = li[0][0]

        conn.close()

        return dur


def deleteProjectFromDisplaySequence(projectName):

        """
        Purpose : Delete a project name from display sequence
                  This is mostly applicable for scenario when updated project is uploaded
        Return : True if all goes well, False otherwise
        """

        conn = connect(path.join(ivzConstants.IVZ_DATABASES + 'global_database.db'))

        try:
            query = "DELETE FROM DISPLAYSEQUENCE WHERE PROJECTNAME=" + "\"" +projectName + "\";"
            cursor = conn.execute(query)
            conn.commit()
            return True

        except Error as e:

            if ivzConstants.ivz_debug_level >= 1 :
                print("deleteProjectFromDisplaySequence : ERROR deleting entry from DISPLAYSEQUENCE. Error = "+ str(e))

            conn.close()
            return False

def deleteProject(projectName):
        """
        Purpose : Delete a project name from databse and remove the directory from /home/pi/IVZ/projects/
                  Updates the DISPLAY_QUEUE accordingly
        prints : DONE if project is successfully deleted, ERROR otherwise
        """

        conn = connect(path.join(ivzConstants.IVZ_DATABASES + 'global_database.db'))

        try:
            query = "DELETE FROM PROJECTSLOG WHERE NAME=" + "\"" +projectName + "\";"

            cursor = conn.execute(query)
            #conn.commit()

        except Error as e:

            if ivzConstants.ivz_debug_level >= 1 :
                print("deleteProject : ERROR deleting entry from PROJECTSLOG. Error = "+ str(e))

            conn.close()
            return

        # Also delete the project from display queue
        try:
            query = "DELETE FROM DISPLAYSEQUENCE WHERE PROJECTNAME=" + "\"" +projectName + "\";"
            cursor = conn.execute(query)
            #conn.commit()

        except Error as e:

            if ivzConstants.ivz_debug_level >= 1 :
                print("deleteProject : ERROR deleting entry from DISPLAYSEQUENCE. Error = "+ str(e))

            conn.close()
            return

        # Delete hash data for the project
        try:
            query = "DELETE FROM HASHDATA WHERE PROJECTNAME=" + "\"" +projectName + "\";"
            cursor = conn.execute(query)
            #conn.commit()

        except Error as e:

            if ivzConstants.ivz_debug_level >= 1 :
                print("deleteProject : ERROR deleting entry from HASHDATA. Error = "+ str(e))

            conn.close()
            return


        # Delete project duration entry from table
        try:
            query = "DELETE FROM PROJECTDURATIONS WHERE PROJECTNAME=" + "\"" +projectName + "\";"
            cursor = conn.execute(query)
            #conn.commit()

        except Error as e:

            if ivzConstants.ivz_debug_level >= 1 :
                print("deleteProject : ERROR deleting entry from PROJECTDURATIONS Error = "+ str(e))

            conn.close()
            return

        if ivzConstants.ivz_debug_level >= 2 :
            print('deleteProject' + str(cursor.rowcount))

        #if cursor.rowcount == 1:

        conn.commit()

        # Code to actually delete the folder
        localpath = path.join(ivzConstants.projects_dir,projectName)

        if path.exists(localpath):

            #rmtree(localpath)

            cmd = "sudo rm -rf " + localpath
            system(cmd)

            conn.close()

            if ivzConstants.ivz_debug_level >= 2 :
                print("deleteProject : DONE")

            return 'DONE'
        else:
            conn.close()

            if ivzConstants.ivz_debug_level >= 1 :
                print("deleteProject : ERROR . No values fetched from database")

            return 'ERROR'

        #print(cursor.rowcount)

        #TODO Add code for updating the QUEUE after project is deleted

def getProjectStatus(projectName):
        """
        Purpose : Get the download status of project
        return : Possible values are , COMPLETE or IN_PROGRESS
                 ERROR if something goes wrong in databse
        """
        conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

        try:

            query = "SELECT STATUS FROM PROJECTSLOG WHERE NAME=" + "\"" +projectName + "\""

            cursor = conn.execute(query)
            #conn.commit()

        except Error as e:

            if ivzConstants.ivz_debug_level >= 1 :
                print("setProjectStatus : ERROR querying database : " + str(e))

            conn.close()
            return "ERROR"

        li = cursor.fetchall()

        if li == []:

            if ivzConstants.ivz_debug_level >= 1 :
                print ("getProjectStatus : ERROR . No values fetched from database.")
                conn.close()
                return "ERROR"
        else:
             conn.close()
             return str(li[0][0])

def setProjectStatus(projectName, status):
        """
        Purpose : Set the download status of project to status value
        status : Possible values are , COMPLETE or IN_PROGRESS
        prints : DONE if update is succesfull , ERROR otherwise
        """
        conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

        try:
            #query = "UPDATE PROJECTSLOG SET STATUS=\"COMPLETE\" WHERE NAME=" + "\"" +projectName + "\""

            query = "UPDATE PROJECTSLOG SET STATUS=" + "\""+ status + "\"" + " WHERE NAME=" + "\"" +projectName + "\""

            cursor = conn.execute(query)
            conn.commit()

        except Error as e:

            if ivzConstants.ivz_debug_level >= 1 :
                print("setProjectStatus : ERROR querying database : " + str(e))

            conn.close()
            return "ERROR"

        if cursor.rowcount == 1:
            conn.close()

            if ivzConstants.ivz_debug_level >= 1 :
                print("setProjectStatus : DONE")

            return "DONE"
        else:
            conn.close()

            if ivzConstants.ivz_debug_level >= 1 :
                print("setProjectStatus : ERROR. No values fetched from database")

            return "ERROR"

        #print(cursor.rowcount)

        #print("DONE")


def getProjectID(projectName):
        """
        Purpose : Get the ID using projectname
        Possible outcome : ERROR or ID of projectName in database
        """
        conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

        try:
            query = "SELECT ID FROM PROJECTSLOG WHERE NAME=" + "\"" +projectName + "\""

            cursor = conn.execute(query)

        except Error as e:

            if ivzConstants.ivz_debug_level >= 1 :
                print("getProjectID : ERROR Querying database : " + str(e))

            conn.close()
            return


        li = cursor.fetchall()

        if li == []:

            if ivzConstants.ivz_debug_level >= 1 :
                print ("getProjectID : ERROR . No values fetched from database.")
        else:

            if ivzConstants.ivz_debug_level >= 2 :
                print (li[0][0])

        conn.close()

        #return li[0][0]

def insertOrUpdateProjectHash(projectname, hashvalue):
        """
        Purpose : Check if projectname already exists in database, if so update hashvalue else insert hash value
        return : TRUE if success, FALSE otherwise
        """

        #result = {'thumbnailrequired':False, 'dbfailure':False}
        result = {'dbfailure':False}

        conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

        cursor = None

        try:
            query = "SELECT HASH FROM HASHDATA WHERE PROJECTNAME=" + "\"" + projectname + "\""
            cursor = conn.execute(query)
            #conn.commit()

        except Error as e:

            if ivzConstants.ivz_debug_level >= 1 :
                print("insertOrUpdateProjectHash : SELECT HASH failed with error : " + str(e))

            conn.close()
            result['dbfailure'] = True
            #return "FALSE"
            return result

        
        #li = cursor.fetchall()
        li = []

        for row in cursor:
            li.append(row[0])

        if li == []:
            # No entry in database with given projectname
            try:

                if ivzConstants.ivz_debug_level >= 2 :
                    print("insertOrUpdateProjecthash : Inserting project name into hashdata table ")

                conn.execute("INSERT INTO HASHDATA(PROJECTNAME, HASH) VALUES (?,?);", (projectname, hashvalue,))

                conn.commit()
                #result['thumbnailrequired'] = True

            except Error as e:

                if ivzConstants.ivz_debug_level >= 2 :
                    print("insertOrUpdateProjectHash : ERROR Inserting hash in HASHDATA Table : "+str(e))

                conn.close()
                result['dbfailure'] = True
                #return "FALSE"

        else:

            #print("insertOrUpdateProjectHash : printing database values \n\n")
            #print(li)
            #print("*********************************************************")

            if li[0][0] != hashvalue:

                #result['thumbnailrequired'] = True

                # entry in database with given projectname exists already
                try:

                    if ivzConstants.ivz_debug_level >= 2 :
                        print("insertOrUpdateProjectHash : Updating project hash into hashdata table")

                    query = "UPDATE HASHDATA SET HASH=""\"" + hashvalue + "\"" + " WHERE PROJECTNAME=" + "\"" + projectname  + "\"" + ";"

                    conn.execute(query)
                    conn.commit()

                except Error as e:

                    if ivzConstants.ivz_debug_level >= 2 :
                        print("insertOrUpdateProjectHash : ERROR Updating hash in HASHDATA Table : " + str(e))

                    conn.close()
                    result['dbfailure'] = True
                    #return "FALSE"

            #else:
            #    if ivzConstants.ivz_debug_level >= 1 :
            #        print(" Project hash matches. No need to create thumbnails again\n")

        conn.close()
        return result


def checkDuplicateMacEntry(MAC):
        """
        Purpose : Check if MAC address already exists in database
        return : TRUE if duplicate entry, FALSE otherwise
        """

        conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

        try:
            query = "SELECT ID FROM HOSTLIST WHERE MACADDRESS=" + "\"" +MAC + "\""

            cursor = conn.execute(query)

        except Error as e:

            if ivzConstants.ivz_debug_level >= 1 :
                print("checkDuplicateMacEntry : SELECT ID FROM HOSTLIST failed with error : " + str(e))

            conn.close()
            # If for some reason database insert fails, we will return TRUE, to avoid pushing any values further
            return "TRUE"


        li = cursor.fetchall()

        conn.close()

        if li == []:
            # No entry in database with given MAC
            return "FALSE"
        else:
            # Duplicate entry in database with ID = li[0][0]
            return "TRUE"


def getProjectHash(projectName):
        """
        Purpose : Get hashvalue of given projectname
        return : False in case of error and hash value when success
        """

        conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

        try:
            # project will be considered as duplicate only if it was fully downloaded before
            query = "SELECT HASH FROM HASHDATA WHERE PROJECTNAME=" + "\"" +projectName + "\""

            cursor = conn.execute(query)

        except Error as e:

            if ivzConstants.ivz_debug_level >= 1 :
                print("getProjectHash : SELECT HASH FROM HASHDATA failed with error : " + str(e))

            conn.close()
            return False


        li = []

        for row in cursor:
            li.append(row[0])

        conn.close()

        if li == []:
            # No entry in database with projectName
            return False
        else:
            return li[0]

def checkProjectInSequence(projectName):
        """
        Purpose : Check if projectName already exists in project sequence
        return : TRUE if duplicate entry, FALSE otherwise
        """
        conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

        try:
            # project will be considered as duplicate only if it was fully downloaded before
            query = "SELECT ID FROM DISPLAYSEQUENCE WHERE PROJECTNAME=" + "\"" +projectName + "\""

            cursor = conn.execute(query)

        except Error as e:

            if ivzConstants.ivz_debug_level >= 1 :
                print("checkProjectInSequnce : SELECT ID FROM DISPLAYSEQUENCE failed with error : " + str(e))

            conn.close()
            # If for some reason database insert fails, we will return TRUE, to avoid pushing any values further
            return True


        #li = cursor.fetchall()
        li = []

        for row in cursor:
            li.append(row[0])

        conn.close()

        if li == []:
            # No entry in database with projectName
            return False
        else:
           
            return True


def checkDuplicateProjectID(projectName):
        """
        Purpose : Check if projectName already exists n database
        return : TRUE if duplicate entry, FALSE otherwise
        """
        conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

        try:
            # project will be considered as duplicate only if it was fully downloaded before
            query = "SELECT ID FROM PROJECTSLOG WHERE NAME=" + "\"" +projectName + "\""

            cursor = conn.execute(query)

        except Error as e:

            if ivzConstants.ivz_debug_level >= 1 :
                print("checkDuplicateProjectID : SELECT ID FROM PROJECTSLOG failed with error : " + str(e))

            conn.close()
            # If for some reason database insert fails, we will return TRUE, to avoid pushing any values further
            return "TRUE"


        #li = cursor.fetchall()
        li = []

        for row in cursor:
            li.append(row[0])

        conn.close()

        if li == []:
            # No entry in database with projectName
            return "FALSE"
        else:
           
            # Check consistency if ivx project directory.
            # There are cases when database contains entry of project but directory doesnt exist.
            # Handle this scenario by creating missing dirs
            checkProjectStructure(str(projectName))
            # Duplicate entry in database with ID = li[0][0]
            return "TRUE"

        #return li[0][0]

def checkDuplicateProjectInDurations(projectName):
        """
        Purpose : Check if projectName already exists in durations table
        return : TRUE if duplicate entry, FALSE otherwise
        """
        conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

        try:
            # project will be considered as duplicate only if it was fully downloaded before
            query = "SELECT ID FROM PROJECTDURATIONS WHERE PROJECTNAME=" + "\"" +projectName + "\""

            cursor = conn.execute(query)

        except Error as e:

            if ivzConstants.ivz_debug_level >= 1 :
                print("checkDuplicateProjectInDurations : SELECT ID FROM PROJECTDURATIONS failed with error : " + str(e))

            conn.close()
            # If for some reason database insert fails, we will return TRUE, to avoid pushing any values further
            return "TRUE"


        li = cursor.fetchall()

        conn.close()

        if li == []:
            # No entry in database with projectName
            return "FALSE"
        else:
           
            return "TRUE"


def checkProjectStructure(projectName):
    """
    Purpose : Check if ivz project directory and its sub directories exists. If not create them.
    """

    projectPath         = str(path.join(ivzConstants.projects_dir, projectName))
    projectImagesPath   = str(path.join(ivzConstants.projects_dir, projectName, 'images'))
    projectVideosPath   = str(path.join(ivzConstants.projects_dir, projectName, 'videos'))
    projectMetadataPath = str(path.join(ivzConstants.projects_dir, projectName, 'metadata'))

    if not path.exists(projectPath):
        command = "sudo mkdir " + projectPath
        system(command)

    if not path.exists(projectImagesPath):
        command = "sudo mkdir " + projectImagesPath
        system(command)

    if not path.exists(projectVideosPath):
        command = "sudo mkdir " + projectVideosPath
        system(command)

    if not path.exists(projectMetadataPath):
        command = "sudo mkdir " + projectMetadataPath
        system(command)

def decrypt_file(in_filename, out_filename=None, chunksize=24*1024):
    """ Decrypts a file using AES (CBC mode) using ivz key. 
    """

    if not out_filename:
        out_filename = path.splitext(in_filename)[0]

    with open(in_filename, 'rb') as infile:
        origsize = unpack('<Q', infile.read(calcsize('Q')))[0]
        iv = infile.read(16)
        decryptor = AES.new(ivzConstants.ivz_encryption_key, AES.MODE_CBC, iv)

        with open(out_filename, 'wb') as outfile:
            while True:
                chunk = infile.read(chunksize)
                if len(chunk) == 0:
                    break
                outfile.write(decryptor.decrypt(chunk))

                del chunk

            outfile.truncate(origsize)

    infile.close()
    outfile.close()


def encrypt_file(key, in_filename, out_filename=None, chunksize=64*1024):
    """ Encrypts a file using AES (CBC mode) with the
        given key.

        key:
            The encryption key - a string that must be
            either 16, 24 or 32 bytes long. Longer keys
            are more secure.

        in_filename:
            Name of the input file

        out_filename:
            If None, '<in_filename>.enc' will be used.

        chunksize:
            Sets the size of the chunk which the function
            uses to read and encrypt the file. Larger chunk
            sizes can be faster for some files and machines.
            chunksize must be divisible by 16.
    """

    if not out_filename:
        out_filename = in_filename + '.enc'

    try:

        iv = urandom(16)
        encryptor = AES.new(key, AES.MODE_CBC, iv)
        filesize = path.getsize(in_filename)

        with open(in_filename, 'rb') as infile:
            with open(out_filename, 'wb') as outfile:
                outfile.write(pack('<Q', filesize))
                outfile.write(iv)

                while True:
                    chunk = infile.read(chunksize)
                    if len(chunk) == 0:
                        break
                    elif len(chunk) % 16 != 0:
                        chunk += b' ' * (16 - len(chunk) % 16)

                    outfile.write(encryptor.encrypt(chunk))

    except Exception as e:
        return False


def zipdir(basedir, archivename):

    if not path.isdir(basedir):
        return False

    try:
        with closing(ZipFile(archivename, "w", ZIP_DEFLATED)) as z:
            for root, dirs, files in walk(basedir):
                #NOTE: ignore empty directories
                for fn in files:
                    absfn = path.join(root, fn)
                    zfn = absfn[len(basedir)+len(sep):]
                    z.write(absfn, zfn)

    except Exception as e:
        return False


    return True


def getTargetMiscData():
        """
        Purpose : Get misc data on this target device.
        Possible outcome : {} empty dictionary if something goes wrong, valid dictionary with current values if all goes well
        """

        result = {}

        conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

        try:
            query = "SELECT PROJECTUPDATESTATUS, SEQUENCEUPDATESTATUS, MARQUEEUPDATESTATUS, BINUPDATESTATUS, ORIENTATIONUPDATESTATUS from TARGET_MISC_STATUS"

            cursor = conn.execute(query)

        except Error as e:

            if ivzConstants.ivz_debug_level >= 1 :
                print("getTargetMiscData : ERROR Querying database : " + str(e))

            conn.close()
            return result


        li = cursor.fetchall()

        if li == []:

            if ivzConstants.ivz_debug_level >= 1 :
                print("getTargetMiscData : ERROR . No values fetched from database.")

            result['marqueeUpdateStatus'] = 0
            result['projectListUpdateStatus'] = 0
            result['sequenceUpdateStatus'] = 0
            result['binUpdateStatus'] = 0
            result['orientationStatus'] = 0

            return result

        else:

            result['marqueeUpdateStatus'] = li[0][2]
            result['projectListUpdateStatus'] = li[0][0]
            result['sequenceUpdateStatus'] = li[0][1]
            result['binUpdateStatus'] = li[0][3]
            result['orientationStatus'] = li[0][4]

        conn.close()

        return result


def updateTargetMiscStatus(marqueeStatus, projectStatus, sequenceStatus, orientationStatus, binStatus):
    """
    Purpose : Update target misc status with new values.
              Invoked mostly after the updates are downloaded from cloud
    Params :
              marqueeStatus  : Latest marquee status value
              projectStatus  :
              sequenceStatus :

    Return :  False if something goes wrong, True otherwise
    """

    conn = connect(ivzConstants.IVZ_DATABASES + 'global_database.db')

    try:
       conn.execute("UPDATE TARGET_MISC_STATUS SET 'MARQUEEUPDATESTATUS'=?, 'PROJECTUPDATESTATUS'=?, 'SEQUENCEUPDATESTATUS'=?, 'BINUPDATESTATUS'=?, 'ORIENTATIONUPDATESTATUS'=?", (marqueeStatus, projectStatus, sequenceStatus, binStatus, orientationStatus,))

    except Error as e:

        if ivzConstants.ivz_debug_level >= 1 :
            print("updateTargetMiscStatus : ERROR Inserting values. Error = " + str(e))

        conn.close()
        return False

    conn.commit()
    conn.close()
    return True

#initializeDatabases()
#updateTargetMiscStatus("12","13","14")
#print(getTargetMiscData())

#insertOrUpdateProjectDuration("allinone.ivzproj", 200.0)
#print(getProjectDuration("allinone1.ivzproj"))

#print(getProjectIndexInSequence('modified_onlyImage.ivzproj'))

#print(checkProjectInSequence('sample1.iwzproj'))

#checkProjectConsistency('xyz.ivzproj')
#print(getDisplaySequence())
#initializeDatabases()
#insertProjectInDisplayQueue("NONE", "project4.ivzproj")
#insertProjectInDisplayQueue("project9.ivzproj", "project10.ivzproj")
#res = getProjectList()
#print(res)
#print(res[0][0])
#print(res[0][1])

'''

def main(argv):
        #print(argv)

        if argv[1] == 'PUT':
            insertProjectEntry(argv[0])
        elif argv[1] == 'GET':
            getProjectID(argv[0])
        elif argv[1] == 'SET':
            #setProjectStatus(argv[0], 'COMPLETE')
            setProjectStatus(argv[0], argv[2])
        elif argv[1] == 'REMOVE':
            deleteProject(argv[0])
        #elif argv[1] == 'SETPROGRESS':
        #    setProjectStatus(argv[0], 'IN_PROGRESS')

        else:
            print("ERROR")


if __name__ == "__main__":

    if not path.exists(r'/home/pi/IVZ/databases/global_databases.db'):
        initializeDatabases()
         
    #checkDuplicateProjectID(sys.argv[1])

    main(sys.argv[1:])

    #insertProjectEntry(r"/home/pi/Judwa.mp4")
    #getProjectID(r"/home/pi/Judwa.mp4")

'''

#res = getProjectStatus("onlyImage.ivzproj")
#print(res)
#print(type(res))
#print(insertOrUpdateProjectHash('zzallinone.ivzproj', '9ceeea276884b8185c9617c4c01bf199'))

