from IVZGLOBALS import ivzConstants
from IVZMGR import ivzProjectManager

from hashlib import sha1
from os import path
from requests import post, get
from ast import literal_eval


def hash_file(filename):
    """
    Purpose : This function returns the SHA-1 hash of the file passed into it
    :param filename: full path of file
    :return: hash value of the provided file,
            None if file does not exist
    """

    if not path.exists(filename):
        return None

    # make a hash object
    h = sha1()

    # open file for reading in binary mode
    with open(filename,'rb') as file:

       chunk = 0
       while chunk != b'':
           # read only 1024 bytes at a time
           chunk = file.read(1024)
           h.update(chunk)

    # return the hex representation of digest
    return h.hexdigest()


def updateMarquee(newMarqueeString):

    print("\n updateMarquee received : " + str(newMarqueeString))

    result = False
	
    # Write the text to message.txt        	
    try:                
        with open(path.join(ivzConstants.ivz_marquee_dir,'message.txt'), 'w') as content_file:                    
	    content_file.write(newMarqueeString)

	result = True

    except Exception as e:

        if ivzConstants.ivz_debug_level >= 1:
            print('querAndUpdateMarquee : UPDATE_MARQUEE_STRING failed : ' + str(e))

        result = False

    return result


'''
    Handle all the tasks related to project download
    Return : True if updatequeue is required, False otherwise
'''
def handleDownloads(downloadList):

    updateQueue = False

    for project in downloadList:

        isDuplicate = ivzProjectManager.checkProjectInSequence(project)

        # Query the hash value from web
        try:
            res = post('https://www.castmyads.com/castmyads_web/product/castmyads/app/queryProjectHash.php', data={'customerID': 'CMA_1001', 'projectName': str(project)})
        except Exception as e:

            if ivzConstants.ivz_debug_level >= 1:
                print('handleDownloads : queryProjectHash  : failed with error : ' + str(e))

            continue

        hashOnServer = res.content

        if hashOnServer == "ERROR":
            # Something went wrong while querying project hash.
            continue

        if isDuplicate:

            # Project already exists. Check hash values
            # If hash values match, there is no need to download

            # Match the hash values
            projectHashOnTarget = ivzProjectManager.getProjectHash(project)

            if projectHashOnTarget == False:

                if ivzConstants.ivz_debug_level >= 1:
                    print('handleDownloads : getProjectHash failed')

                # Project does not exist on target
                continue

            elif projectHashOnTarget == hashOnServer:

                if ivzConstants.ivz_debug_level >= 1:
                    print('handleDownloads : project ' + str(project) + ' has not changed. SKipping download ...')

                # There is no change in project. No need to download.
                continue


        # Fresh project. Just download.
        url = 'https://www.castmyads.com/castmyads_web/product/castmyads/users/CMA_1001/projects/' + project

        if isDuplicate:
            filename = ivzConstants.ivz_tmp_dir + str(project)
        else:
            filename = ivzConstants.projects_dir + str(project)

        try:
            r = get(url, allow_redirects=True)

            open(filename, 'wb').write(r.content)

        except Exception as e:

            if ivzConstants.ivz_debug_level >= 1:
                print('handleDownloads : Downloading project' + str(project) + ' failed with error : ' + str(e))

            # TODO Shall we retry here ?
            continue
 

        # Match the hash values after download is done
        projectHashOnTarget = str(hash_file(path.join(filename)))
        print(projectHashOnTarget)

        if projectHashOnTarget != hashOnServer:

            if ivzConstants.ivz_debug_level >= 1:
                print('handleDownloads : Download was incomplete or corrupted !')

            # Download failed or was incomplete
            continue
        
        # Deflate project
        #ivzProjectManager.deflate_zip_project(filename, project)
        ivzProjectManager.unzip_project(filename, project)

        # Check project consistency
        consistencyResult = ivzProjectManager.checkProjectConsistency(filename)

        if consistencyResult == False:

            if ivzConstants.ivz_debug_level >= 1:
                print('handleDownloads : Download project is not consistent !')

            # Downloaded Project is not consistent
            continue

        hashUpdateResult = ivzProjectManager.insertOrUpdateProjectHash(project, projectHashOnTarget)

        if hashUpdateResult['dbfailure'] == True :

            if ivzConstants.ivz_debug_level >= 1:
                print('handleDownloads : Failed to update hash values !')

            continue

        if not isDuplicate:

            if not ivzProjectManager.insertProjectInDisplayQueue("NONE", project):

                if ivzConstants.ivz_debug_level >= 1:
                    print('handleDownloads : Failed to insert project in display queue !')

                continue

            else:
                # Everything went as expected. Set this flag to ensure that the caller issues commands to update the display queue
                updateQueue = True

    return updateQueue
    

#print(handleDownloads(['test_screens.iwzproj']))


def updateProjects(queriedSequence):

    targetSequence = ivzProjectManager.getDisplaySequence()

    # Result of above call looks like this : [(u'sample1.iwzproj',), (u'sample2.iwzproj',)]

    # List of projects to be deleted
    deleteList = []

    # List of projects to be downloaded
    downloadList = []

    # Populate list of projects to be downloaded
    for project in queriedSequence:

        if project not in targetSequence:

            downloadList.append(project)

    # Populate list of projects to be deleted
    for project in targetSequence:

        if project not in queriedSequence:

            deleteList.append(project)


    # Actually delete the projects
    for project in deleteList:

        ivzProjectManager.deleteProject(project)


    # Handle downloads in a different functions
    handleDownloads(downloadList)
    

'''
    Issues web query to get current status IVZ projects, marquee etc on cloud.
    Performs sync, download, delete etc based on current target device state
'''
def queryStatus():

    try:
        res = post('https://www.castmyads.com/castmyads_web/product/castmyads/app/queryStatus.php', data={'customerID': 'CMA_1001', 'cpuID':'00000000dbb7bcf3'})
    except Exception as e:

        if ivzConstants.ivz_debug_level >= 1:
            print('queryStatus : failed with error : ' + str(e))

        return False

    response = res.content

    if response == "ERROR":
        return False

    print(response)

    data = literal_eval(response)

    print("\nQueried data : " + str(data))

    localStatus = ivzProjectManager.getTargetMiscData()

    if localStatus == {}:
        # Something is wrong with local database
        return False

    # Check if payment is done or not.
    # Dont process any requests till this bit is set
    if data['paymentStatus'] == '0':
        return False

    res = post('https://www.castmyads.com/castmyads_web/product/castmyads/app/miscDataQuery.php', data={'customerID': 'CMA_1001', 'cpuID':'00000000dbb7bcf3', 'queryType':'all'})

    # Contains marquee text, orientation, project sequence
    miscData = literal_eval(res.content)

    print("\nmisc_data : " + str(miscData))

    if data['marqueeUpdateStatus'] > localStatus['marqueeUpdateStatus']:

        # Update marquee text here       

        # Update local status
        localStatus['marqueeUpdateStatus'] = data['marqueeUpdateStatus']

	updateMarquee(miscData['marqueeText'])            


    if data['projectListUpdateStatus'] > localStatus['projectListUpdateStatus']:

        # Update project list here. Download / delete projects if required

        # Update local status
        localStatus['projectListUpdateStatus'] = data['projectListUpdateStatus']

        updateProjects(miscData['projectSequence'])


    if data['sequenceUpdateStatus'] > localStatus['sequenceUpdateStatus']:

        # Update display sequence here

        # Update local status
        localStatus['sequenceUpdateStatus'] = data['sequenceUpdateStatus']

    if data['orientationStatus'] > localStatus['orientationStatus']:

        # Update display sequence here

        # Update local status
        localStatus['orientationUpdateStatus'] = data['orientationStatus']

    if data['binUpdateStatus'] > localStatus['binUpdateStatus']:

        # Handle binary update here

        print("Handle Binary Update")


    # TODO We need a flag to indicate a project was updated without changing the sequence

    # Push values to database with latest status
    ivzProjectManager.updateTargetMiscStatus(localStatus['marqueeUpdateStatus'], localStatus['projectListUpdateStatus'], localStatus['sequenceUpdateStatus'], localStatus['orientationStatus'], localStatus['binUpdateStatus'])


queryStatus()
