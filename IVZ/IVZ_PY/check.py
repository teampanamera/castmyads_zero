import os
import time

filepath = r'/home/pi/sftp.log'

#file = open(filepath, "r")

openfiles = []

with open(filepath, "r") as f:
    for line in f:
        if ": open " in line:

            name = line.split("]:")[1].split(" ")[2].split("\"")[1]

            openfiles.append(name)

        if ": close " in line:

            name = line.split("]:")[1].split(" ")[2].split("\"")[1]

            if name in openfiles:
                openfiles.remove(name)

    f.close()

print(openfiles)

if len(openfiles) == 0:
    print("No Open files")

else:

    # TODO : Make sure there is a deamon in place which cleans up the sftp.log file when there is no upload in progress

    for file in openfiles:
        before = os.stat(file).st_size

        time.sleep(1)

        after = os.stat(file).st_size

        if before != after:
            print("Write operation in progress")
            break
