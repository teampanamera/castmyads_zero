from IVZGLOBALS import ivzConstants
from threading import Timer

class lockmanager:
    '''
        This class is responsible for managing a global lock which is used for requesting access to a target machine.
        Since there are multiple clients that can access the same target machine, this global lock prevents concurrent access to a given target.
    '''

    def __init__(self):

        self.clientIP = None
        self.lockTimer = None

    def acquireLock(self, ipAddress):
        '''
            Purpose   : Acquire lock using IP address of the client. Also launch a timer to reset the lock after 10 minutes
            ipAddress : IP address of the client machine which requests the lock. Note that there is no explicit request to acquire the lock.
                        Lock is implicitely managed by target machine.
        '''

        self.clientIP = ipAddress
        self.lockTimer = Timer(600.0, self.releaseLock)
        self.lockTimer.start()

    def releaseLock(self):
        '''
            Purpose : Release the lock which was acquired by a client and cancel the timer
        '''
        if ivzConstants.ivz_debug_level >= 3:
            print("releaseLock : Releasing global lock for client = " + str(self.clientIP))

        self.lockTimer.cancel()
        self.clientIP = None

    def extendLock(self, ipAddress):
        '''
            Purpose : Although the lock has a 10 minutes validity, the countdown starts from the last access to the target.
                      It means lock will be released when the acquiring client does not communicate with target for 10 minutes.
                      This function restarts the timer, every time client communicates with the target.
        '''

        self.releaseLock()
        self.acquireLock(ipAddress)

    def isLockAcquired(self):
        '''
            Purpose : Check whether lock is acquired by any client or not
            Returns : True, if the lock is taken
                      False, otherwise
        '''

        if self.clientIP != None:
            return True
        else:
            return False

    def requestTargetAccess(self, ipAddress):
        '''
            Purpose : Query whether lock is acquired by given ipAddress.
            Return  : True, if provided ipAddress is the owner of the lock
                      False, otherwise
        '''

        if self.clientIP == ipAddress:
            return True
        else:
            return False


# Although caller to this function wil handle thi scenario, just double check
# Lock cannot be extended if requesting client does not have the lock

#def hello(a1, a2):
#    print(str(a1) + '\t' + str(a2))

#t = Timer(2.0, hello, [1,2])
#t.start()
